package com.prologics.Bellose.network.Entity.login;


import com.google.gson.annotations.SerializedName;

public class LoginUserApiEntity {
//    @SerializedName("id")
//    public String id = "";
    @SerializedName("code")
    public String code;
    @SerializedName("name")
    public String name;
    @SerializedName("device_id")
    public String deviceId;
//    @SerializedName("last_inv")
//    public int lastInv=0;
//    @SerializedName("inv_code")
//    public String invCode= "";
//    @SerializedName("stockist_name")
//    public String stockistName= "";
//    @SerializedName("stockist_code")
//    public String stockistCode= "";
//    @SerializedName("tech_support")
//    public String techSupport= "";
    @SerializedName("target")
    public String target;
    @SerializedName("message")
    public String message;



}