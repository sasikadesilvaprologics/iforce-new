package com.prologics.Bellose.network.Entity.body;

import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;

import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class InvoiceCreateBody {


    @SerializedName("customers_id") public int customers_id;
    @SerializedName("code") public int code;
    @SerializedName("invoice_discount") public double invoiceDiscount;
    @SerializedName("invoice_discount_type") public int invoiceDiscountType;
    @SerializedName("eff_date") public String effDate;
    @SerializedName("sync_time") public String syncTime;
    @SerializedName("is_synced") public boolean isSync;
    @SerializedName("battery_level") public double batteryLevel;
    @SerializedName("latitude") public double latitude;
    @SerializedName("longitude") public double longitude;
    @SerializedName("created") public String created;
    @SerializedName("online") public int online;
    @SerializedName("device_id") public int deviceId;
    @SerializedName("items")
    public List<Item> itemList = new ArrayList();

    public void setInvoiceItems(List<InvoiceItem> invoiceItems, Shared shared, OnTaskCompleted onTaskCompleted){


        for(InvoiceItem invoiceItem : invoiceItems){


                Item item = new Item();
                item.invoiceCode = code;
                item.customers_id = shared.getSelectedCustomerId();
                item.device_id = Integer.parseInt(shared.getCurrentUser().deviceId);
                item.items_id = invoiceItem.items_id;
                item.items_name = invoiceItem.item_name;
                item.qty_selable = invoiceItem.invoice_item_qty_s;
                item.qty_nonselable = invoiceItem.invoice_item_qty_ns;
                item.mrp = invoiceItem.invoiceItemmrp;
                item.discount = invoiceItem.invoiceItemDiscount;
                item.discount_type = 0;
                item.discount_amount = invoiceItem.invoiceItemTotalDiscount;
                item.is_manual_dis = 0;
                item.total = invoiceItem.invoiceItemTotalAmount;
                item.eff_date = invoiceItem.updatedAt;
                item.item_type = invoiceItem.invoice_item_flag;


                itemList.add(item);

        }

        onTaskCompleted.onTaskCompleted();
    }


    public class Item {

        @SerializedName("invoice_code")
        public Integer invoiceCode;
        @SerializedName("customers_id")
        public Integer customers_id;
        @SerializedName("device_id")
        public Integer device_id;
        @SerializedName("items_id")
        public Integer items_id;

        @SerializedName("items_name")
        public String items_name;
        @SerializedName("qty_selable")
        public Double qty_selable;
        @SerializedName("qty_nonselable")
        public Double qty_nonselable;

        @SerializedName("mrp")
        public Double mrp;
        @SerializedName("discount")
        public Double discount;
        @SerializedName("discount_type")
        public Integer discount_type;

        @SerializedName("discount_amount")
        public Double discount_amount;
        @SerializedName("is_manual_dis")
        public Integer is_manual_dis;
        @SerializedName("total")
        public double total;
        @SerializedName("eff_date")
        public String eff_date;
        @SerializedName("item_type")
        public Integer item_type;

    }


}
