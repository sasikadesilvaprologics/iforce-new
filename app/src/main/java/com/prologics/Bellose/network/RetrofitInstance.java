package com.prologics.Bellose.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitInstance {


    //    private static final String BASE_URL = "http://www.jobbank.space/ideamart/photorecover/Api/";

    private static final String BASE_URL = "http://sfa.prologics.lk/http/api/";
    private static final String BASE_URL_NEW  = "http://sfa.prologics.lk/api/";
    private static final String Testing_BASE_URL_NEW  = "https://testingwebsasika.000webhostapp.com/";
    private static Retrofit retrofit;
    private static Retrofit retrofitTest;


//    public static Retrofit getRetrofitInstance() {
//
//        if (retrofit == null) {
//
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(BASE_URL)
//                    .addConverterFactory(ScalarsConverterFactory.create())
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//        }
//        return retrofit;
//    }

    public static Retrofit getRetrofitTestInstance() {

        if (retrofitTest == null) {

            retrofitTest = new Retrofit.Builder()
                    .baseUrl(Testing_BASE_URL_NEW )
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitTest;
    }


    public static Retrofit getRetrofitInstanceNew() {

        if (retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_NEW)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
