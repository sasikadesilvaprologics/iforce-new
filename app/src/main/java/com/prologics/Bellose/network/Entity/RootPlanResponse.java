package com.prologics.Bellose.network.Entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RootPlanResponse {


    @SerializedName("message")
    public String message;
    @SerializedName("success")
    public Boolean success;


    @SerializedName("data")
    public List<Plan> data = new ArrayList<>();


    public class Plan{
        @SerializedName("schedule_id")
        public String schedule_id;

        @SerializedName("date")
        public String date;

        @SerializedName("areas_id")
        public String areas_id;

        @SerializedName("name")
        public String name;

        @SerializedName("day")
        public String day;

        @SerializedName("is_holiday")
        public Integer is_holiday;

    }



}
