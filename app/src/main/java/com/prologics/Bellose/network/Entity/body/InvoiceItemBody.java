package com.prologics.Bellose.network.Entity.body;

import com.google.gson.annotations.SerializedName;

public class InvoiceItemBody {

    @SerializedName("invoice_code")
    public Integer invoiceCode;
    @SerializedName("customers_id")
    public Integer customers_id;
    @SerializedName("device_id")
    public Integer device_id;
    @SerializedName("items_id")
    public Integer items_id;

    @SerializedName("items_name")
    public String items_name;
    @SerializedName("qty_selable")
    public Double qty_selable;
    @SerializedName("qty_nonselable")
    public Double qty_nonselable;

    @SerializedName("mrp")
    public Double mrp;
    @SerializedName("discount")
    public Double discount;
    @SerializedName("discount_type")
    public Integer discount_type;

    @SerializedName("discount_amount")
    public Double discount_amount;
    @SerializedName("is_manual_dis")
    public Integer is_manual_dis;
    @SerializedName("total")
    public double total;
    @SerializedName("eff_date")
    public String eff_date;
    @SerializedName("item_type")
    public Integer item_type;
}
