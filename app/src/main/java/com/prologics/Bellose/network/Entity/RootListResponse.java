package com.prologics.Bellose.network.Entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RootListResponse {

    @SerializedName("success")
    public Boolean success;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public List<Data> data ;




    public class Data{


            @SerializedName("area_id")
            public int areaId;
            @SerializedName("name")
            public String name;
            @SerializedName("last_date")
            public String last_date;



    }
}
