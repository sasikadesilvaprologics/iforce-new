package com.prologics.Bellose.network.Entity;

import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.google.gson.annotations.SerializedName;

public class CustomerCreateResponse {

    @SerializedName("message")
    public String message;
    @SerializedName("success")
    public Boolean success;


    @SerializedName("data")
    public Customer data;


}
