package com.prologics.Bellose.network.Entity;

import com.prologics.Bellose.databaseModule.Entity.CustomerType;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerTypeResponse  {

    @SerializedName("message")
    public String message;
    @SerializedName("success")
    public Boolean success;

    @SerializedName("data")
    public List<CustomerType> dataList;

}
