package com.prologics.Bellose.network;

import com.prologics.Bellose.network.Entity.CommonResponse;
import com.prologics.Bellose.network.Entity.CustomerArrayResponse;
import com.prologics.Bellose.network.Entity.CustomerCreateResponse;
import com.prologics.Bellose.network.Entity.CustomerTypeResponse;
import com.prologics.Bellose.network.Entity.RootListResponse;
import com.prologics.Bellose.network.Entity.RootPlanResponse;
import com.prologics.Bellose.network.Entity.StockListResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIClient {

    @FormUrlEncoded
    @POST("login?")
    Call<CommonResponse> userLogin(@Field("pin") String pin, @Field("mac_id") String mac,@Field("version") int version);

    @FormUrlEncoded
    @POST("loadAreaList?")
    Call<RootListResponse> getRootList(@Field("device_id") int deviceId);

    @FormUrlEncoded
    @POST("loadStockList?")
    Call<StockListResponse> getStockList(@Field("device_id") int deviceId);

    @FormUrlEncoded
    @POST("loadCustomerTypes?")
    Call<CustomerTypeResponse> getCustomerTypeList(@Field("device_id") int deviceId);

    @FormUrlEncoded
    @POST("loadCustomers?")
    Call<CustomerArrayResponse> getCustomers(@Field("areas_id") int areaId);

    @FormUrlEncoded
    @POST("ListRoute?")
    Call<RootPlanResponse> getRootPlanList(@Field("device_id") int deviceId,
                                           @Field("date_from") String dateFrom,
                                           @Field("date_to") String dateTo);


    @Multipart
    @POST("CreateCustomer?")
    Call<CustomerCreateResponse> createCustomer(@Part("customer_types_id") RequestBody customer_type_id,
                                                @Part("areas_id") RequestBody areas_id,
                                                @Part("code") RequestBody code,
                                                @Part("name") RequestBody name,
                                                @Part("address_no") RequestBody address_no,
                                                @Part("address_1") RequestBody address_1,
                                                @Part("address_2") RequestBody address_2,
                                                @Part("street") RequestBody street,
                                                @Part("contact_name") RequestBody contact_name,
                                                @Part("mobile") RequestBody mobile,
                                                @Part("landline") RequestBody landline,
                                                @Part("cover_image") RequestBody cover_image,
                                                @Part("latitude") RequestBody latitude,
                                                @Part("longitude") RequestBody longitude,
                                                @Part("is_synced") RequestBody is_synced,
                                                @Part("created") RequestBody created,
                                                @Part("synced") RequestBody synced,
                                                @Part("is_approved") RequestBody is_approved,
                                                @Part("discount") RequestBody discount,
                                                @Part MultipartBody.Part image,
                                                @Part("online") RequestBody online);

    @FormUrlEncoded
    @POST("InvoiceCreate?")
    Call<String> createInvoice(
            @Field("customers_id") int customerId,
            @Field("code") int code,

            @Field("invoice_net_total") double invoiceNetTotal,
            @Field("invoice_return_total") double invoiceReturnTotal,
            @Field("invoice_total") double invoiceTotal,
            @Field("invoice_other_discount") double invoiceOtherDiscount,


            @Field("invoice_discount") double invoiceDiscount,
            @Field("invoice_discount_type") int invoiceDiscountType,
            @Field("eff_date") String effDate,
            @Field("sync_time") String syncTime,
            @Field("is_synced") boolean isSync,
            @Field("battery_level") double batteryLevel,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("created") String created,
            @Field("online") int online,
            @Field("device_id") int deviceId,
            @Field("items") String items
            );


    @FormUrlEncoded
    @POST("SyncCustomer?")
    Call<String> getCustomerInvoices(
            @Field("device_id") int deviceId,
            @Field("customers_id") int customerId
    );

    @FormUrlEncoded
    @POST("UpdateCustomer?")
    Call<String> updateCustomer(
            @Field("customer_id") int customerId,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude
    );


    @FormUrlEncoded
    @POST("UpdateShelf?")
    Call<String> updateSelf(
            @Field("customers_id") int customerId,
            @Field("items") String items

    );


    @POST("loadStatusList?")
    Call<String> loadCustomerStatusList(


    );


    @FormUrlEncoded
    @POST("UpdateStatus?")
    Call<String> updateCustomerStatus(
            @Field("customers_id") int customerId,
            @Field("device_id") int deviceId,
            @Field("ptype") int ptype,
            @Field("bat_level") int batLevel,
            @Field("remarks") String remarks,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude



    );


    @FormUrlEncoded
    @POST("Dashboard?")
    Call<String> getCustomerDashboard(
            @Field("device_id") int deviceId
    );


    @POST("TestServer?")
    Call<String> testServerConnection(

    );


    @FormUrlEncoded
    @POST("lastinvoicecode?")
    Call<String> getLastInvoiceId(
            @Field("device_id") int deviceId
    );


    @FormUrlEncoded
    @POST("CustomerDashboard?")
    Call<String> getCustomerHistory(
            @Field("customers_id") int customerId
    );


    @FormUrlEncoded
    @POST("chkversion?")
    Call<String> checkAppVersion(
            @Field("cur_version") int currentVersion,
            @Field("package") String packageName
    );






    //removewable

    @GET("test.json?")
    Call<String> testQAUrl(

    );

















//    @POST("InvoiceCreate?")
//    Call<String> createInvoice(
//
//            @Body InvoiceCreateBody invoiceCreateBody
//            );


}
