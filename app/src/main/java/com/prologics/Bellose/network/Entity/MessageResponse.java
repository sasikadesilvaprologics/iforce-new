package com.prologics.Bellose.network.Entity;

import com.google.gson.annotations.SerializedName;

public class MessageResponse {

    @SerializedName("success")
    public Boolean success;

    @SerializedName("message")
    public String message;
}
