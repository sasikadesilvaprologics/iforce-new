package com.prologics.Bellose.network.Entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommonResponseArray {

    @SerializedName("message")
    public String message;
    @SerializedName("success")
    public Boolean success;


    @SerializedName("data")
    public List<Object> data;
}
