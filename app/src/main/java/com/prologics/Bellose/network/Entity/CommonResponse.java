package com.prologics.Bellose.network.Entity;

import com.google.gson.annotations.SerializedName;

public class CommonResponse {

    @SerializedName("message")
    public String message;
    @SerializedName("success")
    public Boolean success;


    @SerializedName("data")
    public Object data;
}
