package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import static com.prologics.Bellose.databaseModule.GlobalValues.getDate;

@Entity(tableName = "invoice_item")
public class InvoiceItem {

    @PrimaryKey(autoGenerate = true)
    public  int invoice_item_id;

    @ColumnInfo(name =  "invoice_id")
    public  int invoiceId;

    @ColumnInfo(name =  "device_id ")
    public  int deviceId;

    @ColumnInfo(name =  "customer_id")
    public int customerId;

    @ColumnInfo(name =  "items_id")
    public int items_id;

    @ColumnInfo(name =  "invoice_item_mrp")
    public double invoiceItemmrp = 0.00;

    @ColumnInfo(name =  "invoice_item_total_amount")
    public double invoiceItemTotalAmount = 0.00;

    @ColumnInfo(name =  "invoice_item_discount")
    public double invoiceItemDiscount = 0.00;

    @ColumnInfo(name =  "invoice_item_total_discount")
    public double invoiceItemTotalDiscount = 0.00 ;

    @ColumnInfo(name =  "invoice_item_qty_s")
    public double invoice_item_qty_s = 0.00 ;

    @ColumnInfo(name =  "invoice_item_qty_ns")
    public double invoice_item_qty_ns = 0.00;

    @ColumnInfo(name =  "invoice_item_flag")
    public int invoice_item_flag ;

    @ColumnInfo(name =  "invoice_item_is_autoLoad")
    public boolean invoice_item_is_autoLoad = false ;

    @ColumnInfo(name = "is_pre_sale")
    public boolean isPreSalse  = false;

    @ColumnInfo(name = "updated_at")
    public String updatedAt = getDate();

    public  String item_name="";

    public  double selling_price = 0.0;

    public double qty;
}
