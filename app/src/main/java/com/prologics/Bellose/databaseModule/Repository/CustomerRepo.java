package com.prologics.Bellose.databaseModule.Repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.DAO.CustomerDAO;
import com.prologics.Bellose.databaseModule.DataStore.AppDb;
import com.prologics.Bellose.databaseModule.Entity.CustomerWithRelation;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.util.List;


public class CustomerRepo {

    private CustomerDAO customerDAO;
    private WeakReference<Context> parentContext;
    private Shared shared;
    private AppDb db;

    public CustomerRepo(Context application) {
        this.parentContext = new WeakReference<>(application);
       db  = AppDb.getDB(application);

        customerDAO = db.customerDAO();
        shared = Shared.getInstance(application);

    }

    public void deleteAll(){
        customerDAO.deleteAll();
    }

    public void insertDummyData() {
//        new insertAsyncTask(parentContext.get(), customerDAO).execute(Customer.fromJSON());
    }

    public void insertNewCustomer(Customer customer, OnTaskCompleted onTaskCompleted) {
        System.out.println("=======???>>>>>>>>");
        new insertAsyncTask(parentContext.get(), customerDAO,onTaskCompleted).execute(customer);
    }

    public void insertAll(List<Customer> customer, OnTaskCompleted onTaskCompleted) {
        db.getTransactionExecutor().execute(new Runnable() {
            @Override
            public void run() {
                customerDAO.deleteAll();
                customerDAO.insertAll(customer.toArray(new Customer[0]));
            }
        });
        onTaskCompleted.onTaskCompleted();
//        new insertAsyncTask(parentContext.get(), customerDAO,onTaskCompleted).execute(customer.toArray(new Customer[0]));
    }

    public LiveData<List<CustomerWithRelation>> getAllCustomers() {
        return customerDAO.getAllCustomersByRoot(Integer.parseInt(shared.getSelectedRoot().id));
    }


    public Customer getCustomer(int id){
        return customerDAO.getCustomer(id);
    }


    private static class insertAsyncTask extends AsyncTask<Customer, Void, Void> {

        private CustomerDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;

        insertAsyncTask(Context parentContext, CustomerDAO customerDAO,OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = customerDAO;
            this.listener=listener;
        }

        @Override
        protected Void doInBackground(Customer... Customers) {

            asyncDAO.insertAll(Customers);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }
    }
}
