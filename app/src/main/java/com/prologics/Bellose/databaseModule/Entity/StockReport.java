package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;

public class StockReport {

    @ColumnInfo(name = "item_name")
    public String item_name = "";

    @ColumnInfo(name = "sales_count")
    public double invoiceCount = 0.0;

    @ColumnInfo(name = "free_count")
    public double freeCount = 0.0;

    @ColumnInfo(name = "sample_count")
    public double sampleCount = 0.0;

    @ColumnInfo(name = "salable_count")
    public double salableCount = 0.0;

    @ColumnInfo(name = "non_salable_count")
    public double nonSalableCount = 0.0;

    @ColumnInfo(name = "updated_at")
    public String updated = "";
}
