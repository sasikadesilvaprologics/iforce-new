package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Ignore;

public class SelfWithItem {


    @ColumnInfo(name =  "self_id")
    public  int self_id;

    @ColumnInfo(name =  "customer_id")
    public  int customerId ;

    @ColumnInfo(name =  "items_id")
    public  int itemsid;

    @ColumnInfo(name =  "own_qty")
    public  double ownQty;

    @ColumnInfo(name =  "other_qty")
    public  double other_qty;


    @ColumnInfo(name =  "item_name")
    public  String itemName;

    @ColumnInfo(name =  "min_price")
    public  double  minPrice;

    @ColumnInfo(name =  "max_price")
    public  double maxPrice;

    @ColumnInfo(name =  "selling_price")
    public  double sellingPrice;

    @Ignore
    public   double newQty = 0;
    @Ignore
    public double newOtherQty = 0;



}
