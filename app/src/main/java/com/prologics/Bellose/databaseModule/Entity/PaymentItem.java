package com.prologics.Bellose.databaseModule.Entity;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "payment_item")
public class PaymentItem {

    @PrimaryKey(autoGenerate = true)
    int id ;

    @ColumnInfo(name =  "payment_id")
    @SerializedName("payment_id")
    public  int payment_id;

    @ColumnInfo(name =  "invoice_id")
    @SerializedName("invoice_id")
    public  int invoice_id;

    @ColumnInfo(name =  "invoice_amount")
    @SerializedName("invoice_amount")
    public  double invoice_amount;

    @ColumnInfo(name =  "amount")
    @SerializedName("amount")
    public  double amount;

}
