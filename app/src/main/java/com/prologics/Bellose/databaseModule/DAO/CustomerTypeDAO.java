package com.prologics.Bellose.databaseModule.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.prologics.Bellose.databaseModule.Entity.CustomerType;

import java.util.List;

@Dao
public interface CustomerTypeDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(CustomerType... customerTypes);

    @Query("SELECT * FROM customer_type")
    LiveData<List<CustomerType>> getAllTypes();
}
