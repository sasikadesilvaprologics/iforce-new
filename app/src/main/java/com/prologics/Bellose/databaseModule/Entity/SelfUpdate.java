package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "self_update")
public class SelfUpdate {

    @PrimaryKey(autoGenerate = true)
    public  int self_update_id;

    @ColumnInfo(name =  "customer_id")
    public  int customer_id;

    @ColumnInfo(name =  "updated")
    public String updated;
}
