package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@Entity(tableName = "invoice")
public class Invoice {


    @PrimaryKey
    public  int invoice_code;

    @ColumnInfo(name = "customer_id")
    public int customerId = 0;

    @ColumnInfo(name = "device_id")
    public int deviceId = 0;

    @ColumnInfo(name = "eff_date")
    public String effDate = "";

    @ColumnInfo(name = "invoice_discount_total")
    public double invoice_total_discount = 0.00;

    @ColumnInfo(name = "invoice_net_total")
    public double invoice_net_total = 0.00;

    @ColumnInfo(name = "invoice_return_total")
    public double invoice_return_total = 0.00;

    @ColumnInfo(name = "invoice_total")
    public double invoice_total = 0.00;

    @ColumnInfo(name = "invoice_other_discount")
    public double invoice_other_discount = 0.00;

    @ColumnInfo(name = "created")
    public String created = "";

    @ColumnInfo(name = "synced")
    public String synced = "";

    @ColumnInfo(name = "is_synced")
    public boolean isSynced = false;

    @ColumnInfo(name = "flag")
    public int flag = 0;


    public String getFormatedEffDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        try {
            Date date = dateFormat.parse(effDate);
            return dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return effDate;
        }


    }
}
