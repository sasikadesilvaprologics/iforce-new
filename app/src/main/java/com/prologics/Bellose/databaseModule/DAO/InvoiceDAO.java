package com.prologics.Bellose.databaseModule.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;


import com.prologics.Bellose.databaseModule.Entity.DashboardData;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceFull;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.InvoicePrint;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReport;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReportSummery;
import com.prologics.Bellose.databaseModule.Entity.InvoiceSummery;

import java.util.List;

@Dao
public interface InvoiceDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Invoice... invoices);
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Invoice... invoices);
    @Update
    void update(Invoice... invoices);


    @Query("DELETE FROM invoice WHERE is_synced = 1 AND customer_id = :customerId  ")
    void removeSyncInvoices(int customerId);

    @Transaction
    @Query("DELETE FROM invoice")
    void deleteAll();


    @Query("SELECT * FROM invoice")
    LiveData<List<Invoice>> getAllIInvoices();

    @Query("SELECT * FROM invoice WHERE invoice_code = :code")
    LiveData<Invoice> getInvoice(int code);

    @Query("SELECT * FROM invoice WHERE customer_id = :id AND flag = 1 AND  2 ORDER BY eff_date DESC")
    LiveData<List<Invoice>> getCustomerAllInvoices(int id);

    @Query("SELECT * FROM invoice WHERE customer_id = :id AND flag = 3  ORDER BY eff_date DESC")
    LiveData<List<Invoice>> getCustomerAllPreInvoices(int id);

    @Query("SELECT * FROM invoice WHERE customer_id = :id AND flag != :draftFlag ORDER BY invoice_code DESC LIMIT 1")
    Invoice getCustomerLastInvoice(int id, int draftFlag);

    @Query("SELECT SUM(invoice_item_total_amount) as total, SUM(invoice_item_total_discount) as discount_total, invoice_item_flag " +
            "FROM invoice_item WHERE invoice_id = :invoiceId " +
            "GROUP BY invoice_item_flag")
    LiveData<List<InvoiceSummery>> getInvoiceSummery(int invoiceId);

    @Query("SELECT *  FROM invoice_item WHERE invoice_id = :code")
    LiveData<List<InvoiceItem>> getInvoiceItems(int code);

    @Query("SELECT COUNT() AS productiveCount, " +
            "COUNT(case is_synced when 0 then 1  end) as syncCount, " +
            "SUM(invoice_return_total) as todayReturns , " +
            "SUM(invoice_total) as todaySubTotal , SUM(invoice_net_total) as todaySales " +
            "from invoice   WHERE   flag != 0  ")
    LiveData<DashboardData> getProductiveInvoiceCount();


    @Query("SELECT *,customer.name  as customer_name    FROM invoice " +
            "LEFT JOIN customer ON invoice.customer_id = customer.customer_id " +
            "WHERE invoice.flag != 0   AND DATE(eff_date) BETWEEN :start AND :end ")
    LiveData<List<InvoiceReport>> getInvoiceReport(String start, String end);

    @Query("SELECT  SUM(invoice_total)  as invoiceTotalValue FROM invoice " +
            "WHERE flag != 0  AND DATE(eff_date)  BETWEEN :start AND :end")
    LiveData<InvoiceReportSummery> getInvoiceReportTotal(String start, String end);

    @Query("SELECT *,invoice_item.invoice_item_flag  FROM invoice " +
            "LEFT JOIN invoice_item ON invoice.invoice_code = invoice_item.invoice_id  " +
            " WHERE invoice_code = :invId GROUP BY invoice_item.invoice_item_flag")
    LiveData<List<InvoicePrint>>  getInvoicePrintData(int invId);

    @Query("SELECT * FROM invoice " +
            "WHERE is_synced = 0 AND flag != 0")
    LiveData<List<InvoiceFull>> getNotSyncInvoices();



}
