package com.prologics.Bellose.databaseModule.EntityViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.Entity.Self;
import com.prologics.Bellose.databaseModule.Entity.SelfWithItem;
import com.prologics.Bellose.databaseModule.Repository.SelfRepo;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.util.List;

public class SelfViewModel extends AndroidViewModel {
    private SelfRepo selfRepo;
    public SelfViewModel(@NonNull Application application) {
        super(application);
        selfRepo = new SelfRepo(application);
    }

    public void insertAll(List<Self> selves, OnTaskCompleted onTaskCompleted){
        selfRepo.insertAll(selves,onTaskCompleted);
    }

    public void refreshSelfCustomer(List<Self> selves,int customerId, OnTaskCompleted onTaskCompleted){
        selfRepo.refreshSelfCustomer(selves,customerId,onTaskCompleted);
    }

    public void update(List<Self> selves, OnTaskCompleted onTaskCompleted){
        selfRepo.update(selves,onTaskCompleted);
    }

    public LiveData<List<SelfWithItem>> getCustomerSelf(int customerId){
        return selfRepo.getCustomerSelf(customerId);
    }

    public Self getSelfById(int id){
        return selfRepo.getSelfById(id);
    }

    public Self getItem(int customerId, int itemId){

        return selfRepo.getItem(customerId,itemId);
    }

    public void deleteAll(){
        selfRepo.deleteAll();
    }
}
