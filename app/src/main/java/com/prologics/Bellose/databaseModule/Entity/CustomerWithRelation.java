package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_DRAFT_FLAG;

public class CustomerWithRelation {

    @Embedded
    public Customer customer;

    @Relation(parentColumn = "customer_id", entityColumn = "customer_id", entity = Invoice.class)
    public List<Invoice> invoices = new ArrayList<>();


    @Relation(parentColumn = "customer_types_id", entityColumn = "customer_types_id", entity = CustomerType.class)
    public List<CustomerType> customerTypes = new ArrayList<>();


    public boolean getTodayCompleteStatus(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        Date date = new Date();

        if(invoices.size() > 0){

            Invoice invoice = invoices.get(invoices.size()-1);
            try {

                Date effDate = dateFormat.parse(invoice.effDate);
                if(effDate == null){
                    return false;
                }
//                System.out.println(dateFormat.format(effDate)+"=======>>>>>>>>>>>>>"+dateFormat.format(date));
                if(invoice.flag == INVOICE_DRAFT_FLAG){
                    return false;
                }
                if(dateFormat.format(effDate).equals( dateFormat.format(date)) ){
                    return true;
                }
                else{
                    return false;
                }
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            }


        }
        else{
            return false;
        }
    }





}
