package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

public class InvoiceStoreItem {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name =  "items_id")
    public  int item_id;

    @ColumnInfo(name =  "code")
    public  String code;

    @ColumnInfo(name =  "item_name")
    public  String itemName;

    @ColumnInfo(name =  "min_price")
    public  double  minPrice;

    @ColumnInfo(name =  "max_price")
    public  double maxPrice;

    @ColumnInfo(name =  "selling_price")
    public  double sellingPrice;

    @ColumnInfo(name =  "qty")
    public double qty ;

    @ColumnInfo(name =  "qty_ns")
    public double qtyNs;

    @ColumnInfo(name =  "invoice_item_qty_s")
    public double invoice_item_qty_s ;

    @ColumnInfo(name =  "invoice_item_qty_ns")
    public double invoice_item_qty_ns;

    @ColumnInfo(name =  "invoice_item_flag")
    public int invoice_item_flag ;



}
