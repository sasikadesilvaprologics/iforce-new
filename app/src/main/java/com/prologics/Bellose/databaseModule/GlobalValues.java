package com.prologics.Bellose.databaseModule;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class GlobalValues {
    public final static boolean IS_SMALL_PRINT = false;
    public final static DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#,##0.00");
    public final static String ITEM_TYPE_KEY = "item_type_key";
    public final static String INVOICE_NUMBER_KEY = "INVOICE_NUMBER_KEY";
    public final static int RETURN_ITEM_CODE = 2;
    public final static int ITEM_CODE = 1;
    public final static int SAMPLE_ITEM_CODE = 4;
    public final static int FREE_ITEM_CODE = 3;



    public final static int INVOICE_DRAFT_FLAG = 0;
    public final static int INVOICE_SAVED_FLAG = 1;
    public final static int INVOICE_PRINTED_FLAG = 2;
    public final static int INVOICE_PRE_FLAG = 3;


    public static String getDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        Date date = new Date();

        return dateFormat.format(date);
    }


}
