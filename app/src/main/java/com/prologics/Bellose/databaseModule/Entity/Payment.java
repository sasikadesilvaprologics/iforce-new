package com.prologics.Bellose.databaseModule.Entity;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "payment")
public class Payment {

    @PrimaryKey(autoGenerate = true)
    int id ;

    @ColumnInfo(name =  "payment_type")
    @SerializedName("payment_type")
    public  int payment_type;

    @ColumnInfo(name =  "amount")
    @SerializedName("amount")
    public  double amount;

    @ColumnInfo(name =  "payment_method")
    @SerializedName("payment_method")
    public  int payment_method;

    @ColumnInfo(name =  "date")
    @SerializedName("date")
    public  int date;

    @ColumnInfo(name =  "note")
    @SerializedName("note")
    public  String note;
}
