package com.prologics.Bellose.databaseModule.Repository;

import android.content.Context;
import android.os.AsyncTask;

import com.prologics.Bellose.databaseModule.DAO.SelfUpdateDAO;
import com.prologics.Bellose.databaseModule.DataStore.AppDb;
import com.prologics.Bellose.databaseModule.Entity.SelfUpdate;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.util.List;

public class SelfUpdateRepo {
    private SelfUpdateDAO selfUpdateDAO;
    private WeakReference<Context> parentContext;
    private Shared shared;

    public SelfUpdateRepo(Context application) {
        this.parentContext = new WeakReference<>(application);
        AppDb db = AppDb.getDB(application);
        selfUpdateDAO = db.selfUpdateDAO();
        shared = Shared.getInstance(application);

    }

    public void insertAll(List<SelfUpdate> selfUpdateList, OnTaskCompleted onTaskCompleted){
        new SelfUpdateRepo.insertAsyncTask(parentContext.get(), selfUpdateDAO,onTaskCompleted).execute(selfUpdateList.toArray(new SelfUpdate[0]));
    }

    public List<SelfUpdate> getTodaySelfUpdates(String date, int customerId){
        return  selfUpdateDAO.getTodaySelfUpdates(date,customerId);
    }



    private static class insertAsyncTask extends AsyncTask<SelfUpdate,Void, Void> {

        private SelfUpdateDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;

        insertAsyncTask(Context parentContext, SelfUpdateDAO selfUpdateDAO,OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = selfUpdateDAO;
            this.listener=listener;
        }

        @Override
        protected Void doInBackground(SelfUpdate... selfUpdates) {
            asyncDAO.insertAll(selfUpdates);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }
}
