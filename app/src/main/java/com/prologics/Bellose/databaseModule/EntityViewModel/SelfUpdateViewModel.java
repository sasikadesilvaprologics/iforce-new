package com.prologics.Bellose.databaseModule.EntityViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.prologics.Bellose.databaseModule.Entity.SelfUpdate;
import com.prologics.Bellose.databaseModule.Repository.SelfUpdateRepo;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.util.List;

public class SelfUpdateViewModel extends AndroidViewModel {
    private SelfUpdateRepo selfUpdateRepo;
    public SelfUpdateViewModel(@NonNull Application application) {
        super(application);
        selfUpdateRepo = new SelfUpdateRepo(application);
    }


    public void insertAll(List<SelfUpdate> selfUpdateList, OnTaskCompleted onTaskCompleted){
        selfUpdateRepo.insertAll(selfUpdateList,onTaskCompleted);
    }

    public List<SelfUpdate> getTodaySelfUpdates(String date,int customerId){
        return  selfUpdateRepo.getTodaySelfUpdates(date,customerId);
    }

}
