package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.ArrayList;
import java.util.List;

public class InvoiceFull {



        @Embedded
        public Invoice invoice;


        @Relation(parentColumn = "invoice_code", entityColumn = "invoice_id", entity = InvoiceItem.class)
        public List<InvoiceItem> invoiceItemList = new ArrayList<>();


}
