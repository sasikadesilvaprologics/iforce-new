package com.prologics.Bellose.databaseModule.DataStore;

import android.content.Context;



import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.prologics.Bellose.databaseModule.DAO.CustomerDAO;
import com.prologics.Bellose.databaseModule.DAO.CustomerTypeDAO;
import com.prologics.Bellose.databaseModule.DAO.InvoiceDAO;
import com.prologics.Bellose.databaseModule.DAO.InvoiceItemDAO;
import com.prologics.Bellose.databaseModule.DAO.ItemDAO;
import com.prologics.Bellose.databaseModule.DAO.ParamDAO;
import com.prologics.Bellose.databaseModule.DAO.SelfDAO;
import com.prologics.Bellose.databaseModule.DAO.SelfUpdateDAO;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Entity.CustomerType;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.Entity.Param;
import com.prologics.Bellose.databaseModule.Entity.Self;
import com.prologics.Bellose.databaseModule.Entity.SelfUpdate;

@Database(entities = {
        Customer.class,
        CustomerType.class,
        Invoice.class,
        InvoiceItem.class,
        Item.class,
        Param.class,
        Self.class,
        SelfUpdate.class

}, version = 49,exportSchema = true)
public abstract class AppDb extends RoomDatabase {

    public abstract CustomerDAO customerDAO();
    public abstract CustomerTypeDAO customerTypeDAO();
    public abstract InvoiceDAO invoiceDAO();
    public abstract InvoiceItemDAO invoiceItemDAO();
    public abstract ItemDAO itemDAO();
    public abstract ParamDAO paramDAO();
    public abstract SelfDAO selfDAO();
    public abstract SelfUpdateDAO selfUpdateDAO();



    private static volatile AppDb sharedInstance;
    public static AppDb getDB(final Context context) {
        if (sharedInstance == null) {
            synchronized (AppDb.class) {
                if (sharedInstance == null) {
                    sharedInstance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDb.class, "bellose_db")
                            .fallbackToDestructiveMigration() //Dev only, else create a migration
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return sharedInstance;
    }
}
