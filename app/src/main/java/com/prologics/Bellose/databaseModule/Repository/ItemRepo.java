package com.prologics.Bellose.databaseModule.Repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.DAO.ItemDAO;
import com.prologics.Bellose.databaseModule.DataStore.AppDb;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.Entity.StockReport;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.util.List;

public class ItemRepo {
    private ItemDAO itemDAO;
    private WeakReference<Context> parentContext;
    private Shared shared;

    public ItemRepo(Context application) {
        this.parentContext = new WeakReference<>(application);
        AppDb db = AppDb.getDB(application);
        itemDAO = db.itemDAO();
        shared = Shared.getInstance(application);

    }

    public void insertItems(List<Item> items, OnTaskCompleted onTaskCompleted){

        new ItemRepo.insertAsyncTask(parentContext.get(), itemDAO,onTaskCompleted).execute(items.toArray(new Item[0]));

    }

    public void deleteAll(){
        itemDAO.deleteAll();
    }


    public LiveData<List<Item>> getAllItems(){
      return   itemDAO.getAllItems();
    }

    public Item getItem(int id){
        return  itemDAO.getItem(id);
    }

    public LiveData<List<StockReport>> getStockReport(String start , String end){
        return  itemDAO.getStockReport(start,end);
    }

    private static class insertAsyncTask extends AsyncTask<Item, Void, Void> {

        private ItemDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;

        insertAsyncTask(Context parentContext, ItemDAO itemDAO,OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = itemDAO;
            this.listener=listener;
        }

        @Override
        protected Void doInBackground(Item... items) {
            asyncDAO.insertAll(items);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


    private static class deleteAndInsertAsyncTask extends AsyncTask<Item, Void, Void> {

        private ItemDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;

        deleteAndInsertAsyncTask(Context parentContext, ItemDAO itemDAO,OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = itemDAO;
            this.listener=listener;
        }

        @Override
        protected Void doInBackground(Item... items) {
            asyncDAO.insertAll(items);


            return null;
        }



        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }
}
