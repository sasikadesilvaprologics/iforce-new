package com.prologics.Bellose.databaseModule.EntityViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.Entity.StockReport;
import com.prologics.Bellose.databaseModule.Repository.ItemRepo;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.util.List;

public class ItemViewModel extends AndroidViewModel {
    private ItemRepo itemRepo;
    public ItemViewModel(@NonNull Application application) {
        super(application);
        itemRepo = new ItemRepo(application);
    }

    public void insertItems(List<Item> items, OnTaskCompleted onTaskCompleted){
        itemRepo.insertItems(items,onTaskCompleted);
    }

    public LiveData<List<Item>> getAllItems(){
        return   itemRepo.getAllItems();
    }

    public Item getItem(int id){
        return  itemRepo.getItem(id);
    }

    public LiveData<List<StockReport>> getStockReport(String start , String end){
        return  itemRepo.getStockReport(start,end);
    }

    public void deleteAll(){
        itemRepo.deleteAll();
    }
}
