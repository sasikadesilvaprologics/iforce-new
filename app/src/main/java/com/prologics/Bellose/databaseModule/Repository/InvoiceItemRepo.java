package com.prologics.Bellose.databaseModule.Repository;

import android.content.Context;
import android.os.AsyncTask;


import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.DAO.InvoiceItemDAO;
import com.prologics.Bellose.databaseModule.DataStore.AppDb;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.InvoiceStoreItem;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.util.List;


import static com.prologics.Bellose.databaseModule.GlobalValues.ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.RETURN_ITEM_CODE;

public class InvoiceItemRepo {

    private InvoiceItemDAO invoiceItemDAO;
    private WeakReference<Context> parentContext;
    private Shared shared;

    public InvoiceItemRepo(Context application) {
        this.parentContext = new WeakReference<>(application);
        AppDb db = AppDb.getDB(application);
        invoiceItemDAO = db.invoiceItemDAO();
        shared = Shared.getInstance(application);
    }

    public void insertAll(List<InvoiceItem> invoiceItems, OnTaskCompleted onTaskCompleted) {
        new InvoiceItemRepo.insertAsyncTask(
                parentContext.get(),
                invoiceItemDAO,
                onTaskCompleted).execute(invoiceItems.toArray(new InvoiceItem[0]));
    }

    public void deleteAndInsertAll(List<InvoiceItem> invoiceItems, List<Integer> invoiceIdArray, OnTaskCompleted onTaskCompleted) {

        new InvoiceItemRepo.deleteWithInvIdsAsyncTask(
                parentContext.get(),
                invoiceItemDAO,
                invoiceIdArray,
                onTaskCompleted
        ).execute(invoiceItems.toArray(new InvoiceItem[0]));


    }

    public LiveData<List<InvoiceItem>> getItemsByInvoiceId(int id, int type) {

        return invoiceItemDAO.getItemsByInvoiceId(id, type);
    }

    public void deleteAll() {
        invoiceItemDAO.deleteAll();
    }

    public LiveData<List<InvoiceItem>> getItemsByInvoiceId(int id) {

        return invoiceItemDAO.getItemsByInvoiceId(id);
    }

    public List<InvoiceItem> getPositiveItemsByInvoiceId(int id) {
        return invoiceItemDAO.getPositiveItemsByInvoiceId(id, ITEM_CODE);
    }

    public LiveData<List<InvoiceItem>> getItemsWithoutReturns(int id) {

        return invoiceItemDAO.getItemsWithoutReturns(id, RETURN_ITEM_CODE);
    }

    public LiveData<List<InvoiceItem>> getPreSalseItems(int id) {

        return invoiceItemDAO.getPreSaleItems(id);
    }

    public void OnDelete(List<InvoiceItem> invoiceItems, OnTaskCompleted onTaskCompleted) {
        new InvoiceItemRepo.deleteAsyncTask(parentContext.get(), invoiceItemDAO, onTaskCompleted).execute(invoiceItems.toArray(new InvoiceItem[0]));
    }

    public InvoiceItem checkInvoiceItem(int invNo, int itemId, double price, double discount, int type) {
        return invoiceItemDAO.checkInvoiceItem(invNo, itemId, price, discount, type);
    }

    public LiveData<List<InvoiceStoreItem>> getItemsWithStoreData(int invoiceNo) {
        return invoiceItemDAO.getItemsWithStoreData(invoiceNo);
    }

    public void deleteAutoloadItems(int id) {
        invoiceItemDAO.deleteAutoloadItems(id, true);
    }

    private static class insertAsyncTask extends AsyncTask<InvoiceItem, Void, Void> {

        private InvoiceItemDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;


        insertAsyncTask(Context parentContext, InvoiceItemDAO invoiceItemDAO, OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = invoiceItemDAO;
            this.listener = listener;

        }

        @Override
        protected Void doInBackground(InvoiceItem... invoiceItems) {

            asyncDAO.insertAll(invoiceItems);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


    private static class deleteWithInvIdsAsyncTask extends AsyncTask<InvoiceItem, Void, Void> {

        private InvoiceItemDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;
        private List<Integer> invoiceIdArray;


        deleteWithInvIdsAsyncTask(Context parentContext, InvoiceItemDAO invoiceItemDAO, List<Integer> invoiceIdArray, OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = invoiceItemDAO;
            this.listener = listener;
            this.invoiceIdArray = invoiceIdArray;

        }

        @Override
        protected Void doInBackground(InvoiceItem... invoiceItems) {


            asyncDAO.deleteInvoices(invoiceIdArray);
            asyncDAO.insertAll(invoiceItems);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }



        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


    private static class deleteAsyncTask extends AsyncTask<InvoiceItem, Void, Void> {

        private InvoiceItemDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;

        deleteAsyncTask(Context parentContext, InvoiceItemDAO invoiceItemDAO, OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = invoiceItemDAO;
            this.listener = listener;
        }

        @Override
        protected Void doInBackground(InvoiceItem... invoiceItems) {
            asyncDAO.delete(invoiceItems);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }
}
