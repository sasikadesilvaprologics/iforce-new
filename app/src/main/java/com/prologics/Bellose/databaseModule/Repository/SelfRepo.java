package com.prologics.Bellose.databaseModule.Repository;

import android.content.Context;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.DAO.SelfDAO;
import com.prologics.Bellose.databaseModule.DataStore.AppDb;
import com.prologics.Bellose.databaseModule.Entity.Self;
import com.prologics.Bellose.databaseModule.Entity.SelfWithItem;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.util.List;

public class SelfRepo {

    private SelfDAO selfDAO;
    private WeakReference<Context> parentContext;
    private Shared shared;

    public SelfRepo(Context application) {
        this.parentContext = new WeakReference<>(application);
        AppDb db = AppDb.getDB(application);
        selfDAO = db.selfDAO();
        shared = Shared.getInstance(application);

    }

    public void insertAll(List<Self> selves, OnTaskCompleted onTaskCompleted){
        new SelfRepo.insertAsyncTask(parentContext.get(), selfDAO,onTaskCompleted).execute(selves.toArray(new Self[0]));
    }

    public void update(List<Self> selves, OnTaskCompleted onTaskCompleted){
        new SelfRepo.updateAsyncTask(parentContext.get(), selfDAO,onTaskCompleted).execute(selves.toArray(new Self[0]));
    }

    public LiveData<List<SelfWithItem>> getCustomerSelf(int customerId){
        return selfDAO.getCustomerSelf(customerId);
    }


    public void refreshSelfCustomer(List<Self> selves,int customerId, OnTaskCompleted onTaskCompleted){
        new SelfRepo.refreshCustomerAsyncTask(parentContext.get(),customerId, selfDAO,onTaskCompleted).execute(selves.toArray(new Self[0]));
    }



    public Self getSelfById(int id){
       return selfDAO.getSelfById(id);
    }

    public Self getItem(int customerId, int itemId){

        return selfDAO.getItem(customerId,itemId);
    }

    public void deleteAll(){
        selfDAO.deleteAll();
    }



    private static class insertAsyncTask extends AsyncTask<Self, Void, Void> {

        private SelfDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;

        insertAsyncTask(Context parentContext, SelfDAO selfDAO,OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = selfDAO;
            this.listener=listener;
        }

        @Override
        protected Void doInBackground(Self... selves) {
            asyncDAO.insertAll(selves);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    private static class refreshCustomerAsyncTask extends AsyncTask<Self, Void, Void> {

        private SelfDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;
        private  int customerId;

        refreshCustomerAsyncTask(Context parentContext,int customerId, SelfDAO selfDAO,OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = selfDAO;
            this.listener=listener;
            this.customerId = customerId;
        }

        @Override
        protected Void doInBackground(Self... selves) {
            asyncDAO.deleSelfsByCustomer(customerId);
            asyncDAO.insertAll(selves);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    private static class updateAsyncTask extends AsyncTask<Self, Void, Void> {

        private SelfDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;

        updateAsyncTask(Context parentContext, SelfDAO selfDAO,OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = selfDAO;
            this.listener=listener;
        }

        @Override
        protected Void doInBackground(Self... selves) {
            asyncDAO.update(selves);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }




}
