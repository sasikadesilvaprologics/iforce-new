package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.ArrayList;
import java.util.List;

public class InvoicePrint {

    @Embedded
    public Invoice invoice;

    public  int invoice_item_flag;


    @Relation(parentColumn = "invoice_code", entityColumn = "invoice_id", entity = InvoiceItem.class)
    public List<InvoiceItem> invoiceItemList = new ArrayList<>();



//    "(SELECT * FROM invoice_item  WHERE invoice_item.invoice_id = :invId AND invoice_item.invoice_item_flag = 1) as salesItems, " +
//            "(SELECT * FROM invoice_item  WHERE invoice_item.invoice_id = :invId AND invoice_item.invoice_item_flag = 4) as samplesItems, " +
//            "(SELECT * FROM invoice_item  WHERE invoice_item.invoice_id = :invId AND invoice_item.invoice_item_flag = 3) as freeItems, " +
//            "(SELECT * FROM invoice_item  WHERE invoice_item.invoice_id = :invId AND invoice_item.invoice_item_flag = 2) as returnItems " +
}
