package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "customer")
public class Customer {

    @PrimaryKey
    @ColumnInfo(name = "customer_id")
    @SerializedName("id")
    private int id;

    @ColumnInfo(name = "customer_types_id")
    @SerializedName("customer_types_id")
    private int customerTypeId = 0;

    @ColumnInfo(name = "areas_id")
    @SerializedName("areas_id")
    private int areaId = 0;

    @ColumnInfo(name = "code")
    @SerializedName("code")
    public String code;

    @ColumnInfo(name = "img_path")
    @SerializedName("cover_image")
    private String imagePath = "";

    @ColumnInfo(name = "name")
    @SerializedName("name")
    private String name = "";

    @ColumnInfo(name = "address_no")
    @SerializedName("address_no")
    private String addressNo = "";

    @ColumnInfo(name = "address_1")
    @SerializedName( "address_1")
    private String address1 = "";

    @ColumnInfo(name = "address_2")
    @SerializedName("address_2")
    private String address2 = "";

    @ColumnInfo(name = "street")
    @SerializedName("street")
    private String street = "";

    @ColumnInfo(name = "contact_name")
    @SerializedName( "contact_name")
    private String contactName = "";

    @ColumnInfo(name = "mobile")
    @SerializedName(  "mobile")
    private String mobile = "";

    @ColumnInfo(name = "landline")
    @SerializedName("landline")
    private String landline = "";

    @ColumnInfo(name = "latitude")
    @SerializedName("latitude")
    private String latitude = "0.0";

    @ColumnInfo(name = "longitude")
    @SerializedName("longitude")
    private String longitude = "0.0";

    @ColumnInfo(name = "dob")
    private String dob = "";

    @ColumnInfo(name = "remarks")
    private String remarks = "";

    @ColumnInfo(name = "created")
    @SerializedName("created")
    private String created = "";

    @ColumnInfo(name = "is_approved")
    @SerializedName("is_approved")
    public Integer is_approved = 0;


    @ColumnInfo(name = "is_synced")
    @SerializedName("is_synced")
    public Integer is_synced = 0;

    @ColumnInfo(name = "discount")
    @SerializedName("discount")
    public double discount = 0;

    @ColumnInfo(name = "synced")
    @SerializedName("synced")
    public String synced = "";






    private int customer_type_id;

    private String customer_type_name = "N/A";

    @ForeignKey(entity = CustomerType.class, parentColumns = "customer_types_id", childColumns = "id")


    public static Customer fromJSON(

    ) {
        Customer customer = new Customer();


        return customer;
    }



    public int getAreaId() {
        return areaId;
    }

    public int getCustomerTypeId() {
        return customerTypeId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public int getId() {
        return id;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getAddressNo() {
        return addressNo;
    }

    public String getAndline() {
        return this.landline;
    }

    public String getContactName() {
        return contactName;
    }

    public String getCreated() {
        return created;
    }

    public String getDob() {
        return dob;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getMobile() {
        return mobile;
    }

    public String getName() {
        return name;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getStreet() {
        return street;
    }

    public String getLandline() {
        return landline;
    }

    public String getCustomer_type_name() {
        return customer_type_name;
    }

    public int getCustomer_type_id() {
        return customer_type_id;
    }

    public Integer getIs_approved() {
        return is_approved;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public void setAddressNo(String addressNo) {
        this.addressNo = addressNo;
    }

    public void setLandline(String andline) {
        this.landline = andline;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setCustomerTypeId(int customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void setCustomer_type_id(int customer_type_id) {
        this.customer_type_id = customer_type_id;
    }

    public void setCustomer_type_name(String customer_type_name) {
        this.customer_type_name = customer_type_name;
    }

    public void setFustomerFlag(Integer is_approved) {
        this.is_approved = is_approved;
    }

    public String getFormatedAddress(){
        return addressNo+","+address1+","+address2;
    }
}
