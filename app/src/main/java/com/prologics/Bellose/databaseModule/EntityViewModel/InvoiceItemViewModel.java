package com.prologics.Bellose.databaseModule.EntityViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.InvoiceStoreItem;
import com.prologics.Bellose.databaseModule.Repository.InvoiceItemRepo;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.util.List;



public class InvoiceItemViewModel extends AndroidViewModel {
    private InvoiceItemRepo invoiceItemrepo;
    public InvoiceItemViewModel(@NonNull Application application) {
        super(application);
        invoiceItemrepo = new InvoiceItemRepo(application);
    }

    public void insertAll(List<InvoiceItem> invoiceItems, OnTaskCompleted onTaskCompleted){
        invoiceItemrepo.insertAll(invoiceItems,onTaskCompleted);
    }

    public void deleteAndInsertAll(List<InvoiceItem> invoiceItems, List<Integer> invoiceIdArray, OnTaskCompleted onTaskCompleted){
        invoiceItemrepo.deleteAndInsertAll(invoiceItems,invoiceIdArray,onTaskCompleted);
    }

    public LiveData<List<InvoiceItem>> getItemsByInvoiceId(int id, int type){
        return invoiceItemrepo.getItemsByInvoiceId(id,type);
    }

    public LiveData<List<InvoiceItem>> getItemsByInvoiceId(int id){
        return invoiceItemrepo.getItemsByInvoiceId(id);
    }

    public List<InvoiceItem> getPositiveItemsByInvoiceId(int id){
        return invoiceItemrepo.getPositiveItemsByInvoiceId(id);
    }

    public LiveData<List<InvoiceItem>> getItemsWithoutReturns(int id){

        return invoiceItemrepo.getItemsWithoutReturns(id);
    }

    public LiveData<List<InvoiceItem>> getPreSalseItems(int id){

        return invoiceItemrepo.getPreSalseItems(id);
    }

    public void onDelete(List<InvoiceItem> invoiceItems, OnTaskCompleted onTaskCompleted){
        invoiceItemrepo.OnDelete(invoiceItems,onTaskCompleted);
    }

    public void deleteAutoloadItems(int invoiceId){
        invoiceItemrepo.deleteAutoloadItems(invoiceId);
    }

    public InvoiceItem checkInvoiceItem(int invNo,int itemId, double price, double discount,int type){
        return invoiceItemrepo.checkInvoiceItem(invNo,itemId,price,discount, type);
    }

    public LiveData<List<InvoiceStoreItem>> getItemsWithStoreData(int invoiceNo){
        return  invoiceItemrepo.getItemsWithStoreData(invoiceNo);
    }

    public void deleteAll(){
        invoiceItemrepo.deleteAll();
    }
}
