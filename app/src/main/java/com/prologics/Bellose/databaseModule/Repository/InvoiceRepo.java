package com.prologics.Bellose.databaseModule.Repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.DAO.InvoiceDAO;
import com.prologics.Bellose.databaseModule.DataStore.AppDb;
import com.prologics.Bellose.databaseModule.Entity.DashboardData;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceFull;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.InvoicePrint;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReport;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReportSummery;
import com.prologics.Bellose.databaseModule.Entity.InvoiceSummery;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_DRAFT_FLAG;

public class InvoiceRepo {

    private InvoiceDAO invoiceDAO;
    private WeakReference<Context> parentContext;
    private Shared shared;
    private static int UPDATE = 3, INSERT = 1, REPLACEALL =2, DOWNLOADED = 4;



    public InvoiceRepo(Context application) {
        this.parentContext = new WeakReference<>(application);
        AppDb db = AppDb.getDB(application);
        invoiceDAO = db.invoiceDAO();
        shared = Shared.getInstance(application);

    }

    public void insert(List<Invoice> invoices, OnTaskCompleted onTaskCompleted){
        System.out.println("jfhhdhh=======?????????");
        new InvoiceRepo.insertAsyncTask(
                parentContext.get(),
                invoiceDAO,
                onTaskCompleted,
                INSERT
        ).execute(invoices.toArray(new Invoice[0]));
    }

    public void insertAll(List<Invoice> invoices, OnTaskCompleted onTaskCompleted){
        new InvoiceRepo.insertAsyncTask(
                parentContext.get(),
                invoiceDAO,
                onTaskCompleted,
                REPLACEALL
        ).execute(invoices.toArray(new Invoice[0]));
    }

    public void update(List<Invoice> invoices, OnTaskCompleted onTaskCompleted){
        new InvoiceRepo.insertAsyncTask(
                parentContext.get(),
                invoiceDAO,
                onTaskCompleted,
                UPDATE
        ).execute(invoices.toArray(new Invoice[0]));
    }

    public void insertDownloaded(List<Invoice> invoices, OnTaskCompleted onTaskCompleted){
        new InvoiceRepo.insertAsyncTask(
                parentContext.get(),
                invoiceDAO,
                onTaskCompleted,
                DOWNLOADED
        ).execute(invoices.toArray(new Invoice[0]));
    }

    public void deleteAll(){
        invoiceDAO.deleteAll();
    }

    public LiveData<List<Invoice>> getAllInvoices(){
        return  invoiceDAO.getAllIInvoices();
    }

    public LiveData<Invoice> getInvoiceById(int id){
        return invoiceDAO.getInvoice(id);
    }

    public LiveData<List<Invoice>> getCustomerAllInvoices(int customerId){
        return invoiceDAO.getCustomerAllInvoices(customerId);
    }

    public LiveData<List<Invoice>> getCustomerAllPreInvoices(int customerId){
        return invoiceDAO.getCustomerAllPreInvoices(customerId);
    }

    public Invoice getCustomerLastInvoice(int customerId){
        return invoiceDAO.getCustomerLastInvoice(customerId,INVOICE_DRAFT_FLAG);
    }

    public LiveData<List<InvoiceSummery>> getInvoiceSummery(int invId){
        return invoiceDAO.getInvoiceSummery(invId);
    }

    public LiveData<List<InvoiceItem>> getInvoiceItems(int invNo){
        return invoiceDAO.getInvoiceItems(invNo);
    }

    public LiveData<DashboardData>  getProductiveInvoiceCount(String date){

        return invoiceDAO. getProductiveInvoiceCount();
    }

    public LiveData<List<InvoiceReport>> getInvoiceReport(String start, String end){
        return  invoiceDAO.getInvoiceReport(start, end);
    }

    public LiveData<InvoiceReportSummery> getInvoiceReportTotal(String start, String end){
        return invoiceDAO.getInvoiceReportTotal(start,end);
    }

    public LiveData<List<InvoicePrint>>  getInvoicePrintData(int invId){
        return  invoiceDAO.getInvoicePrintData(invId);
    }

    public LiveData<List<InvoiceFull>> getNotSyncInvoices(){
        return invoiceDAO.getNotSyncInvoices();
    }



    private static class insertAsyncTask extends AsyncTask<Invoice, Void, Void> {

        private InvoiceDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;
        private int taskType = 0;

        insertAsyncTask(Context parentContext, InvoiceDAO invoiceDAO,OnTaskCompleted listener, int taskType) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = invoiceDAO;
            this.listener=listener;
            this.taskType = taskType;
        }

        @Override
        protected Void doInBackground(Invoice... invoices) {

            if(taskType == INSERT){
                asyncDAO.insert(invoices);
            }
            else if(taskType == UPDATE){
                asyncDAO.update(invoices);
            }
            else if(taskType == REPLACEALL){
                asyncDAO.insertAll(invoices);
            }
            else if(taskType == DOWNLOADED){
                if(invoices.length > 0){
                    asyncDAO.removeSyncInvoices(invoices[0].customerId);
                }

                asyncDAO.insert(invoices);
            }



            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


}
