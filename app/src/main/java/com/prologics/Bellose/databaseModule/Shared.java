package com.prologics.Bellose.databaseModule;

import android.content.Context;
import android.content.SharedPreferences;

import com.prologics.Bellose.network.Entity.RootListResponse;
import com.prologics.Bellose.network.Entity.login.LoginUserApiEntity;

public class Shared {

    private static Shared instance = null;

    private SharedPreferences preferences;

    private Shared(Context context) {
        preferences = context.getSharedPreferences("user_settings", Context.MODE_PRIVATE);
    }

    //Singleton
    public static Shared getInstance(Context context) {
        if (instance == null) {
            instance = new Shared(context);
        }

        return instance;
    }

    public void setCurrentInv(int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("currentInvoiceNo", value).apply();
    }

    public int getCurrentInv() {
        return preferences.getInt("currentInvoiceNo", 456);
    }

    public void setSelectedInv(int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("selectedInvoiceNo", value).apply();
    }

    public int getSelectedInv() {
        return preferences.getInt("selectedInvoiceNo", 456);
    }

    public void setLoginStatus(Boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("isLoginCompleted", value).apply();
    }

    public Boolean getLoginStatus() {
        return preferences.getBoolean("isLoginCompleted", false);
    }

    public void setCurrentUser(LoginUserApiEntity loginUserApiEntity){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("userCode", loginUserApiEntity.code)
                .putString("userName", loginUserApiEntity.name)
                .putString("userDeviceId", loginUserApiEntity.deviceId)
                .putString("userTarget", loginUserApiEntity.target)
                .apply();
    }

    public LoginUserApiEntity getCurrentUser(){
        LoginUserApiEntity loginUserApiEntity = new LoginUserApiEntity();
        loginUserApiEntity.code = preferences.getString("userCode","");
        loginUserApiEntity.name = preferences.getString("userName","");
        loginUserApiEntity.deviceId = preferences.getString("userDeviceId","");
        loginUserApiEntity.target = preferences.getString("userTarget","");

        return loginUserApiEntity;

    }

    public void setSelectedRoot(RootListResponse.Data area) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("rootName", area.name)
                .putString("rootId", String.valueOf(area.areaId))
                .putString("rootLastDate", area.last_date)
                .apply();
    }

    public Area getSelectedRoot() {

        Area area = new Area();
        area.name = preferences.getString("rootName","");
        area.id = preferences.getString("rootId","");
        area.last_date = preferences.getString("rootLastDate","");


      return  area;
    }

    public void setRootStatus(Boolean value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("rootStatusValue", value).apply();
    }

    public Boolean getRootStatus(){
        return preferences.getBoolean("rootStatusValue", false);
    }

    public void setSelectedCustomerId(int id){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("selectedCustomerId", id).apply();
    }

    public int getSelectedCustomerId(){
        return preferences.getInt("selectedCustomerId", 0);
    }

    public void setPrinterName(String name){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("printerName", name).apply();
    }

    public String getPrinterName(){
        return  preferences.getString("printerName","");
    }


    public  class  Area{


        public String id;

        public String name;

        public String last_date;



    }

    public  class  Location{


        public double latitude = 0.00;
        public double longitude = 0.00;



    }


    public void setLocation(double latitude,double longitude){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("currentLatitude", String.valueOf(latitude));
        editor.putString("currentLongitude", String.valueOf(longitude)).apply();
    }

    public Location getLocation(){
        Location location = new Location();
        location.latitude = Double.parseDouble(preferences.getString("currentLatitude","0.00"));
        location.longitude = Double.parseDouble(preferences.getString("currentLongitude","0.00"));

        return location;
    }


    public  String getAgencyName(){
        return "";
    }

    public String getAgencyAddress(){
        return "";
    }

    public String getAgencyMobileNo(){
        return  "";
    }

    public String getTerritory(){
        return "";
    }


    public void setPrintEnable(boolean value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("isPrintModeEnable",value)
        .apply();
    }

    public boolean isPrintModeEnabled(){
        return  preferences.getBoolean("isPrintModeEnable",false);
    }

    public void setHaveUnSyncInvoices(Boolean value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("isHaveUnSyncInvoices",value)
                .apply();
    }

    public boolean isHaveUnSyncInVoices(){
        return  preferences.getBoolean("isHaveUnSyncInvoices",false);
    }


    public void setSelectedCustomerDisc(double value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("selectedCustomerDisc", String.valueOf(value))
                .apply();
    }

    public double getSelectedCustomerDisc(){
        double value = Double.parseDouble(preferences.getString("selectedCustomerDisc","0"));
        if(value > 100){
            return  0.0;
        }
        else {
            return value;
        }


    }


}
