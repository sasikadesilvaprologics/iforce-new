package com.prologics.Bellose.databaseModule.EntityViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.Entity.DashboardData;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceFull;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.InvoicePrint;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReport;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReportSummery;
import com.prologics.Bellose.databaseModule.Entity.InvoiceSummery;
import com.prologics.Bellose.databaseModule.Repository.InvoiceRepo;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.util.List;

public class InvoiceViewModel extends AndroidViewModel {
    private InvoiceRepo invoiceRepo;
    public InvoiceViewModel(@NonNull Application application) {
        super(application);
        invoiceRepo = new InvoiceRepo(application);
    }

    public void insertAll(List<Invoice> invoices, OnTaskCompleted onTaskCompleted) {
        invoiceRepo.insertAll(invoices,onTaskCompleted);
    }

    public void insert(List<Invoice> invoices, OnTaskCompleted onTaskCompleted) {
        invoiceRepo.insert(invoices,onTaskCompleted);
    }

    public void insertDownloaded(List<Invoice> invoices, OnTaskCompleted onTaskCompleted) {
        invoiceRepo.insertDownloaded(invoices,onTaskCompleted);
    }

    public void update(List<Invoice> invoices, OnTaskCompleted onTaskCompleted) {
        invoiceRepo.update(invoices,onTaskCompleted);
    }

    public LiveData<List<Invoice>> getAllInvoices(){
        return  invoiceRepo.getAllInvoices();
    }

    public LiveData<Invoice> getInvoiceById(int id){
        return invoiceRepo.getInvoiceById(id);
    }
    public LiveData<List<Invoice>> getCustomerAllInvoices(int id){
        return invoiceRepo.getCustomerAllInvoices(id);
    }

    public LiveData<List<Invoice>> getCustomerAllPreInvoices(int customerId){
        return invoiceRepo.getCustomerAllPreInvoices(customerId);
    }

    public Invoice getCustomerLastInvoice(int customerId){
        return invoiceRepo.getCustomerLastInvoice(customerId);
    }

    public LiveData<List<InvoiceSummery>> getInvoiceSummery(int invId){
        return invoiceRepo.getInvoiceSummery(invId);
    }

    public LiveData<List<InvoiceItem>> getInvoiceItems(int invNo){
        return invoiceRepo.getInvoiceItems(invNo);
    }

    public LiveData<DashboardData>  getProductiveInvoiceCount(String date){
        return invoiceRepo.getProductiveInvoiceCount(date);
    }

    public LiveData<List<InvoiceReport>> getInvoiceReport(String start, String end){
        return invoiceRepo.getInvoiceReport(start, end);
    }

    public LiveData<InvoiceReportSummery> getInvoiceReportTotal(String start, String end){
        return invoiceRepo.getInvoiceReportTotal(start,end);
    }

    public LiveData<List<InvoicePrint>>  getInvoicePrintData(int invId){
        return  invoiceRepo.getInvoicePrintData(invId);
    }

    public LiveData<List<InvoiceFull>> getNotSyncInvoices(){
        return invoiceRepo.getNotSyncInvoices();
    }

    public void deleteAll(){
        invoiceRepo.deleteAll();
    }


}
