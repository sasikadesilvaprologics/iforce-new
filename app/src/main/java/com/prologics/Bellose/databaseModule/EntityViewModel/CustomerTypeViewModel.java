package com.prologics.Bellose.databaseModule.EntityViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.Entity.CustomerType;
import com.prologics.Bellose.databaseModule.Repository.CustomerTypeRepo;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.util.List;

public class CustomerTypeViewModel extends AndroidViewModel {
    private CustomerTypeRepo customerTypeRepo;
    public CustomerTypeViewModel(@NonNull Application application) {
        super(application);
        customerTypeRepo = new CustomerTypeRepo(application);
    }

    public  void insertDummy(List<CustomerType> customerTypes,OnTaskCompleted onTaskCompleted){

        customerTypeRepo.insertTypeList(customerTypes,onTaskCompleted);
    }

    public LiveData<List<CustomerType>> getAllTypes(){
        return customerTypeRepo.getAllTypes();
    }

}
