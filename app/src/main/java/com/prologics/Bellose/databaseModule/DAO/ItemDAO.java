package com.prologics.Bellose.databaseModule.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;


import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.Entity.StockReport;

import java.util.List;

@Dao
public interface ItemDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Item... items);

    @Delete
    void delete(Item... items);

    @Query("SELECT * FROM store_items")
    LiveData<List<Item>> getAllItems();

    @Transaction
    @Query("DELETE FROM store_items")
    void deleteAll();

    @Query("SELECT * FROM store_items WHERE items_id = :id")
    Item getItem(int id);

    @Query("SELECT store_items.item_name, store_items.updated_at as updated_at ,(" +
            "SELECT SUM(invoice_item.invoice_item_qty_s) FROM invoice_item WHERE invoice_item.items_id = store_items.items_id AND invoice_item_flag=1 AND DATE(updated_at) BETWEEN :start AND :end " +
            ") as sales_count, " +
            "(SELECT SUM(invoice_item.invoice_item_qty_s) FROM invoice_item WHERE invoice_item.items_id = store_items.items_id AND invoice_item_flag=3  AND DATE(updated_at) BETWEEN :start AND :end" +
            ") as free_count ," +
            "(SELECT SUM(invoice_item.invoice_item_qty_s) FROM invoice_item WHERE invoice_item.items_id = store_items.items_id AND invoice_item_flag=4 AND DATE(updated_at) BETWEEN :start AND :end" +
            ") as sample_count  ," +
            "(SELECT SUM(invoice_item.invoice_item_qty_ns) FROM invoice_item  WHERE invoice_item.items_id = store_items.items_id AND invoice_item_flag=2 AND DATE(updated_at) BETWEEN :start AND :end" +
            ") as non_salable_count  ," +
            "(SELECT SUM(invoice_item.invoice_item_qty_s) FROM invoice_item  WHERE invoice_item.items_id = store_items.items_id AND invoice_item_flag=2 AND DATE(updated_at) BETWEEN :start AND :end" +
            ") as salable_count  " +
            "FROM store_items  ")
    LiveData<List<StockReport>> getStockReport(String start, String end);
}
