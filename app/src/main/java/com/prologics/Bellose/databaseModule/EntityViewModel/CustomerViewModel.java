package com.prologics.Bellose.databaseModule.EntityViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.Entity.CustomerWithRelation;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Repository.CustomerRepo;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.util.List;

public class CustomerViewModel extends AndroidViewModel {
    private CustomerRepo customerRepo;
    public CustomerViewModel(@NonNull Application application) {
        super(application);
        customerRepo = new CustomerRepo(application);
    }

    public  void insertDummy(){

        customerRepo.insertDummyData();
    }
    public void insertNewCustomer(Customer customer, OnTaskCompleted onTaskCompleted){
        System.out.println("=======???>>>>>>>656565>");
        customerRepo.insertNewCustomer(customer,onTaskCompleted);

    }

    public void insertAll(List<Customer> customer, OnTaskCompleted onTaskCompleted) {

        customerRepo.insertAll(customer,onTaskCompleted);
    }

    public void deleteAll(){
        customerRepo.deleteAll();
    }


    public LiveData<List<CustomerWithRelation>> getAll(){
        return customerRepo.getAllCustomers() ;
    }
    public Customer getCustomer(int id){
        return customerRepo.getCustomer(id);
    }
}
