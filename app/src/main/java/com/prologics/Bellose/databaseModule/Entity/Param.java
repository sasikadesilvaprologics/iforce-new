package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "param")
public class Param {

    @PrimaryKey(autoGenerate = true)
    public  int id;

    @ColumnInfo(name = "key")
    public String key;

    @ColumnInfo(name = "value")
    public String value;
}
