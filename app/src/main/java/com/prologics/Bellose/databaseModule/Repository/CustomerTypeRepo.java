package com.prologics.Bellose.databaseModule.Repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.prologics.Bellose.databaseModule.DAO.CustomerTypeDAO;
import com.prologics.Bellose.databaseModule.DataStore.AppDb;
import com.prologics.Bellose.databaseModule.Entity.CustomerType;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.util.List;

public class CustomerTypeRepo {
    private CustomerTypeDAO customerTypeDAO;
    private WeakReference<Context> parentContext;
    private Shared shared;

    public CustomerTypeRepo(Context application) {
        this.parentContext = new WeakReference<>(application);
        AppDb db = AppDb.getDB(application);
        customerTypeDAO = db.customerTypeDAO();
        shared = Shared.getInstance(application);

    }

    public void insertTypeList(List<CustomerType> customerTypeList , OnTaskCompleted onTaskCompleted){
        new CustomerTypeRepo.insertAsyncTask(parentContext.get(), customerTypeDAO,onTaskCompleted).execute(customerTypeList.toArray(new CustomerType[0]));
    }

    public LiveData<List<CustomerType>> getAllTypes(){
        return customerTypeDAO.getAllTypes();
    }


    private static class insertAsyncTask extends AsyncTask<CustomerType, Void, Void> {

        private CustomerTypeDAO asyncDAO;
        private WeakReference<Context> parentContext;
        private OnTaskCompleted listener;

        insertAsyncTask(Context parentContext, CustomerTypeDAO customerTypeDAO,OnTaskCompleted listener) {
            this.parentContext = new WeakReference<>(parentContext);
            this.asyncDAO = customerTypeDAO;
            this.listener=listener;
        }

        @Override
        protected Void doInBackground(CustomerType... customerTypes) {
            asyncDAO.insertAll(customerTypes);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onTaskCompleted();
        }
    }
}
