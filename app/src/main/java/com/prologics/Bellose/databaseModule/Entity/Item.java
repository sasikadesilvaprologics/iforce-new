package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static com.prologics.Bellose.databaseModule.GlobalValues.getDate;

@Entity(tableName = "store_items", indices = {@Index(value = {"items_id"},
        unique = true)})
public class Item {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name =  "items_id")
    @SerializedName("items_id")
    public  int item_id;

    @ColumnInfo(name =  "code")
    @SerializedName("code")
    public  String code;

    @ColumnInfo(name =  "item_name")
    @SerializedName("item_name")
    public  String itemName;

    @ColumnInfo(name =  "min_price")
    @SerializedName("min_price")
    public  double  minPrice;

    @ColumnInfo(name =  "max_price")
    @SerializedName("max_price")
    public  double maxPrice;

    @ColumnInfo(name =  "selling_price")
    @SerializedName("selling_price")
    public  double sellingPrice;

    @ColumnInfo(name =  "qty")
    @SerializedName("qty")
    public double qty ;

    @ColumnInfo(name =  "qty_ns")
    @SerializedName("qty_ns")
    public double qtyNs;

    @ColumnInfo(name = "updated_at")
    public String updatedAt = getDate();





}
