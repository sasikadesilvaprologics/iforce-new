package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "self")
public class Self {



    @PrimaryKey(autoGenerate = true)
    public  int self_id;

    @ColumnInfo(name =  "customer_id")
    public  int customerId ;

    @ColumnInfo(name =  "items_id")
    public  int itemsid;

    @ColumnInfo(name =  "own_qty")
    public  double ownQty;

    @ColumnInfo(name =  "other_qty")
    public  double other_qty;



}
