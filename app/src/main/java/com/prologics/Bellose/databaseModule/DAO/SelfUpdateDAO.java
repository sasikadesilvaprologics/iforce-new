package com.prologics.Bellose.databaseModule.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.prologics.Bellose.databaseModule.Entity.SelfUpdate;

import java.util.List;

@Dao
public interface SelfUpdateDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(SelfUpdate... selfUpdates);

    @Query("SELECT * FROM self_update   WHERE DATE(updated) LIKE :date AND customer_id = :customerId")
    List<SelfUpdate> getTodaySelfUpdates(String date,int customerId);
}
