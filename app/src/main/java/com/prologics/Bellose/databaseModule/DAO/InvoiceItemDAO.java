package com.prologics.Bellose.databaseModule.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.InvoiceStoreItem;

import java.util.List;

@Dao
public interface InvoiceItemDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(InvoiceItem... invoiceItems);


    @Query("SELECT * FROM invoice_item")
    LiveData<List<InvoiceItem>> getAllIInvoices();



    @Query("DELETE FROM invoice_item WHERE invoice_id IN (:invoiceIds)")
    void deleteInvoices(List<Integer> invoiceIds);

    @Transaction
    @Query("DELETE FROM invoice_item")
    void deleteAll();

    @Query("SELECT * FROM invoice_item " +
            "LEFT JOIN store_items " +
            "WHERE invoice_item.items_id = store_items.items_id AND  invoice_item.invoice_id = :id AND invoice_item.invoice_item_flag = :type ")
    LiveData<List<InvoiceItem>> getItemsByInvoiceId(int id, int type);

    @Query("SELECT * FROM invoice_item " +
            "LEFT JOIN store_items " +
            "WHERE invoice_item.items_id = store_items.items_id AND  invoice_item.invoice_id = :id   ")
    LiveData<List<InvoiceItem>> getItemsByInvoiceId(int id);

    @Query("SELECT * FROM invoice_item " +
            "LEFT JOIN store_items " +
            "WHERE invoice_item.items_id = store_items.items_id AND  invoice_item.invoice_id = :id  AND invoice_item.invoice_item_flag = :itemFlag  ")
    List<InvoiceItem> getPositiveItemsByInvoiceId(int id,int itemFlag);

    @Query("SELECT * FROM invoice_item " +
            "LEFT JOIN store_items " +
            "WHERE invoice_item.items_id = store_items.items_id " +
            "AND  invoice_item.invoice_id = :id AND invoice_item.invoice_item_flag != :type  ")
    LiveData<List<InvoiceItem>> getItemsWithoutReturns(int id, int type);

    @Query("SELECT * FROM invoice_item " +
            "LEFT JOIN store_items " +
            "WHERE invoice_item.items_id = store_items.items_id " +
            "AND  invoice_item.invoice_id = :id AND  invoice_item.is_pre_sale = 1 ")
    LiveData<List<InvoiceItem>> getPreSaleItems(int id);

    @Query("SELECT * from invoice_item " +
            "WHERE items_id = :itemId " +
            "AND invoice_id = :invNo " +
            "AND invoice_item_mrp = :price " +
            "AND invoice_item_discount = :discount " +
            "AND invoice_item_flag = :type LIMIT 1")
    InvoiceItem checkInvoiceItem(int invNo,int itemId, double price, double discount, int type );

    @Query("DELETE FROM invoice_item " +
            "WHERE invoice_id = :invoiceId " +
            "AND invoice_item_is_autoLoad = :isAutoload")
    void deleteAutoloadItems(int invoiceId , boolean isAutoload);


    @Delete
    void delete(InvoiceItem... invoiceItems);

    @Update
    void update(InvoiceItem... invoiceItems);


    @Query("SELECT * FROM invoice_item " +
            "LEFT JOIN store_items " +
            "WHERE invoice_item.items_id = store_items.items_id AND invoice_item.invoice_id = :invoiceId")
    LiveData<List<InvoiceStoreItem>> getItemsWithStoreData(int invoiceId);

}
