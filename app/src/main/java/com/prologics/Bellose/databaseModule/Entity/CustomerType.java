package com.prologics.Bellose.databaseModule.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "customer_type")
public class CustomerType {

    @PrimaryKey
    @ColumnInfo(name = "customer_types_id")
    @SerializedName("customer_types_id")
    public  int customer_type_id;

    @ColumnInfo(name =  "customer_type_name")
    @SerializedName("name")
    public  String customer_type_name;
}
