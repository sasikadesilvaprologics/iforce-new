package com.prologics.Bellose.databaseModule.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.prologics.Bellose.databaseModule.Entity.Self;
import com.prologics.Bellose.databaseModule.Entity.SelfWithItem;

import java.util.List;

@Dao
public interface SelfDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Self... selves);

    @Update
    void update(Self... selves);

    @Query("SELECT * FROM self WHERE  customer_id = :customerId AND items_id = :itemId LIMIT 1")
    Self getItem(int customerId, int itemId);

    @Transaction
    @Query("DELETE FROM self")
    void deleteAll();


    @Transaction
    @Query("SELECT * FROM self " +
            "LEFT JOIN store_items " +
            "WHERE self.items_id = store_items.items_id " +
            "AND customer_id = :customerId ORDER BY self.self_id DESC LIMIT 50")
    LiveData<List<SelfWithItem>> getCustomerSelf(int customerId);

    @Query("SELECT * FROM self WHERE self_id = :id")
    Self getSelfById(int id);

    @Query("DELETE FROM self WHERE customer_id = :customerId")
    void deleSelfsByCustomer(int customerId);
}
