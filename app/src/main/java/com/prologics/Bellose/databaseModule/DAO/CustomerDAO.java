package com.prologics.Bellose.databaseModule.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.prologics.Bellose.databaseModule.Entity.CustomerWithRelation;
import com.prologics.Bellose.databaseModule.Entity.Customer;

import java.util.List;


@Dao
public interface CustomerDAO {

    @Query("SELECT * FROM customer")
    LiveData<List<Customer>> getAllCustomers();

    @Transaction
    @Query("SELECT * FROM customer LEFT JOIN customer_type WHERE customer.areas_id = :id AND customer.customer_types_id = customer_type.customer_types_id ")
    LiveData<List<CustomerWithRelation>> getAllCustomersByRoot(int id);

    @Query("SELECT * FROM customer LEFT JOIN customer_type WHERE customer.customer_id = :id AND customer.customer_types_id = customer_type.customer_types_id LIMIT 1")
    Customer getCustomer(int id);


    @Transaction
    @Query("DELETE FROM customer")
    void deleteAll();



//    @Transaction
//    @Query("SELECT * FROM customer WHERE customer.areas_id = :id " )
//
//    LiveData<List<CustomerWithRelation>> getAllCustomersByRoot1(int id, String date);

//    @Query("SELECT * FROM customer " +
//            "WHERE invoice_code IN" +
//            "(SELECT invoice_code FROM invoice WHERE customer.id = invoice.customer_id AND " +
//            "invoice.flag != 0 AND DATE(invoice.eff_date) LIKE :date ) " +
//            " AND customer.areas_id = :id " )


//    @Query("SELECT * FROM customer " +
//            " LEFT JOIN invoice ON customer.customer_id = invoice.customer_id " +
//            "AND invoice.flag != 0 AND DATE(invoice.eff_date) LIKE :date " +
//            " WHERE customer.areas_id = :id " )


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Customer... customer);
}
