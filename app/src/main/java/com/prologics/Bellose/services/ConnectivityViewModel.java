package com.prologics.Bellose.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.lang.ref.WeakReference;

public class ConnectivityViewModel extends ViewModel {
    private MutableLiveData<Boolean> isNetworkConnected;
    public static ConnectivityViewModel connectivityViewModel;
    private WeakReference<Context> weakReference;

    public static ConnectivityViewModel getInstance(Context context) {
        if(connectivityViewModel == null){
            connectivityViewModel = new ConnectivityViewModel(context);
        }

        return connectivityViewModel;
    }

    public ConnectivityViewModel(Context context){
        weakReference = new WeakReference<>(context);
        isNetworkConnected = new MutableLiveData<>();
        isNetworkConnected.setValue(isNetworkAvailable(weakReference.get()));



    }

    public LiveData<Boolean> getNetworkStatus() {
        return isNetworkConnected;
    }

    public Boolean onCheckConnectivity(Context context){
        Boolean value = isNetworkAvailable(context);
        isNetworkConnected.setValue(value);

        return value;
    }

    public static boolean isNetworkAvailable(Context context) {
        if(context == null)  return false;


        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {


            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        return true;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return true;
                    }  else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)){
                        return true;
                    }
                }
            }

            else {

                try {
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                        Log.i("update_statut", "Network is available : true");
                        return true;
                    }
                } catch (Exception e) {
                    Log.i("update_statut", "" + e.getMessage());
                }
            }
        }
        Log.i("update_statut","Network is available : FALSE ");
        return false;
    }
}
