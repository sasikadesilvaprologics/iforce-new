package com.prologics.Bellose.services;

import com.prologics.Bellose.BuildConfig;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.Entity.Self;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.helper.callbacks.OnApiResponse;
import com.prologics.Bellose.helper.callbacks.OnCheckAppVersion;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.helper.callbacks.OntaskCompleteInvoice;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.Entity.CustomerArrayResponse;
import com.prologics.Bellose.network.Entity.StockListResponse;
import com.prologics.Bellose.network.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataSyncService {

    public static void runStockDataListApiService(ItemViewModel itemViewModel, int deviceId, OnApiResponse onApiResponse) {
        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<StockListResponse> stringCall = getNoticeDataService.getStockList(deviceId);
        stringCall.enqueue(new Callback<StockListResponse>() {
            @Override
            public void onResponse(Call<StockListResponse> call, Response<StockListResponse> response) {

                if (response.isSuccessful()) {

                    onApiResponse.onSuccess("success");

                    List<Item> items = response.body().data;
                    itemViewModel.insertItems(items, new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted() {
                            System.out.println("========>>>>>>?????====" + items.get(0).item_id);
                            System.out.println("========>>>>>>?????====" + response.body().data.size());
                        }
                    });
                } else {
                    onApiResponse.onError("");
                    System.out.println("========>>>>>>???successerror??====" + response.code());
                }

            }

            @Override
            public void onFailure(Call<StockListResponse> call, Throwable t) {
                onApiResponse.onError("");
            }
        });
    }


    public static void getCustomers(int areaId, CustomerViewModel customerViewModel) {
        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<CustomerArrayResponse> stringCall = getNoticeDataService.getCustomers(areaId);
        stringCall.enqueue(new Callback<CustomerArrayResponse>() {
            @Override
            public void onResponse(Call<CustomerArrayResponse> call, Response<CustomerArrayResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body().success) {
                        if (customerViewModel != null) {
                            customerViewModel.insertAll(response.body().data, new OnTaskCompleted() {
                                @Override
                                public void onTaskCompleted() {

                                }
                            });
                        }

                    }

                }

            }

            @Override
            public void onFailure(Call<CustomerArrayResponse> call, Throwable t) {

            }
        });
    }

    public static void getCustomerInvoicesWithApi(
            int deviceId, int customerId, OntaskCompleteInvoice ontaskCompleteInvoice) {


        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.getCustomerInvoices(deviceId, customerId);
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject object = new JSONObject(response.body());
                        ArrayList<Invoice> invoices = new ArrayList<>();
                        ArrayList<InvoiceItem> invoiceItems = new ArrayList<>();
                        ArrayList<Self> selfList = new ArrayList<>();
                        List<Integer> invIdArray = new ArrayList<>() ;
                        System.out.println("....+++++>>>>>>" + object.getBoolean("success"));
                        System.out.println("....+++++>>>>>>" + object.getString("message"));
                        if (object.getBoolean("success")) {

                            JSONArray list = object.getJSONArray("data");




                            for (int i = 0; i < list.length(); i++) {

                                JSONObject invoiceObject = list.getJSONObject(i);
//                                System.out.println("======="+invoiceObject.getInt("customers_id")+"======>><<==="+invoiceObject.getInt("code"));
                                Invoice invoice = new Invoice();
                                invoice.customerId = invoiceObject.getInt("customers_id");
                                invoice.deviceId = invoiceObject.getInt("device_id");
                                invoice.invoice_code = invoiceObject.getInt("code");
                                invoice.effDate = invoiceObject.getString("eff_date");
                                invoice.synced = invoiceObject.getString("sync_time");
                                invoice.isSynced = true;
                                invoice.flag = invoiceObject.getInt("online");
                                invoice.created = invoiceObject.getString("created");
                                invoice.invoice_total_discount = invoiceObject.getDouble("invoice_discount");
                                invoice.invoice_net_total = invoiceObject.getDouble("invoice_net_total");
                                invoice.invoice_return_total = invoiceObject.getDouble("invoice_return_total");
                                invoice.invoice_total = invoiceObject.getDouble("invoice_total");
                                invoice.invoice_other_discount = invoiceObject.getDouble("invoice_other_discount");



                                invIdArray.add(invoice.invoice_code);
                                invoices.add(invoice);


                                JSONArray itemList = invoiceObject.getJSONArray("invoice_items");

                                for (int ii = 0; ii < itemList.length(); ii++) {
                                    JSONObject itemObject = itemList.getJSONObject(ii);
                                    InvoiceItem invoiceItem = new InvoiceItem();
                                    invoiceItem.invoiceId = itemObject.getInt("invoice_code");
                                    invoiceItem.customerId = itemObject.getInt("customers_id");
                                    invoiceItem.items_id = itemObject.getInt("items_id");
                                    invoiceItem.item_name = itemObject.getString("item_name");
                                    invoiceItem.invoice_item_qty_s = itemObject.getDouble("qty_selable");
                                    invoiceItem.invoice_item_qty_ns = itemObject.getDouble("qty_nonselable");
                                    invoiceItem.invoiceItemmrp = itemObject.getDouble("mrp");
                                    invoiceItem.invoiceItemDiscount = itemObject.getDouble("discount");
                                    invoiceItem.invoiceItemTotalDiscount = itemObject.getDouble("discount_amount");
                                    invoiceItem.invoiceItemTotalAmount = itemObject.getDouble("total");
                                    invoiceItem.deviceId = itemObject.getInt("device_id");
                                    invoiceItem.updatedAt = itemObject.getString("eff_date");
                                    invoiceItem.invoice_item_flag = itemObject.getInt("item_type");

                                    invoiceItems.add(invoiceItem);

                                }

                            }

                        }

                        ontaskCompleteInvoice.OnComplete(invoices, invoiceItems,invIdArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                ontaskCompleteInvoice.OnError(t.getMessage());
            }
        });

    }


    public static   void checkVersion(OnCheckAppVersion onCheckAppVersion){
        int versionCode = BuildConfig.VERSION_CODE;
        String packageName = BuildConfig.APPLICATION_ID;


        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.checkAppVersion(versionCode,packageName);

        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    System.out.println("============>>>>>>>>>>"+response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(jsonObject.getBoolean("success")){
                            JSONObject data = jsonObject.getJSONObject("data");
                            if(data.getInt("cur_version") > versionCode){
                                onCheckAppVersion.onAppUpdate();
                            }
                            else{
                                onCheckAppVersion.onSuccess();

                            }
                        }
                        else{
                            onCheckAppVersion.onExpired();
                        }
                    } catch (JSONException | NullPointerException | IllegalArgumentException e) {
                        e.printStackTrace();
                        onCheckAppVersion.onSuccess();
                    }
                }
                else{
                    onCheckAppVersion.onSuccess();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                onCheckAppVersion.onSuccess();
            }
        });
    }


}
