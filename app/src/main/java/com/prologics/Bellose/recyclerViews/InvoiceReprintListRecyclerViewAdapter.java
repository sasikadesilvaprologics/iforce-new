package com.prologics.Bellose.recyclerViews;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.fragments.InvoiceReprintListFragment.OnListFragmentInteractionListener;
import com.prologics.Bellose.fragments.dummy.DummyContent.DummyItem;

import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;
import static com.prologics.Bellose.databaseModule.GlobalValues.RETURN_ITEM_CODE;
import static com.prologics.Bellose.helper.Common.roundVal;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class InvoiceReprintListRecyclerViewAdapter extends RecyclerView.Adapter<InvoiceReprintListRecyclerViewAdapter.ViewHolder> {

    private final List<InvoiceItem> mValues;
    private final OnListFragmentInteractionListener mListener;

    public InvoiceReprintListRecyclerViewAdapter(List<InvoiceItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_invoicereprintlist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        String totalValue = DECIMAL_FORMAT.format(holder.mItem.invoiceItemTotalAmount+holder.mItem.invoiceItemTotalDiscount);
        if(holder.mItem.invoice_item_flag == RETURN_ITEM_CODE){
           totalValue = "-"+totalValue;
        }
        holder.itemTotal.setText(totalValue);
        double itemDiscount = holder.mItem.invoiceItemTotalDiscount/(holder.mItem.invoiceItemTotalAmount+holder.mItem.invoiceItemTotalDiscount)*100;
//
        holder.itemDiscount.setText(String.valueOf( roundVal(itemDiscount,1)));
        holder.itemPrice.setText(DECIMAL_FORMAT.format(holder.mItem.selling_price));
        holder.itemQty.setText(String.valueOf(holder.mItem.invoice_item_qty_s + holder.mItem.invoice_item_qty_ns  ));
        holder.itemName.setText(holder.mItem.item_name);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
//                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView itemName;
        public final TextView itemPrice;
        public final TextView itemDiscount;
        public final TextView itemQty;
        public InvoiceItem mItem;
        public final TextView itemTotal;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            itemName = view.findViewById(R.id.item_specification);
            itemPrice = view.findViewById(R.id.item_price);
            itemDiscount = view.findViewById(R.id.item_discount);
            itemQty = view.findViewById(R.id.item_qty);
            itemTotal = view.findViewById(R.id.sub_total);



        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }
}
