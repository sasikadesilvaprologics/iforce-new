package com.prologics.Bellose.recyclerViews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.helper.callbacks.OnInvoicePreviewListner;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;
import static com.prologics.Bellose.helper.Common.roundVal;

public class InvoiceRecyclerViewAdapter extends RecyclerView.Adapter<InvoiceRecyclerViewAdapter.ViewHolder> {

    public List<Invoice> invoices = new ArrayList<>();
    private WeakReference<FragmentActivity> fragmentActivityWeakReference;
    private OnInvoicePreviewListner onInvoicePreviewListner;

    public InvoiceRecyclerViewAdapter(FragmentActivity fragmentActivity,List<Invoice> invoices,OnInvoicePreviewListner onInvoicePreviewListner){
        this.invoices = invoices;
        this.fragmentActivityWeakReference = new WeakReference<>(fragmentActivity);
        this.onInvoicePreviewListner = onInvoicePreviewListner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_invoice_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InvoiceRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.invoice =invoices.get(position);
        holder.invNo.setText(String.valueOf(holder.invoice.invoice_code) );
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));

        System.out.println("=====sybs value======"+holder.invoice.isSynced);

        String formatedDate = "N/A";
        try {
            Date date = dateFormat.parse(holder.invoice.created);
            formatedDate = dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.invDate.setText(formatedDate);
        double discountPre = ((holder.invoice.invoice_total_discount)/(holder.invoice.invoice_net_total+holder.invoice.invoice_total_discount)*100);
        holder.invDiscount.setText(String.valueOf(roundVal(discountPre,1)));
        holder.invAmount.setText(DECIMAL_FORMAT.format(holder.invoice.invoice_total));

        if(holder.invoice.isSynced){
            holder.invoiceSyncLabel.setCardBackgroundColor(
                    fragmentActivityWeakReference.get().getResources().getColor(R.color.colorGreenBtn)
            );
        }
        else{
            holder.invoiceSyncLabel.setCardBackgroundColor(
                    fragmentActivityWeakReference.get().getResources().getColor(R.color.colorTextGray)
            );
        }


        holder.printButton.setOnClickListener(v -> onInvoicePreviewListner.onPreview(holder.invoice));


//        holder.mIdView.setText(mValues.get(position).id);
//        holder.mContentView.setText(mValues.get(position).content);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return invoices.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final CardView invoiceSyncLabel;
        final TextView invNo;
        final TextView invDate;
        final TextView invDiscount;
        final TextView invAmount;
        final ImageView printButton;
        public Invoice invoice;
//
        public ViewHolder(View view) {
            super(view);
            mView = view;
            invoiceSyncLabel = view.findViewById(R.id.invoiceSyncLabel);
            invNo = view.findViewById(R.id.invoice_no);
            invDate = view.findViewById(R.id.invoice_date);
            invDiscount = view.findViewById(R.id.invoice_dis);
            invAmount = view.findViewById(R.id.invoice_amount);
            printButton = view.findViewById(R.id.printButton);

//            mIdView = (TextView) view.findViewById(R.id.item_number);
//            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }
}

