package com.prologics.Bellose.recyclerViews;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.SelfWithItem;
import com.prologics.Bellose.helper.callbacks.OnSelfItemChangeListner;

import java.lang.ref.WeakReference;
import java.util.List;

public class SelfRecyclerViewAdapter extends RecyclerView.Adapter<SelfRecyclerViewAdapter.ViewHolder> {

    private List<SelfWithItem> mValues;
    private WeakReference<Context> weakReference;
    private StockListRecyclerViewAdapter.ItemClickListener mClickListener;
    private OnSelfItemChangeListner onSelfItemChangeListner;


    public SelfRecyclerViewAdapter(Context context, List<SelfWithItem> items, OnSelfItemChangeListner onSelfItemChangeListner) {
        mValues = items;
        weakReference = new WeakReference<>(context);
        this.onSelfItemChangeListner = onSelfItemChangeListner;
    }

    public List<SelfWithItem> getmValues() {
        return mValues;
    }

    public void setmValues(List<SelfWithItem> mValues) {
        this.mValues = mValues;
    }

    @Override
    public SelfRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_self_update_list_item, parent, false);
        return new SelfRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SelfRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.itemName.setText(holder.mItem.itemName);
//        holder.itemQty.setText(String.valueOf(holder.mItem.ownQty));
//        holder.otherQty.setText(String.valueOf(holder.mItem.other_qty));

        holder.itemQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                SelfWithItem self = mValues.get(position);
                self.other_qty = holder.mItem.other_qty;
                if (s.length() == 0) {
                    self.ownQty = 0;

                }
                else{
                    self.ownQty = Double.parseDouble(s.toString());
                    self.newQty = Double.parseDouble(s.toString());
                }

//                onSelfItemChangeListner.onChange(Collections.singletonList(self));
            }
        });

        holder.otherQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                SelfWithItem self = mValues.get(position);
                self.ownQty = holder.mItem.ownQty;
                if (s.length() == 0) {
                    self.other_qty = 0;
                }
                else{
                    self.other_qty = Double.parseDouble(s.toString());
                    self.newOtherQty = Double.parseDouble(s.toString());
                }

//                onSelfItemChangeListner.onChange(Collections.singletonList(self));
            }
        });


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClick();


            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView itemName;
        public final EditText itemQty;
        public final EditText otherQty;
        public SelfWithItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            itemName = view.findViewById(R.id.itemName);
            itemQty = view.findViewById(R.id.item_qty);
            otherQty = view.findViewById(R.id.othersQty);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }

    public interface ItemClickListener {
        void onItemClick();

    }
}
