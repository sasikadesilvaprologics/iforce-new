package com.prologics.Bellose.recyclerViews;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.StockActivity;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.fragments.StockListFragment.OnListFragmentInteractionListener;
import com.prologics.Bellose.fragments.dummy.DummyContent.DummyItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class StockListRecyclerViewAdapter extends RecyclerView.Adapter<StockListRecyclerViewAdapter.ViewHolder> implements Filterable {

    private  List<Item> filteredList = new ArrayList<>();
    private final List<Item> allValues;
    private final StockActivity.OnItemClickListner mListener;
    private WeakReference<Context> weakReference;
    private StockListRecyclerViewAdapter.ItemClickListener mClickListener;


    public StockListRecyclerViewAdapter(Context context,List<Item> items, StockActivity.OnItemClickListner listener) {
        allValues = items;
        filteredList = items;
        mListener = listener;
        weakReference = new WeakReference<>(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_stock_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = filteredList.get(position);
        holder.itemName.setText(holder.mItem.itemName);
        holder.itemAvl.setText(String.valueOf(holder.mItem.qty) );
        holder.itemPrice.setText( DECIMAL_FORMAT.format(holder.mItem.sellingPrice));
        if(holder.mItem.qty <= 0){

            holder.itemAvl.setTextColor(weakReference.get().getResources().getColor(R.color.colorRedBtn));
        }
        else{
            holder.itemAvl.setTextColor(weakReference.get().getResources().getColor(R.color.colorBlack));
        }



        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClick();

                mListener.onClickItem(holder.mItem,holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        return  new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = allValues;
                } else {
                    List<Item> newList = new ArrayList<>();
                    for (Item row : allValues) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.itemName.toLowerCase().contains(charSequence)) {
                            newList.add(row);
                        }
                    }

                    filteredList = newList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<Item>) results.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView itemName;
        public final TextView itemAvl;
        public final TextView itemPrice;
        public Item mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            itemName =  view.findViewById(R.id.itemName);
            itemAvl =  view.findViewById(R.id.itemAvl);
            itemPrice =  view.findViewById(R.id.itemPrice);
        }

        @Override
        public String toString() {
            return super.toString() + " '"  + "'";
        }
    }

    public interface ItemClickListener {
        void onItemClick();

    }
}
