package com.prologics.Bellose.recyclerViews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;

public class InvoiceReportRecyclerViewAdapter extends RecyclerView.Adapter<InvoiceReportRecyclerViewAdapter.ViewHolder> implements Filterable {

    private  List<InvoiceReport> mValues;
    private  List<InvoiceReport> filteredList;


    public InvoiceReportRecyclerViewAdapter(List<InvoiceReport> items) {
        mValues = items;
        filteredList = items;
    }

    public void setmValues(List<InvoiceReport> mValues) {
        this.mValues = mValues;
        this.filteredList = mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_report_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = filteredList.get(position);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));

        holder.name.setText(filteredList.get(position).customer_name);
        try {
            Date date = dateFormat.parse(filteredList.get(position).invoice.effDate);
            holder.invoiceDate.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.invNo.setText(String.valueOf(holder.mItem.invoice.invoice_code));
        holder.amount.setText(DECIMAL_FORMAT.format(holder.mItem.invoice.invoice_total));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView invoiceDate;
        public final TextView name;
        public final TextView invNo;
        public final TextView amount;
        public InvoiceReport mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            invoiceDate =  view.findViewById(R.id.invoiceDate);
            name =  view.findViewById(R.id.name);
            invNo =  view.findViewById(R.id.invoiceNo);
            amount =  view.findViewById(R.id.amount);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }



    @Override
    public Filter getFilter() {
        return  new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = mValues;
                } else {
                    List<InvoiceReport> newList = new ArrayList<>();
                    for (InvoiceReport row : mValues) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (
                                row.customer_name.toLowerCase().contains(charSequence.toString().toLowerCase()) ||
                                        String.valueOf(row.invoice.invoice_code).contains(charSequence.toString().toLowerCase())

                        ) {

                            newList.add(row);
                        }
                    }

                    filteredList = newList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<InvoiceReport>) results.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}





