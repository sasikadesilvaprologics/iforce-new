package com.prologics.Bellose.recyclerViews;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.reports.SalesDetailsReportActivity;
import com.prologics.Bellose.databaseModule.Entity.StockReport;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class SalesReportRecyclerViewAdapter extends RecyclerView.Adapter<SalesReportRecyclerViewAdapter.ViewHolder> implements Filterable {

    private  List<StockReport> mValues;
    private  List<StockReport> filteredList;
    private WeakReference<SalesDetailsReportActivity> salesDetailsReportActivityWeakReference;


    public SalesReportRecyclerViewAdapter(Context context, List<StockReport> stockReport) {
        mValues = stockReport;
        filteredList = stockReport;
        salesDetailsReportActivityWeakReference = new WeakReference<>((SalesDetailsReportActivity) context);


    }

    public void setmValues(List<StockReport> mValues) {
        this.mValues = mValues;
        this.filteredList = mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sales_report_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem =filteredList.get(position);
        System.out.println("================>>>>>////>>>>>>"+holder.mItem.updated);
        holder.itemName.setText(holder.mItem.item_name);
        holder.sale.setText(String.valueOf((int) holder.mItem.invoiceCount));
        Resources resources = salesDetailsReportActivityWeakReference.get().getResources();
        if(holder.mItem.invoiceCount > 0){
            holder.sale.setTextColor(resources.getColor(R.color.colorGreenBtn));
        }
        else{
            holder.sale.setTextColor(resources.getColor(R.color.colorBlack));
        }
        holder.free.setText(String.valueOf((int) holder.mItem.freeCount));
        if(holder.mItem.freeCount > 0){
            holder.free.setTextColor(resources.getColor(R.color.colorGreenBtn));
        }
        else{
            holder.free.setTextColor(resources.getColor(R.color.colorBlack));

        }
        holder.sample.setText(String.valueOf((int) holder.mItem.sampleCount));
        if(holder.mItem.sampleCount > 0){
            holder.sample.setTextColor(resources.getColor(R.color.colorGreenBtn));
        }
        else{
            holder.sample.setTextColor(resources.getColor(R.color.colorBlack));
        }

        holder.salable.setText(String.valueOf((int) holder.mItem.salableCount));
        if(holder.mItem.salableCount > 0){
            holder.salable.setTextColor(resources.getColor(R.color.colorGreenBtn));
        }
        else{
            holder.salable.setTextColor(resources.getColor(R.color.colorBlack));
        }


        holder.nonSalable.setText(String.valueOf((int) holder.mItem.nonSalableCount));
        if(holder.mItem.nonSalableCount > 0){
            holder.nonSalable.setTextColor(resources.getColor(R.color.colorGreenBtn));
        }
        else{
            holder.nonSalable.setTextColor(resources.getColor(R.color.colorBlack));
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView itemName;
        public final TextView sale;
        public final TextView free;
        public final TextView sample;
        public final TextView salable;
        public final TextView nonSalable;
        public StockReport mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            itemName =  view.findViewById(R.id.itemName);
            sale =  view.findViewById(R.id.sale);
            free =  view.findViewById(R.id.free);
            sample =  view.findViewById(R.id.sample);
            salable =  view.findViewById(R.id.salable);
            nonSalable =  view.findViewById(R.id.nonSalable);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }



    @Override
    public Filter getFilter() {
        return  new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = mValues;
                } else {
                    List<StockReport> newList = new ArrayList<>();
                    for (StockReport row : mValues) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (
                                row.item_name.toLowerCase().contains(charSequence.toString().toLowerCase())
                        ) {

                            newList.add(row);
                        }
                    }

                    filteredList = newList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<StockReport>) results.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}





