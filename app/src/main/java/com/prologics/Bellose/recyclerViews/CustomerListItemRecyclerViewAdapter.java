package com.prologics.Bellose.recyclerViews;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.CustomerActivity;
import com.prologics.Bellose.activities.DashboardActivity;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Entity.CustomerWithRelation;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.fragments.dummy.DummyContent.DummyItem;
import com.prologics.Bellose.helper.callbacks.OnCustomerSelectCallback;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * TODO: Replace the implementation with code for your data type.
 */
public class CustomerListItemRecyclerViewAdapter extends RecyclerView.Adapter<CustomerListItemRecyclerViewAdapter.ViewHolder> implements Filterable {

    private  List<CustomerWithRelation> filteredList = new ArrayList<>();
    private  List<CustomerWithRelation> mValues;
    private CustomerListItemRecyclerViewAdapter.ItemClickListener mClickListener;
    private WeakReference<Context> contextWeakReference;
    private  OnCustomerSelectCallback onCustomerSelectCallback;

    public CustomerListItemRecyclerViewAdapter(Context context, List<CustomerWithRelation> items, OnCustomerSelectCallback onCustomerSelectCallback) {
        mValues = items;
        contextWeakReference = new WeakReference<>(context);
        this.onCustomerSelectCallback = onCustomerSelectCallback;
    }

    public void setmValues(List<CustomerWithRelation> mValues) {
        this.mValues = mValues;
        this.filteredList = mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_customer_list_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.customerWithRelation = filteredList.get(position);
        holder.mItem = filteredList.get(position).customer;

        holder.customerId.setText( Integer.toString(holder.mItem.getId()) );
        holder.customerName.setText(holder.mItem.getName());
        holder.customerAddress.setText(holder.mItem.getAddressNo()+","+holder.mItem.getAddress1()+","+holder.mItem.getAddress2());
//        holder.customerType.setText(holder.mItem.getCustomerTypeId());System.out.println("============?????=======+++++++=======>>>>"+holder.mItem.getLatitude());
        holder.callBtn.setOnClickListener(new phoneClick(holder.mItem.getMobile()));
        holder.customerType.setText(holder.mItem.getCustomer_type_name());
        System.out.println(">>>><<>>>"+holder.customerWithRelation.getTodayCompleteStatus());
        if(holder.customerWithRelation.getTodayCompleteStatus()){
            holder.wrapper.setBackgroundColor(contextWeakReference.get().getResources().getColor(R.color.whiteGray));
        }
        else{
            holder.wrapper.setBackgroundColor(contextWeakReference.get().getResources().getColor(R.color.white));
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null)
                    mClickListener.onItemClick(holder.mView, position);

                onCustomerSelectCallback.onClick(holder.mItem);
            }
        });


    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout wrapper;
        public final TextView customerId;
        public final TextView customerName;
        public final TextView customerAddress;
        public final TextView customerType;
        public final ImageView callBtn;
        public final TextView completedLabel;
        public  CustomerWithRelation customerWithRelation;
        public Customer mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            wrapper = view.findViewById(R.id.wrapper);
            customerId =  view.findViewById(R.id.customerCode);
            customerName =  view.findViewById(R.id.customerName);
            customerAddress  =  view.findViewById(R.id.customerAddress);
            customerType  =  view.findViewById(R.id.customerType);
            callBtn = view.findViewById(R.id.callBtn);
            completedLabel = view.findViewById(R.id.completedLabel);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);

    }

    private  class  phoneClick implements View.OnClickListener{

        private String phoneCall;
        phoneClick(String no){
            this.phoneCall = no;
        }
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + phoneCall));
            if (ContextCompat.checkSelfPermission(contextWeakReference.get(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((DashboardActivity)contextWeakReference.get(), new String[]{Manifest.permission.CALL_PHONE},1);
            }
            else
            {
                contextWeakReference.get().startActivity(intent);
            }
        }
    }


    @Override
    public Filter getFilter() {
        return  new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = mValues;
                } else {
                    List<CustomerWithRelation> newList = new ArrayList<>();
                    for (CustomerWithRelation row : mValues) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (
                                row.customer.getName().toLowerCase().contains(charSequence.toString().toLowerCase()) ||
                               String.valueOf(row.customer.getId()).contains(charSequence.toString().toLowerCase())

                        ) {
                            System.out.println("=======++++"+row.customer.getName().toLowerCase());
                            System.out.println("=======++++"+charSequence.toString().toLowerCase());
                            newList.add(row);
                        }
                    }

                    filteredList = newList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<CustomerWithRelation>) results.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}
