package com.prologics.Bellose.recyclerViews;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.fragments.NewInvoiceFragment.OnListFragmentInteractionListener;
import com.prologics.Bellose.helper.callbacks.OnInvoiceItemClickListner;


import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;
import static com.prologics.Bellose.helper.Common.roundVal;


public class NewInvoiceRecyclerViewAdapter extends RecyclerView.Adapter<NewInvoiceRecyclerViewAdapter.ViewHolder> {

    private final List<InvoiceItem> mValues;
    private final OnListFragmentInteractionListener mListener;

    private final OnInvoiceItemClickListner onInvoiceItemClickListner;

    public NewInvoiceRecyclerViewAdapter(
            List<InvoiceItem> items,
            OnListFragmentInteractionListener listener,
            OnInvoiceItemClickListner onInvoiceItemClickListner) {
        mValues = items;
        mListener = listener;
        this.onInvoiceItemClickListner = onInvoiceItemClickListner;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_new_invoice_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.itemName.setText(holder.mItem.item_name);
        holder.itemQty.setText(String.valueOf(holder.mItem.invoice_item_qty_s + holder.mItem.invoice_item_qty_ns  ));
        holder.itemAmount.setText(DECIMAL_FORMAT.format(holder.mItem.invoiceItemTotalAmount+holder.mItem.invoiceItemTotalDiscount));

        double itemDiscount = holder.mItem.invoiceItemTotalDiscount/(holder.mItem.invoiceItemTotalAmount+holder.mItem.invoiceItemTotalDiscount)*100;
        holder.itemDisc.setText(String.valueOf(roundVal(itemDiscount,1)));
//        holder.mIdView.setText(mValues.get(position).id);
//        holder.mContentView.setText(mValues.get(position).content);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInvoiceItemClickListner.onClick(holder.mItem);
            }
        });

        holder.itemremoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInvoiceItemClickListner.onDelete(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView itemName;
        public final TextView itemQty;
        public final TextView itemDisc;
        public final TextView itemAmount;
        public final ImageView itemremoveButton;
        public InvoiceItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            itemName = view.findViewById(R.id.itemName);
            itemQty = view.findViewById(R.id.itemQty);
            itemDisc = view.findViewById(R.id.itemDisc);
            itemAmount = view.findViewById(R.id.itemAmount);
            itemremoveButton = view.findViewById(R.id.itemRemoveButton);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }
}
