package com.prologics.Bellose.recyclerViews;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.fragments.RootViewFragment.OnListFragmentInteractionListener;
import com.prologics.Bellose.dummy.DummyContent.DummyItem;
import com.prologics.Bellose.network.Entity.RootListResponse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class RootListRecyclerViewAdapter extends RecyclerView.Adapter<RootListRecyclerViewAdapter.ViewHolder> {

    private final List<RootListResponse.Data> mValues;
    private final OnListFragmentInteractionListener mListener;
    private RootListRecyclerViewAdapter.ItemClickListener mClickListener;

    public RootListRecyclerViewAdapter( List<RootListResponse.Data> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.root_fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));

        holder.rootName.setText(mValues.get(position).name);
        String formatedDate = "N/A";
        try {
           Date date = dateFormat.parse(mValues.get(position).last_date);
           formatedDate = dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.rootDate.setText(formatedDate);


        holder.mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mListener != null)
                   mListener.onListFragmentInteraction(mValues.get(position));


//                finish();
//                qif (null != mListener) {
//                    // Notify the active callbacks interface (the activity, if the
//                    // fragment is attached to one) that an item has been selected.
//
//                    mListener.onListFragmentInteraction(holder.mItem);
////
//
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public final LinearLayout mContentView;
        public final TextView rootName;
        public final TextView rootDate;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = view.findViewById(R.id.contentView);
            rootName = view.findViewById(R.id.rootName);
            rootDate = view.findViewById(R.id.rootDate);
//            mContentView = (TextView) view.findViewById(R.id.content);
        }

        public View getmView() {
            return mView;
        }


        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }


    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);

    }
}
