package com.prologics.Bellose.recyclerViews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.helper.callbacks.OnInvoicePreviewListner;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class PaymentListRecyclerViewAdapter extends RecyclerView.Adapter<PaymentListRecyclerViewAdapter.ViewHolder> {

    private List<Invoice> invoices = new ArrayList<>();
    private WeakReference<FragmentActivity> fragmentActivityWeakReference;
    private OnInvoicePreviewListner onInvoicePreviewListner;

    public PaymentListRecyclerViewAdapter(FragmentActivity fragmentActivity,List<Invoice> invoices,OnInvoicePreviewListner onInvoicePreviewListner){
        this.invoices = invoices;
        this.fragmentActivityWeakReference = new WeakReference<>(fragmentActivity);
        this.onInvoicePreviewListner = onInvoicePreviewListner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_payment_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentListRecyclerViewAdapter.ViewHolder holder, int position) {
//        holder.invoice =invoices.get(position);
//        holder.invNo.setText(String.valueOf(holder.invoice.invoice_code) );
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
//        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
//
//        System.out.println("=====sybs value======"+holder.invoice.isSynced);
//
//        String formatedDate = "N/A";
//        try {
//            Date date = dateFormat.parse(holder.invoice.created);
//            formatedDate = dateFormat.format(date);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        holder.invDate.setText(formatedDate);
//        holder.invDiscount.setText(DECIMAL_FORMAT.format(holder.invoice.invoice_total_discount));
//        holder.invAmount.setText(DECIMAL_FORMAT.format(holder.invoice.invoice_total));
//
//        if(holder.invoice.isSynced){
//            holder.invoiceSyncLabel.setCardBackgroundColor(
//                    fragmentActivityWeakReference.get().getResources().getColor(R.color.colorGreenBtn)
//            );
//        }
//        else{
//            holder.invoiceSyncLabel.setCardBackgroundColor(
//                    fragmentActivityWeakReference.get().getResources().getColor(R.color.colorTextGray)
//            );
//        }
//
//
//        holder.printButton.setOnClickListener(v -> onInvoicePreviewListner.onPreview(holder.invoice));
//

//        holder.mIdView.setText(mValues.get(position).id);
//        holder.mContentView.setText(mValues.get(position).content);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return invoices.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;

        final TextView id;
        final TextView date;
        final TextView amount;

        //
        public ViewHolder(View view) {
            super(view);
            mView = view;
            id = view.findViewById(R.id.id);
            date = view.findViewById(R.id.date);
            amount = view.findViewById(R.id.amount);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }
}

