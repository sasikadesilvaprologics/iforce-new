package com.prologics.Bellose.recyclerViews;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.Item;

import java.util.ArrayList;
import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;


public class InventoryValuationReportRecyclerViewAdapter extends RecyclerView.Adapter<InventoryValuationReportRecyclerViewAdapter.ViewHolder> implements Filterable {

    private List<Item> mValues;
    private  List<Item> filteredList;


    public InventoryValuationReportRecyclerViewAdapter(List<Item> items) {
        mValues = items;
        filteredList = items;

    }

    public void setmValues(List<Item> mValues) {
        this.mValues = mValues;
        this.filteredList = mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.inventory_report_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = filteredList.get(position);
        holder.name.setText(filteredList.get(position).itemName);
        holder.avl.setText(String.valueOf(filteredList.get(position).qty) );
        holder.price.setText(DECIMAL_FORMAT.format(holder.mItem.sellingPrice));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView name;
        public final TextView avl;
        public final TextView price;
        public Item mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            name =  view.findViewById(R.id.name);
            avl =  view.findViewById(R.id.avl);
            price =  view.findViewById(R.id.price);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }


    @Override
    public Filter getFilter() {
        return  new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = mValues;
                } else {
                    List<Item> newList = new ArrayList<>();
                    for (Item row : mValues) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (
                                row.itemName.toLowerCase().contains(charSequence.toString().toLowerCase())
                        ) {

                            newList.add(row);
                        }
                    }

                    filteredList = newList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<Item>) results.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}
