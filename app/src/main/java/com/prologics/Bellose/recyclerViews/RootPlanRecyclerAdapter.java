package com.prologics.Bellose.recyclerViews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.fragments.dashboardFragments.rootPlan.RootPlanFragment;
import com.prologics.Bellose.network.Entity.RootPlanResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class RootPlanRecyclerAdapter extends RecyclerView.Adapter<RootPlanRecyclerAdapter.ViewHolder> {

    private static RootPlanRecyclerAdapter rootPlanRecyclerAdapter;
    private List<RootPlanResponse.Plan> dataList = new ArrayList<>();
    private RootPlanFragment.OnViewHolderListner onViewHolderListner;


    public static RootPlanRecyclerAdapter getInstance(List<RootPlanResponse.Plan> list, RootPlanFragment.OnViewHolderListner onViewHolderListner) {
        if (rootPlanRecyclerAdapter == null) {
            rootPlanRecyclerAdapter = new RootPlanRecyclerAdapter();
        }

        rootPlanRecyclerAdapter.dataList = list;
        rootPlanRecyclerAdapter.onViewHolderListner = onViewHolderListner;


        return rootPlanRecyclerAdapter;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.root_plan_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.dataItem = dataList.get(position);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));

        holder.name.setText(holder.dataItem.name);
        holder.day.setText(holder.dataItem.day);
        holder.fullDate.setText(holder.dataItem.date);
        try {
            Date date = dateFormat.parse(holder.dataItem.date);
            holder.date.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }


            onViewHolderListner.onColorChange(holder.background,holder.dataItem.is_holiday);


//        holder.mContentView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout background;
        public final TextView date;
        public final TextView day;
        public final TextView fullDate;
        public final TextView name;
        public RootPlanResponse.Plan dataItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            date = view.findViewById(R.id.date);
            day = view.findViewById(R.id.day);
            fullDate = view.findViewById(R.id.fullDate);
            name = view.findViewById(R.id.name);
            background = view.findViewById(R.id.background);

        }

        public View getmView() {
            return mView;
        }


        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }


    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);

    }
}