package com.prologics.Bellose.helper.callbacks;

import com.prologics.Bellose.databaseModule.Entity.Invoice;

public interface OnInvoicePreviewListner {
    void onPreview(Invoice invoice);
}
