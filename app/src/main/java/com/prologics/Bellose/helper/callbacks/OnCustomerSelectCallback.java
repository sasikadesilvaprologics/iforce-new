package com.prologics.Bellose.helper.callbacks;

import com.prologics.Bellose.databaseModule.Entity.Customer;

public interface OnCustomerSelectCallback {

    void onClick(Customer customer);
}
