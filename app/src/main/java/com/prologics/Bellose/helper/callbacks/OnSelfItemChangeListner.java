package com.prologics.Bellose.helper.callbacks;

import com.prologics.Bellose.databaseModule.Entity.Self;

import java.util.List;

public interface OnSelfItemChangeListner {

    void onChange(List<Self> self);
}
