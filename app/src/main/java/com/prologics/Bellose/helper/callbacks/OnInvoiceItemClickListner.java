package com.prologics.Bellose.helper.callbacks;

import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;

public interface OnInvoiceItemClickListner {
    void onClick(InvoiceItem invoiceItem);
    void onDelete(InvoiceItem invoiceItem);
}
