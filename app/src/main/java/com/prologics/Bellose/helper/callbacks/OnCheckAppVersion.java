package com.prologics.Bellose.helper.callbacks;

public interface OnCheckAppVersion {

    void onAppUpdate();
    void onExpired();
    void onSuccess();
}
