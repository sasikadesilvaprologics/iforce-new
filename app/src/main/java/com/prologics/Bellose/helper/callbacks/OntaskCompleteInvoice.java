package com.prologics.Bellose.helper.callbacks;

import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;

import java.util.ArrayList;
import java.util.List;

public interface OntaskCompleteInvoice {

    void OnComplete(ArrayList<Invoice> invoices, ArrayList<InvoiceItem> invoiceItems, List<Integer> invoiceIdArray);

    void OnError(String err);
}
