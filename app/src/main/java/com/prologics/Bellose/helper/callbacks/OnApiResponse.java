package com.prologics.Bellose.helper.callbacks;

public interface OnApiResponse {
    void onSuccess(String message);
    void  onError(String message);
}
