package com.prologics.Bellose.printModule;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PrintActivity {

    WeakReference<Context> weakReferenceContext;
    WeakReference<AppCompatActivity> con;
   public static PrintOptSmall printOPT;
   public Shared shared;
    public ProgressDialog mProgressDialog;
   public int lineCount = 1;
   public boolean nextPage = false;

    void checkPrinter(OnTaskCompleted onTaskCompleted){
        if (shared.getPrinterName().isEmpty() || shared.getPrinterName() == null) {

            changePrinter(weakReferenceContext.get(), () -> {
                printOPT.reInitialize();
                onTaskCompleted.onTaskCompleted();
            });
            return;
        }
        else{
            onTaskCompleted.onTaskCompleted();
        }
    }

    private static boolean isAPrinter(BluetoothDevice device){
        int printerMask = 0b000001000000011010000000;
        int fullCod = device.getBluetoothClass().hashCode();
        return (fullCod & printerMask) == printerMask;
    }

    public static void changePrinter(Context context, OnTaskCompleted onTaskCompleted){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        Shared shared = Shared.getInstance(context);

        List<String> list = new ArrayList<String>();
        list.add("Disable");
        for (BluetoothDevice bt : pairedDevices) {

//           if(isAPrinter(bt)){
               list.add(bt.getName());
//           }



        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Warning !");
        builder.setMessage("Printer is not connected. Please select your printer name..");


        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(60, 20, 30, 20);
        Spinner spinner = new Spinner(context);
        spinner.setAdapter(dataAdapter);
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).equals(shared.getPrinterName()) &&  shared.isPrintModeEnabled()){
                spinner.setSelection(i);
            }
        }

        linearLayout.addView(spinner);
        builder.setView(linearLayout);



// Set up the buttons

        builder.setPositiveButton("Save & Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (list.size() > 0) {
                    int id = (int) spinner.getSelectedItemId();
                    if(list.get(id).equals("Disable")){
                        shared.setPrintEnable(false);
                    }
                    else{
                        shared.setPrinterName(list.get(id));
                        onTaskCompleted.onTaskCompleted();
                        shared.setPrintEnable(true);


                    }



                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }





}
