package com.prologics.Bellose.printModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReport;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReportSummery;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_PRINTED_FLAG;

public class InvoiceSmallPrint extends PrintActivity {
    private static InvoiceSmallPrint invoiceLargePrint;


    public static InvoiceSmallPrint getInstance(AppCompatActivity con, Context context) {
        if (invoiceLargePrint == null) {
            invoiceLargePrint = new InvoiceSmallPrint();

        }


        invoiceLargePrint.weakReferenceContext = new WeakReference<>(context);
        invoiceLargePrint.con = new WeakReference<>(con);
        if (printOPT == null) {
            printOPT = new PrintOptSmall(con, context);

        }

        invoiceLargePrint.shared = Shared.getInstance(context);

        return invoiceLargePrint;
    }


    public boolean isPrinterConnected(){
        return printOPT.isConnect();
    }

    public static InvoiceSmallPrint getNewInstance(AppCompatActivity con, Context context) {

        if (invoiceLargePrint == null) {
            invoiceLargePrint = new InvoiceSmallPrint();

        }

        invoiceLargePrint.weakReferenceContext = new WeakReference<>(context);
        invoiceLargePrint.con = new WeakReference<>(con);
        printOPT = new PrintOptSmall(con, context);
        invoiceLargePrint.shared = Shared.getInstance(context);

        return invoiceLargePrint;
    }


    public void startPrint(InvoicePrintModel invoicePrintModel, OnTaskCompleted onTaskCompletedParent) {

        checkPrinter(() -> {
            if (printOPT.getMac()) {

                OnTaskCompleted onTaskCompleted = new OnTaskCompleted() {
                    @Override
                    public void onTaskCompleted() {
                        dismissProgressDialog();
                        onTaskCompletedParent.onTaskCompleted();
                    }
                };

                showProgressDialog();

                if (invoicePrintModel == null) {
                    return;
                }

                new PrintAsyncTask(onTaskCompleted, printOPT).execute(invoicePrintModel);

                onTaskCompleted.onTaskCompleted();
                onTaskCompletedParent.onTaskCompleted();


            } else {

                Toast.makeText(weakReferenceContext.get(), "Printer is disconnected", Toast.LENGTH_LONG).show();
            }
        });

    }


    private class PrintAsyncTask extends AsyncTask<InvoicePrintModel, Integer, Boolean> {

        private OnTaskCompleted onTaskCompleted;
        private PrintOptSmall printOPT;

        public PrintAsyncTask(OnTaskCompleted onTaskCompleted, PrintOptSmall printOPT) {
            this.onTaskCompleted = onTaskCompleted;
            this.printOPT = printOPT;
        }

        @Override
        protected Boolean doInBackground(InvoicePrintModel... invoicePrintModels) {
            if (printOPT.isConnect()) {
                doPrintSmall(invoicePrintModels[0], false);
            }


            return true;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Boolean value) {
            onTaskCompleted.onTaskCompleted();

        }
    }


    private boolean doPrintSmall(InvoicePrintModel invoicePrintModel, boolean isCustomerSlip) {


        DecimalFormat df2 = new DecimalFormat("#,##0.00");
        String name;
        String address;
        String contact;
        String customer_code;
        String[] addressArray;
        String mobile;

        Double otherDiscount = invoicePrintModel.getInvoice().invoice_other_discount;


        name = invoicePrintModel.getCustomer().getName();
        address = invoicePrintModel.getCustomer().getFormatedAddress();
        mobile = invoicePrintModel.getCustomer().getMobile();

        contact = invoicePrintModel.getCustomer().getMobile();
        customer_code = String.valueOf(invoicePrintModel.getCustomer().getId());


        printOPT.writeEmptyLine();
        printOPT.writeCenter(shared.getAgencyName(), 1);
        printOPT.writeCenter("Distributor - East West Marketing (Pvt)Ltd", 1);
        printOPT.writeCenter(shared.getAgencyAddress(), 1);
        printOPT.writeCenter("Tel : " + shared.getAgencyMobileNo(), 1);
        printOPT.writeEmptyLine();
//        printOPT.writeCenter("INV"+SessionManager.getInstance().getDevice()+"-"+ SessionManager.getInstance().getPrintInvoice(),1);

        if (invoicePrintModel.getInvoice().flag != INVOICE_PRINTED_FLAG) {
            if (isCustomerSlip) {
                printOPT.writeCenter("Invoice-Customer Copy", 1);
            } else {
                printOPT.writeCenter("Invoice-Duplicate", 1);
            }

        } else {
            printOPT.writeCenter("Invoice", 1);
        }


        printOPT.writeLeft("Inv No    : " + invoicePrintModel.getInvoice().invoice_code, 0);
        printOPT.writeLeft("Date      : " + invoicePrintModel.getInvoice().effDate, 0);
        printOPT.writeLeft("Customer  : " + invoicePrintModel.getCustomer().getName(), 10);
        printOPT.writeLeft("Address   : " + address, 10);
        printOPT.writeLeft("Contact   : " + mobile, 0);
        printOPT.writeLeft("Sales Rep : " + shared.getCurrentUser().name, 0);
        printOPT.writeLeft("Route     : " + shared.getSelectedRoot().name, 0);
        printOPT.writeLeft("Territory : " + shared.getTerritory(), 0);
        printOPT.writeEmptyLine();
        printOPT.writeLine();
        printOPT.writeitemHeader();
        printOPT.writeLine();
        printOPT.writeEmptyLine();


//         invoice items
        List<InvoiceItem> saleItemsList = invoicePrintModel.getSalesItems();
        if (saleItemsList.size() > 0) {
            int invoiceCount = 0;
            for (InvoiceItem invoiceItem : saleItemsList) {
                int type = invoiceItem.invoice_item_flag;
                String itemName = invoiceItem.item_name;
                double itemPrice = invoiceItem.invoiceItemmrp;
                double itemTotal = invoiceItem.invoiceItemTotalAmount;
                double qty = invoiceItem.invoice_item_qty_s;

                printOPT.writeItemName(invoiceCount + "    " + itemName);

                printOPT.writeitemRow(String.valueOf(df2.format(itemPrice)), String.valueOf(qty), String.valueOf(df2.format(itemTotal)));


            }

            String inVoicetotal = String.valueOf(df2.format(invoicePrintModel.getInvoice().invoice_total));
            String net = String.valueOf(df2.format(invoicePrintModel.getInvoice().invoice_net_total));
            String rtnTotal = String.valueOf(df2.format(invoicePrintModel.getInvoice().invoice_return_total));

            String discount_val = String.valueOf(df2.format(invoicePrintModel.getInvoice().invoice_total_discount));
            String paymentTypeStr = "Cash";


            printOPT.writeLine();


            printOPT.writeTwoColumns("                    Total Savings :", discount_val);
            printOPT.writeTwoColumns("                    Invoice Total :", net);
            if (otherDiscount > 0) {
                printOPT.writeTwoColumns("                    Other Savings :", df2.format(otherDiscount));
            }

            if (paymentTypeStr.equals("Cash")) {
                printOPT.writeTwoColumns("                     Payment      : ", "Cash ");
            } else {
                printOPT.writeTwoColumns("                     Payment      :   ", "Credit");
            }
        }


        //free issue
        // sample free issue

        List<InvoiceItem> freeItemsList = invoicePrintModel.getFreeItems();
        if (freeItemsList.size() > 0) {

            try {
                printOPT.writeLine();
                printOPT.writeItemName("Free Issue");
                printOPT.writeLine();
                printOPT.writeEmptyLine();
            } catch (Exception e) {
            }

            int freeIssueCount = 0;
            for (InvoiceItem invoiceItem : freeItemsList) {


                int type = invoiceItem.invoice_item_flag;
                String itemName = invoiceItem.item_name;
                double itemPrice = invoiceItem.invoiceItemmrp;
                double itemTotal = invoiceItem.invoiceItemTotalAmount;
                double qty = invoiceItem.invoice_item_qty_s;
                freeIssueCount++;
//                            checkLineCount("small");
                printOPT.writeItemName(freeIssueCount + "    " + itemName);
                printOPT.writeitemRow(String.valueOf(df2.format(itemPrice)), String.valueOf(qty), String.valueOf(df2.format(itemTotal)));


            }
        }


        // sample  issue

        List<InvoiceItem> sampleItemsList = invoicePrintModel.getSamplesItems();
        if (freeItemsList.size() > 0) {

            try {
                printOPT.writeLine();
                printOPT.writeItemName("Sample Issue");
                printOPT.writeLine();
                printOPT.writeEmptyLine();
            } catch (Exception e) {
            }

            int sampleIssueCount = 0;
            for (InvoiceItem invoiceItem : sampleItemsList) {


                int type = invoiceItem.invoice_item_flag;
                String itemName = invoiceItem.item_name;
                double itemPrice = invoiceItem.invoiceItemmrp;
                double itemTotal = invoiceItem.invoiceItemTotalAmount;
                double qty = invoiceItem.invoice_item_qty_s;
                sampleIssueCount++;
//                            checkLineCount("small");
                printOPT.writeItemName(sampleIssueCount + "    " + itemName);
                printOPT.writeitemRow(String.valueOf(df2.format(itemPrice)), String.valueOf(qty), String.valueOf(df2.format(itemTotal)));


            }
        }


        //RETURN ITEMS
        List<InvoiceItem> returnItemsList = invoicePrintModel.getReturnItems();
        if (returnItemsList.size() > 0) {
            try {
                printOPT.writeLine();
                printOPT.writeItemName("Credit Note (Good Returns)");
                printOPT.writeLine();
                printOPT.writeEmptyLine();
            } catch (Exception e) {
            }


            int returnCount = 0;
            for (InvoiceItem invoiceItem : returnItemsList) {
                String rtnItem = invoiceItem.item_name;
                double rtnItemPrice = invoiceItem.invoiceItemmrp;
                double rtnItemTotal = invoiceItem.invoiceItemTotalAmount;
                String itemId = String.valueOf(invoiceItem.items_id);
                String itemName = invoiceItem.item_name;
                double itemQty = invoiceItem.invoice_item_qty_s;

//

                if (invoiceItem.invoice_item_qty_s == 0) {


                    returnCount++;
//                            checkLineCount("small");
                    printOPT.writeItemName(returnCount + "    " + rtnItem);
                    printOPT.writeitemRow(String.valueOf(df2.format(rtnItemPrice)), "", String.valueOf(df2.format(rtnItemTotal)));


                } else {

                    returnCount++;
//                            checkLineCount("small");
                    printOPT.writeItemName(returnCount + "    " + rtnItem);
                    printOPT.writeitemRow(String.valueOf(df2.format(rtnItemPrice)), String.valueOf((itemQty) * -1), String.valueOf(df2.format(rtnItemTotal)));


                }
            }


        }
        String inVoicetotal = String.valueOf(df2.format(invoicePrintModel.getInvoice().invoice_total));
        String net = String.valueOf(df2.format(invoicePrintModel.getInvoice().invoice_net_total));
        String rtnTotal = String.valueOf(df2.format(invoicePrintModel.getInvoice().invoice_return_total));

        String discount_val = String.valueOf(df2.format(invoicePrintModel.getInvoice().invoice_total_discount));
        String paymentTypeStr = "Cash";


        printOPT.writeLine();

        if (isCustomerSlip) {
            printOPT.writeLine();
            printOPT.writeTwoColumns("                     Return Total :", rtnTotal);
            printOPT.writeTwoColumns("                         ", "-------------");
            printOPT.writeEmptyLine();
            printOPT.writeTwoColumns("                   Payable Amount :", inVoicetotal);

            printOPT.writeTwoColumns("                         ", "-------------");
            printOPT.writeLine();
            printOPT.writeEmptyLine();
            printOPT.writeEmptyLine();
            printOPT.writeLeft("..................            ..................", 0);
            printOPT.writeLeft("Sales Rep Signature           Customer Signature", 0);
            printOPT.writeLeft("                              /Rubber Stamp", 0);
            printOPT.writeEmptyLine();
        } else {
            printOPT.writeLine();
            printOPT.writeTwoColumns("                     Return Total :", rtnTotal);
            printOPT.writeTwoColumns("                         ", "-------------");
            printOPT.writeEmptyLine();
            printOPT.writeTwoColumns("                   Payable Amount :", inVoicetotal);

            printOPT.writeTwoColumns("                         ", "-------------");
        }
        printOPT.writeEmptyLine();
        printOPT.writeEmptyLine();
        printOPT.writeLine();
        printOPT.writeCenter("iForce by Prologics", 0);
        printOPT.writeCenter("www.prologics.lk", 0);
        printOPT.writeCenter("000 000 000", 0);
        printOPT.writeEmptyLine();
        printOPT.writeEmptyLine();
        System.gc();


        return true;

    }


    public boolean printInvoiceReport(
            String start,
            String end,
            List<InvoiceReport> invoiceReports,
            InvoiceReportSummery invoiceReportSummery) {
        if (!printOPT.isConnect()) {
            return false;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));

        checkPrinter(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {

                DecimalFormat df2 = new DecimalFormat("#,##0.00");
                printOPT.writeEmptyLine();
                printOPT.writeTwoColumns("Invoice Report", start + " to " + end);
                printOPT.writeEmptyLine();
                printOPT.writeLine();


                for (InvoiceReport invoiceReport : invoiceReports) {
                    printOPT.writeItemName("     " + invoiceReport.customer_name);
                    try {
                        Date date = dateFormat.parse(invoiceReport.invoice.effDate);
                        dateFormat.format(date);
                        printOPT.writeitemRow(
                                String.valueOf(dateFormat.format(date)),
                                String.valueOf(invoiceReport.invoice.invoice_code),
                                String.valueOf(df2.format(invoiceReport.invoice.invoice_total))
                        );
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }


                printOPT.writeEmptyLine();
                printOPT.writeLine();


                printOPT.writeTwoColumns("Total", String.valueOf(df2.format(invoiceReportSummery.invoiceTotalValue)));
                printOPT.writeEmptyLine();

                printOPT.writeLeft("                              ..................", 0);
                printOPT.writeLeft("                                  Authorized", 0);
                printOPT.writeEmptyLine();
                printOPT.writeLine();
                printOPT.writeEmptyLine();


            }
        });


        return true;
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(weakReferenceContext.get(), "Processing...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }
}
