package com.prologics.Bellose.printModule;

import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;

import java.util.List;

public class InvoicePrintModel {

        private String title = "";
        private boolean isCustomerSlip = false;
        private Customer  customer;
        private Invoice invoice;
        private List<InvoiceItem> salesItems;
        private List<InvoiceItem> samplesItems;
        private List<InvoiceItem> freeItems;
        private List<InvoiceItem> returnItems;
       public static InvoicePrintModel getInstance(Boolean isCustomerSlip,String title, Customer customer,
                                                   Invoice invoice,
                                                   List<InvoiceItem> salesItems,
                                                   List<InvoiceItem> samplesItems,
                                                   List<InvoiceItem> freeItems,
                                                   List<InvoiceItem> returnItems
                                            ){
            InvoicePrintModel invoicePrintModel = new InvoicePrintModel();
            invoicePrintModel.title = title;
            invoicePrintModel.invoice = invoice;
            invoicePrintModel.isCustomerSlip = isCustomerSlip;
            invoicePrintModel.customer = customer;
            invoicePrintModel.salesItems = salesItems;
            invoicePrintModel.freeItems = freeItems;
            invoicePrintModel.samplesItems = samplesItems;
            invoicePrintModel.returnItems = returnItems;

            return invoicePrintModel;

        }


    public Customer getCustomer() {
        return customer;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public List<InvoiceItem> getFreeItems() {
        return freeItems;
    }

    public List<InvoiceItem> getReturnItems() {
        return returnItems;
    }

    public List<InvoiceItem> getSalesItems() {
        return salesItems;
    }

    public List<InvoiceItem> getSamplesItems() {
        return samplesItems;
    }

    public String getTitle() {
        return title;
    }

    public boolean isCustomerSlip() {
        return isCustomerSlip;
    }
}
