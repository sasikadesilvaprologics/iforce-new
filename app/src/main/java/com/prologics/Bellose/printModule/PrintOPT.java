package com.prologics.Bellose.printModule;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.ParcelUuid;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.prologics.Bellose.databaseModule.Shared;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

public class PrintOPT {

    int maxBig = 27;
    int maxCarets = 50;
    int maxSmallCarets = 85;

    int size_item = 25;
    int size_dis = 15;
    int size_qty = 18;
    int size_unit = 11;
    int size_total = 21;
    int size_name =15;
    int size_totals = 27;
    int size_separator =2;
    int size_stockist =17;
    int size_address =25;


    int tab_to_description = size_item;
    int tab_to_qty = tab_to_description + size_qty;
    int tab_to_unit_price = tab_to_qty + size_unit;
    int tab_to_total = tab_to_unit_price + size_unit;

    BluetoothDevice mmDevice;

    AppCompatActivity con;
    String mac;
    int deviceIndex = 0;
    BluetoothDevice device;
    WeakReference<Context> contextWeakReference;
    private ProgressDialog mProgressDialog;

    private Shared shared;
    public PrintOPT(AppCompatActivity con, Context context){

        shared = Shared.getInstance(context);
        this.con = con;
        contextWeakReference = new WeakReference<>(context);

        showProgressDialog();
        new InitializeAsyncTask().execute(true);

    }

    public void reInitialize(){
        new InitializeAsyncTask().execute(true);
    }


    //    @SuppressLint("StaticFieldLeak")
    private   class InitializeAsyncTask extends AsyncTask<Boolean, Integer, Boolean> {


        @Override
        protected Boolean doInBackground(Boolean... booleans) {

            if(getMac()){
//
                if(initialize()){
                    System.out.println("===============printerConnected");
                }
                else{
                    initialize();
                }
            }
            else{
//                Toast.makeText(contextWeakReference.get(), "Printer Name is not match", Toast.LENGTH_SHORT).show();
            }

            return true;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Boolean value) {
            dismissProgressDialog();
            System.out.println("=========printerInitialize=========="+value);

        }
    }




    public boolean getMac()  {
        deviceIndex = 0;

        try {
            String mcn = "FireFly-BTP";
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                Log.d("print error", "No bluetooth adapter available");
            }
            assert mBluetoothAdapter != null;
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                con.startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
                    .getBondedDevices();
            System.out.println("===========par device ======="+pairedDevices.size());

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    System.out.println("===========par device =="+
                            device.getName()+"====="+
                            device.getAddress()+"==="+
                            device.getUuids().length+"========");
//
                    deviceIndex++;

//                    System.out.println("===========par device =="+device.getName()+"====="+device.getAddress());
//                    Log.d("devices", device.getName());
                    if (device.getName().equals(shared.getPrinterName())) {
                        mmDevice = device;
                        mac = device.getAddress();
                        return true;

                    }
                }
            }
        }catch (Exception e){
            return false;

        }

        return false;

    }

    BluetoothSocket socket;
    OutputStream out;


    private boolean initialize(){




        //test
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

        Method getUuidsMethod = null;
        try {
            getUuidsMethod = BluetoothAdapter.class.getDeclaredMethod("getUuids", null);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        ParcelUuid[] uuids = new ParcelUuid[0];
        try {

            uuids = (ParcelUuid[]) getUuidsMethod.invoke(adapter, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        System.out.println("==========index======================"+uuids.length);
        if(uuids == null || uuids.length  == 0){

            return false;
        }


//        ===========par device ==BT-PTR-C71872=====00:02:72:C7:18:72===3========

        //test
//        "00001101-0000-1000-8000-00805f9b34fb"

        final UUID SerialPortServiceClass_UUID = UUID.fromString( "00001101-0000-1000-8000-00805f9b34fb");
        final BluetoothAdapter BA = BluetoothAdapter.getDefaultAdapter();

        final String PrinterBsid = mac;

        if(SerialPortServiceClass_UUID.equals("null")){
            return false;
        }


        if (PrinterBsid != null && !PrinterBsid.isEmpty() && !PrinterBsid.equals("null")){
            BluetoothDevice BD = BA.getRemoteDevice(PrinterBsid);

            socket =null;
            try {
                socket = BD.createRfcommSocketToServiceRecord(SerialPortServiceClass_UUID);
            } catch (Exception e) {Log.e("","Error creating socket");}

            try {
                socket.connect();
                out = socket.getOutputStream();


                Log.e("","Connected");
            } catch (IOException e) {
//                Log.e("",e.getMessage());
//                Toast.makeText(contextWeakReference.get(),"This is not a printer , Please check again", Toast.LENGTH_SHORT).show();
                try {

                    Log.e("","trying fallback...");
                    socket =(BluetoothSocket) BD.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(BD,1);
                    socket.connect();
                    out = socket.getOutputStream();

                    Log.e("","Connected");
                }
                catch (Exception e2) {
                    Log.e("", "Couldn't establish Bluetooth connection!");
                    Log.e("",e2.getMessage());
                    return false;
                }
            }
            return true;
        }else{
//            Toast.makeText(con.getApplicationContext(), "Invalid MAC Address", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void connect(){
        if (!socket.isConnected()) {
            try {
                socket.connect();
                out = socket.getOutputStream();

            } catch (IOException e) {

            }
        }
    }

    public boolean isConnect(){
        return socket != null && socket.isConnected();
    }

    public void setDefault() throws IOException {
        byte[] format = { 27, 33, 0 };

        out.write(format);
    }

    public void setSmall() throws IOException {
        byte[] format = new byte[]{ 27, 15, 0};
        out.write(format);
    }

    public void setHeight() throws IOException {
        byte[] arrayOfByte1 = new byte[]{0, 0, 10};
        byte[] format = new byte[]{ 27, 33, 0};
//        format[2] = ((byte)(0x10 | arrayOfByte1[2]));
//        out.write(format);
        format[2] = ((byte) (0x20 | arrayOfByte1[2]));
        out.write(format);
    }

    public void writeVerticalLine() throws IOException {
//        byte[] arrayOfByte1 = new byte[]{0, 0, 10};
//        byte[] format = new byte[]{ 27, 33, 0};
//        format[2] = ((byte)(0x80 | arrayOfByte1[2]));
//        out.write(format);

        StringBuilder bu = new StringBuilder();
        for(int t = 0;t < maxCarets; t++){
            bu.append('-');
        }
        write(bu.toString());
    }

    public void writeRowNew(String unit, String qty, String tot){
        StringBuilder bu = new StringBuilder();

        if(qty.length() > size_qty){
            qty = qty.substring(0, size_qty-3)+"...";
        }
        if(unit.length() > size_unit){
            unit = unit.substring(0, size_unit-3)+"...";
        }
        if(tot.length() > size_total){
            tot = tot.substring(0, size_total-3)+"...";
        }
        for(int t= 0; t < 25; t++){
            bu.append(' ');
        }
        getRowData(unit, bu, size_unit, true);
        getRowData(qty, bu, size_qty, true);
        getRowData(tot, bu, size_total, true);
        write(bu.toString());

    }

    public void writeRow(String item, String unit, String qty, String tot){
        StringBuilder bu = new StringBuilder();
        if(item.length() > size_item) {
            item = item.substring(0, size_item - 3) + "...";
        }
        if(qty.length() > size_qty){
            qty = qty.substring(0, size_qty-3)+"...";
        }
        if(unit.length() > size_unit){
            unit = unit.substring(0, size_unit-3)+"...";
        }
        if(tot.length() > size_total){
            tot = tot.substring(0, size_total-3)+"...";
        }
        getRowData(item, bu, size_item, false);
        getRowData(unit, bu, size_unit, true);
        getRowData(qty, bu, size_qty, true);
//        leftAndRight("",tot,1);
        getRowData(tot, bu, size_total, true);




        write(bu.toString());
    }

    public void writeTotalRow(String item, String separator, String tot){
        StringBuilder bu = new StringBuilder();
        if(item.length() > size_name){
            item = item.substring(0, size_name-3)+"...";       }
        if(separator.length() > size_separator){
            separator = separator.substring(0, size_separator-3)+"...";
        }
        if(tot.length() > size_totals){
            tot = tot.substring(0, size_totals-3)+"...";
        }
        getRowData(item, bu, size_name, false);
        getRowData(separator, bu, size_separator, false);
        getRowData(tot, bu, size_totals, true);
        write(bu.toString());
    }

    public void writeAddressRow(String item, String tot){
        StringBuilder bu = new StringBuilder();
        if(item.length() > size_address){
            item = item.substring(0, size_address-3)+"...";       }

        if(tot.length() > size_stockist){
            tot = tot.substring(0, size_stockist-3)+"...";
        }
        getRowData(item, bu, size_address, false);
        getRowData(tot, bu, size_stockist, false);
        write(bu.toString());
    }

    public void writeRowRight(String item){
        int len = item.length();
        int tot = 42;
        StringBuilder bu = new StringBuilder();
        for(int t= 0; t < tot-len; t++){
            bu.append(' ');
        }
        bu.append(item);

        write(bu.toString());
    }

    private void getRowData(String data, StringBuilder bu, int len, boolean rightAlign){
        int siz = data.length();

        if(!rightAlign) {
            bu.append(data);
        }
        for(int t = siz; t<len; t++){
            bu.append(' ');
        }
        if(rightAlign) {
            bu.append(data);
        }
    }

    public void writeAndKeepSpace(String txt, int font) throws IOException {
        int tot;
        if(font == 0){
            tot = maxBig;
        }else if(font == 2) {
            tot = maxSmallCarets;
        }else{
            tot = maxCarets;
        }
        int len = txt.length();
        StringBuilder bu = new StringBuilder();
        bu.append(txt);
        for(int t= 0; t < tot-len; t++){
            bu.append(' ');
        }
        write(bu.toString());
    }

    public void leftAndRightInv(String left, String right, int font){
        StringBuilder bu = new StringBuilder();
        bu.append(left);

        int len = right.length();
        int tot =43;


        int tab = tot - (bu.length() + right.length());
        for(int t = 0; t < tab; t++){
            bu.append(' ');
        }
        bu.append(right);

        write(bu.toString());
    }

    public void leftAndRight(String left, String right, int font){
        StringBuilder bu = new StringBuilder();
        bu.append(left);

        int len = right.length();
        int tot;
        if(font == 0){
            tot = maxBig;
        }else if(font == 2) {
            tot = maxSmallCarets;
        }else{
            tot = maxCarets;
        }

        int tab = tot - (bu.length() + right.length());
        for(int t = 0; t < tab; t++){
            bu.append(' ');
        }
        bu.append(right);

        write(bu.toString());
    }

    public void writeCenter(String tx, int font){
        int len = tx.length();
        int tot;
        if(font == 0){
            tot = maxBig;
        }else if(font == 2) {
            tot = maxSmallCarets;
        }else{
            tot = maxCarets;
        }

        int dif = tot-len;
        if(dif>0){
            int left = dif/2;
            StringBuilder bu = new StringBuilder();
            for(int t= 0; t < left; t++){
                bu.append(' ');
            }
            bu.append(tx);
            write(bu.toString());
        } else{
            write(tx);
        }
    }



    public void write(String tx){
        if (!socket.isConnected()) {
            connect();
        }

        Log.d("siz", tx.length()+"");
        try{
            out.write((tx + "\n").getBytes());
//            System.out.println("======test print ====="+hexToByte("FF"));
//            out.write(hexToByte("FF"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pageEnd(){
        try {
            out.write(hexToByte("FF"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //hexa to bite helper function
    public byte hexToByte(String hexString) {
        int firstDigit = toDigit(hexString.charAt(0));
        int secondDigit = toDigit(hexString.charAt(1));
        return (byte) ((firstDigit << 4) + secondDigit);
    }

    private int toDigit(char hexChar) {
        int digit = Character.digit(hexChar, 16);
        if(digit == -1) {
            throw new IllegalArgumentException(
                    "Invalid Hexadecimal Character: "+ hexChar);
        }
        return digit;
    }

    public void close(){
        try {
//
//            out.close();
//            socket.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = android.app.ProgressDialog.show(contextWeakReference.get(), "Connecting...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }


    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}