package com.prologics.Bellose.printModule;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.ParcelUuid;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.prologics.Bellose.databaseModule.Shared;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

public class PrintOptSmall {

    int maxBig = 27;
    int maxCarets = 50;
    int maxSmallCarets = 48;

    int size_item = 25;
    int size_dis = 15;
    int size_qty = 18;
    int size_unit = 11;
    int size_total = 21;
    int size_name = 15;
    int size_totals = 27;
    int size_separator = 2;
    int size_stockist = 17;
    int size_address = 25;


    int tab_to_description = size_item;
    int tab_to_qty = tab_to_description + size_qty;
    int tab_to_unit_price = tab_to_qty + size_unit;
    int tab_to_total = tab_to_unit_price + size_unit;

    byte FONT_TYPE;

    public static final byte LF = 0x0A;
    public static byte[] FEED_LINE = {10};

    public static final byte[] ESC_ALIGN_LEFT = new byte[]{0x1b, 'a', 0x00};
    public static final byte[] ESC_ALIGN_RIGHT = new byte[]{0x1b, 'a', 0x02};
    public static final byte[] ESC_ALIGN_CENTER = new byte[]{0x1b, 'a', 0x01};
    public static final byte[] ESC_CANCEL_BOLD = new byte[]{0x1B, 0x45, 0};

    BluetoothDevice mmDevice;


    AppCompatActivity con;
    String mac;
    int deviceIndex = 0;
    BluetoothDevice device;
    WeakReference<Context> contextWeakReference;
    private ProgressDialog mProgressDialog;

    private Shared shared;

    public PrintOptSmall(AppCompatActivity con, Context context) {
        shared = Shared.getInstance(context);
        this.con = con;
        contextWeakReference = new WeakReference<>(context);

        showProgressDialog();
        new InitializeAsyncTask().execute(true);

    }

    public void reInitialize(){
        new InitializeAsyncTask().execute(true);
    }

    //    @SuppressLint("StaticFieldLeak")
    private class InitializeAsyncTask extends AsyncTask<Boolean, Integer, Boolean> {


        @Override
        protected Boolean doInBackground(Boolean... booleans) {

            if (getMac()) {
//
                if (initialize()) {
                    System.out.println("===============printerConnected");
                } else {
                    initialize();
                }
            } else {
//                Toast.makeText(contextWeakReference.get(), "Printer Name is not match", Toast.LENGTH_SHORT).show();
            }

            return true;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Boolean value) {
            dismissProgressDialog();
            System.out.println("=========printerInitialize==========" + value);

        }
    }


    public boolean getMac() {
        deviceIndex = 0;

        try {
            String mcn = "FireFly-BTP";
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                Log.d("print error", "No bluetooth adapter available");
            }
            assert mBluetoothAdapter != null;
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                con.startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
                    .getBondedDevices();
            System.out.println("===========par device =======" + pairedDevices.size());

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    System.out.println("===========par device ==" +
                            device.getName() + "=====" +
                            device.getAddress() + "===" +
                            device.getUuids().length + "========");
//
                    deviceIndex++;

//                    System.out.println("===========par device =="+device.getName()+"====="+device.getAddress());
//                    Log.d("devices", device.getName());
                    if (device.getName().equals(shared.getPrinterName())) {
                        mmDevice = device;
                        mac = device.getAddress();
                        return true;

                    }
                }
            }
        } catch (Exception e) {
            return false;

        }

        return false;

    }

    BluetoothSocket socket;
    OutputStream out;


    private boolean initialize() {
        //test
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

        Method getUuidsMethod = null;
        try {
            getUuidsMethod = BluetoothAdapter.class.getDeclaredMethod("getUuids", null);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        ParcelUuid[] uuids = new ParcelUuid[0];
        try {

            uuids = (ParcelUuid[]) getUuidsMethod.invoke(adapter, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        System.out.println("==========index======================" + uuids.length);
        if (uuids == null || uuids.length == 0) {

            return false;
        }


//        ===========par device ==BT-PTR-C71872=====00:02:72:C7:18:72===3========

        //test
//        "00001101-0000-1000-8000-00805f9b34fb"

        final UUID SerialPortServiceClass_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
        final BluetoothAdapter BA = BluetoothAdapter.getDefaultAdapter();

        final String PrinterBsid = mac;

        if (SerialPortServiceClass_UUID.equals("null")) {
            return false;
        }


        if (PrinterBsid != null && !PrinterBsid.isEmpty() && !PrinterBsid.equals("null")) {
            BluetoothDevice BD = BA.getRemoteDevice(PrinterBsid);

            socket = null;
            try {
                socket = BD.createRfcommSocketToServiceRecord(SerialPortServiceClass_UUID);
            } catch (Exception e) {
                Log.e("", "Error creating socket");
            }

            try {
                socket.connect();
                out = socket.getOutputStream();


                Log.e("", "Connected");
            } catch (IOException e) {
//                Log.e("",e.getMessage());
//                Toast.makeText(contextWeakReference.get(),"This is not a printer , Please check again", Toast.LENGTH_SHORT).show();
                try {

                    Log.e("", "trying fallback...");
                    socket = (BluetoothSocket) BD.getClass().getMethod("createRfcommSocket", new Class[]{int.class}).invoke(BD, 1);
                    socket.connect();
                    out = socket.getOutputStream();

                    Log.e("", "Connected");
                } catch (Exception e2) {
                    Log.e("", "Couldn't establish Bluetooth connection!");
                    Log.e("", e2.getMessage());
                    return false;
                }
            }
            return true;
        } else {
//            Toast.makeText(con.getApplicationContext(), "Invalid MAC Address", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void connect() {
        if (!socket.isConnected()) {
            try {
                socket.connect();
                out = socket.getOutputStream();

            } catch (IOException e) {

            }
        }
    }

    public boolean isConnect() {
        System.out.println("=========sshdhshdhs===" + socket.isConnected());
        return socket != null && socket.isConnected();
    }

    public void setDefault() throws IOException {
        byte[] format = {27, 33, 0};

        out.write(format);
    }


    public void writeCenter(String tx, int font) {


        try {


            int len = tx.length();
            System.out.println("========length======" + len);
            int dif = maxSmallCarets - len;

            if (dif > 0) {
                int left = dif / 2;
                System.out.println("========diff======" + left);
                StringBuilder bu = new StringBuilder();
                for (int t = 0; t < left; t++) {
                    bu.append(' ');
                }

                int rightCount = dif - left;
                StringBuilder right = new StringBuilder();
                for (int t = 0; t < rightCount; t++) {
                    right.append(' ');
                }


                bu.append(tx).append(right);
                System.out.println("========" + bu + "======");
                out.write(bu.toString().getBytes());
            } else {
                out.write(tx.getBytes());
            }


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public void writeLeft(String tx, int leftMargin) {

        System.out.println("=========left margin ==========" + leftMargin);
        try {

            int len = tx.length();
            int dif;
            System.out.println("========length======" + len);
            int count = 0;
            String buTmp = "";
            if (len > maxSmallCarets) {
                for (int i = 0; i < len; i++) {
                    count++;
                    if (count == maxSmallCarets) {
                        buTmp = buTmp + "             ";
                        count = 0;
                    } else {
                        buTmp = buTmp + tx.charAt(i);
                    }

                }

                tx = buTmp;
                len = tx.length();
                int count1 = len / maxSmallCarets;
                int count2 = len - count1 * maxSmallCarets;
                dif = maxSmallCarets - count2;
            } else {
                dif = maxSmallCarets - len;
            }


            if (dif > 0) {
                int left = dif;
                StringBuilder bu = new StringBuilder();


                bu.append(tx);
                for (int t = 0; t < left; t++) {
                    bu.append(' ');
                }

                out.write(bu.toString().getBytes());
            } else {
                out.write(tx.getBytes());
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void writeRight(String tx) {
        try {
            out.write(tx.getBytes(), 0, tx.length());

//            int len = tx.length();
//            System.out.println("========length======"+len);
//            int dif = maxSmallCarets-len;
//
//            if(dif>0){
//                int left = dif;
//                System.out.println("========diff======"+left);
//                StringBuilder bu = new StringBuilder();
//                for(int t= 0; t < left; t++){
//                    bu.append(' ');
//                }
//
//
//
//
//                bu.append(tx);
//                System.out.println("========"+bu+"======");
//                out.write(bu.toString().getBytes());
//            } else{
//                out.write(tx.getBytes());
//            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void writeLine() {


        try {
            StringBuilder line = new StringBuilder();
            for (int t = 0; t < maxSmallCarets; t++) {
                line.append("-");
            }
            out.write(line.toString().getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void writeTwoColumns(String title, String value) {
        try {

            StringBuilder columns = new StringBuilder();
            int length = title.length() + value.length();
            if (length == maxSmallCarets) {
                out.write((title + value).getBytes(), 0, (title + value).getBytes().length);
            } else if (length > maxSmallCarets) {
                StringBuilder columnsUpdate = new StringBuilder();
                columnsUpdate.append(title);
                for (int i = 0; i < length - maxSmallCarets; i++) {
                    columnsUpdate.append(" ");
                }
                out.write(columnsUpdate.append(value).toString().getBytes());
            } else {
                StringBuilder columnsUpdate = new StringBuilder();
                columnsUpdate.append(title);
                for (int i = 0; i < maxSmallCarets - length; i++) {
                    columnsUpdate.append(" ");
                }

                out.write(columnsUpdate.append(value).toString().getBytes());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeEmptyLine() {
        try {
            StringBuilder line = new StringBuilder();
            for (int t = 0; t < maxSmallCarets; t++) {
                line.append(" ");
            }
            out.write(line.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeTestLine(String tx) {
//        try {
//            byte[] arrayOfByte1 = new byte[]{0, 0, 10};
//            byte[] format = new byte[]{ 27, 15, 10};
//
//
//
//            out.write(format);
//
//            String str = tx+"\n";
//            out.write(str.getBytes());
//
//            byte[] format2 = { 27, 33, 0 };
//
//            out.write(format2);
//
//            out.write(tx.getBytes());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        String message = tx;
//        writeWithFormat(message.getBytes(), new Formatter().get(), Formatter.leftAlign());
//// Bold format center:
//        writeWithFormat(message.getBytes(), new Formatter().bold().get(), Formatter.centerAlign());
//// Bold underlined format with right alignment:
//        writeWithFormat(message.getBytes(), new Formatter().bold().underlined().get(), Formatter.rightAlign());
    }

    public void writeitemHeader() {
        try {
            StringBuilder columns = new StringBuilder();
            for (int t = 0; t < 39; t++) {
                if (t < 4) {
                    if (t == 0) {
                        columns.append("Ls");
                    } else if (t > 1) {
                        columns.append(" ");
                    }

                } else if (t < 13) {
                    if (t == 5) {
                        columns.append("Item");
                    } else if (t > 8) {
                        columns.append(" ");
                    }

                } else if (t < 28) {
                    if (t == 14) {
                        columns.append("Price");
                    } else if (t > 18) {
                        columns.append(" ");
                    }

                } else if (t < 38) {
                    if (t == 29) {
                        columns.append("Qty");
                    } else if (t > 32) {
                        columns.append(" ");
                    }

                } else {
                    columns.append(" ");
                }


            }

            for (int i = 0; i < 7; i++) {
                columns.append(" ");
            }


            columns.append("Amount");


            if (columns.toString().length() == maxSmallCarets) {
                out.write(columns.toString().getBytes());
            } else if (columns.toString().length() > maxSmallCarets) {
                StringBuilder columnsUpdate = new StringBuilder();
                for (int i = 0; i < columns.toString().length() - maxSmallCarets; i++) {
                    columnsUpdate.append(" ");
                }
                out.write(columns.append(columnsUpdate).toString().getBytes());
            } else if (columns.toString().length() < maxSmallCarets) {
                StringBuilder columnsUpdate = new StringBuilder();
                for (int i = 0; i < maxSmallCarets - columns.toString().length(); i++) {
                    columnsUpdate.append(" ");
                }
                out.write(columns.append(columnsUpdate).toString().getBytes());
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeitemRow(String price, String qty, String amount) {
        try {

            StringBuilder columns = new StringBuilder();
            for (int t = 0; t < 39; t++) {
                if (t < 13) {

                    columns.append(" ");


                } else if (t < 28) {
                    if (t == 14) {
                        columns.append(price);
                    } else if (t > (14 + (price.length()))) {
                        columns.append(" ");
                    }

                } else if (t < 38) {
                    if (t == 29) {
                        columns.append(qty);
                    } else if (t > (29 + (qty.length()))) {
                        columns.append(" ");
                    }

                } else {
                    columns.append(" ");
                }


            }

            for (int i = 0; i < 13 - amount.length(); i++) {
                columns.append(" ");
            }


            columns.append(amount);


            if (columns.toString().length() == maxSmallCarets) {
                out.write(columns.toString().getBytes());
            } else if (columns.toString().length() > maxSmallCarets) {
                StringBuilder columnsUpdate = new StringBuilder();
                for (int i = 0; i < columns.toString().length() - maxSmallCarets; i++) {
                    columnsUpdate.append(" ");
                }
                out.write(columns.append(columnsUpdate).toString().getBytes());
            } else if (columns.toString().length() < maxSmallCarets) {
                StringBuilder columnsUpdate = new StringBuilder();
                for (int i = 0; i < maxSmallCarets - columns.toString().length(); i++) {
                    columnsUpdate.append(" ");
                }
                out.write(columns.append(columnsUpdate).toString().getBytes());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeItemName(String itemName) {
        System.out.println("item name-=======");
        try {


            if (itemName.length() == maxSmallCarets) {
                out.write(itemName.getBytes());
            } else if (itemName.length() > maxSmallCarets) {
                StringBuilder columnsUpdate = new StringBuilder();
                columnsUpdate.append(itemName);
                for (int i = 0; i < itemName.length() - maxSmallCarets; i++) {
                    columnsUpdate.append(" ");
                }
                out.write(columnsUpdate.toString().getBytes());
            } else {

                StringBuilder columnsUpdate = new StringBuilder();
                columnsUpdate.append(itemName);
                int count1 = itemName.length() / maxSmallCarets;
                int count2 = itemName.length() - count1 * maxSmallCarets;
                int diff = maxSmallCarets - count2;
                for (int i = 0; i < diff; i++) {
                    columnsUpdate.append(" ");
                }
                out.write(columnsUpdate.toString().getBytes());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = android.app.ProgressDialog.show(contextWeakReference.get(), "Connecting...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    //hexa to bite helper function
    public byte hexToByte(String hexString) {
        int firstDigit = toDigit(hexString.charAt(0));
        int secondDigit = toDigit(hexString.charAt(1));
        return (byte) ((firstDigit << 4) + secondDigit);
    }

    private int toDigit(char hexChar) {
        int digit = Character.digit(hexChar, 16);
        if (digit == -1) {
            throw new IllegalArgumentException(
                    "Invalid Hexadecimal Character: " + hexChar);
        }
        return digit;
    }


    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


    /**
     * Method to write with a given format
     *
     * @param buffer     the array of bytes to actually write
     * @param pFormat    The format byte array
     * @param pAlignment The alignment byte array
     * @return true on successful write, false otherwise
     */
    public boolean writeWithFormat(byte[] buffer, final byte[] pFormat, final byte[] pAlignment) {
        try {
            // Notify printer it should be printed with given alignment:
            out.write(pAlignment);
            // Notify printer it should be printed in the given format:
            out.write(pFormat);
            // Write the actual data:
            out.write(buffer, 0, buffer.length);

            // Share the sent message back to the UI Activity

            return true;
        } catch (IOException e) {

            return false;
        }
    }


    /**
     * Class for formatting
     */
    public static class Formatter {
        /**
         * The format that is being build on
         */
        private byte[] mFormat;

        public Formatter() {
            // Default:
            mFormat = new byte[]{27, 33, 0};
        }

        /**
         * Method to get the Build result
         *
         * @return the format
         */
        public byte[] get() {
            return mFormat;
        }

        public Formatter bold() {
            // Apply bold:
            mFormat[2] = ((byte) (0x8 | mFormat[2]));
            return this;
        }

        public Formatter small() {
            mFormat[2] = ((byte) (0x1 | mFormat[2]));
            return this;
        }

        public Formatter height() {
            mFormat[2] = ((byte) (0x10 | mFormat[2]));
            return this;
        }

        public Formatter width() {
            mFormat[2] = ((byte) (0x20 | mFormat[2]));
            return this;
        }

        public Formatter underlined() {
            mFormat[2] = ((byte) (0x80 | mFormat[2]));
            return this;
        }

        public static byte[] rightAlign() {
            return new byte[]{0x1B, 'a', 0x02};
        }

        public static byte[] leftAlign() {
            return new byte[]{0x1B, 'a', 0x00};
        }

        public static byte[] centerAlign() {
            return new byte[]{0x1B, 'a', 0x01};
        }

    }
}

