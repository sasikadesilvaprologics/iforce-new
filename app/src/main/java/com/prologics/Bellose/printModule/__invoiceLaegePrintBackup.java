package com.prologics.Bellose.printModule;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class __invoiceLaegePrintBackup {
}



//package com.prologics.Bellose.printModule;
//
//        import android.app.AlertDialog;
//        import android.app.ProgressDialog;
//        import android.bluetooth.BluetoothAdapter;
//        import android.bluetooth.BluetoothDevice;
//        import android.content.Context;
//        import android.content.DialogInterface;
//        import android.os.AsyncTask;
//        import android.util.Log;
//        import android.widget.ArrayAdapter;
//        import android.widget.LinearLayout;
//        import android.widget.Spinner;
//        import android.widget.Toast;
//
//        import androidx.appcompat.app.AppCompatActivity;
//
//        import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
//        import com.prologics.Bellose.databaseModule.Shared;
//        import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
//
//        import java.lang.ref.WeakReference;
//        import java.text.DecimalFormat;
//        import java.util.ArrayList;
//        import java.util.List;
//        import java.util.Set;
//
//public class InvoiceLargePrint {
//    private static InvoiceLargePrint invoiceLargePrint;
//    private WeakReference<Context> weakReferenceContext;
//    private WeakReference<AppCompatActivity> con;
//    private PrintOPT printOPT;
//    private Shared shared;
//    private ProgressDialog mProgressDialog;
//    int lineCount = 1;
//    boolean nextPage = false;
//
//    public static InvoiceLargePrint getInstance(AppCompatActivity con, Context context) {
//        if (invoiceLargePrint == null) {
//            invoiceLargePrint = new InvoiceLargePrint();
//
//        }
//
//        invoiceLargePrint.weakReferenceContext = new WeakReference<>(context);
//        invoiceLargePrint.con = new WeakReference<>(con);
//        invoiceLargePrint.printOPT = new PrintOPT(con, context);
//        invoiceLargePrint.shared = Shared.getInstance(context);
//
//        return invoiceLargePrint;
//    }
//
//    public static InvoiceLargePrint getNewInstance(AppCompatActivity con, Context context) {
//
//        if (invoiceLargePrint == null) {
//            invoiceLargePrint = new InvoiceLargePrint();
//
//        }
//
//        invoiceLargePrint.weakReferenceContext = new WeakReference<>(context);
//        invoiceLargePrint.con = new WeakReference<>(con);
//        invoiceLargePrint.printOPT = new PrintOPT(con, context);
//        invoiceLargePrint.shared = Shared.getInstance(context);
//
//        return invoiceLargePrint;
//    }
//
//
//    public void startPrint(InvoicePrintModel invoicePrintModel, OnTaskCompleted onTaskCompletedParent) {
//        if (shared.getPrinterName().isEmpty() || shared.getPrinterName() == null) {
////            this.isPrintButtonClickedBreak = true;
//
//            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
//
//            List<String> list = new ArrayList<String>();
//            for(BluetoothDevice bt : pairedDevices){
//                list.add(bt.getName());
//                System.out.println("==============>>>>>>>>|||||||||"+bt.getName());
//            }
//
//            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(weakReferenceContext.get(),
//                    android.R.layout.simple_spinner_item, list);
//            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(weakReferenceContext.get());
//            builder.setTitle("Warning !");
//            builder.setMessage("Printer is not connected. Please select your printer name..");
//
//
//            LinearLayout linearLayout = new LinearLayout(weakReferenceContext.get());
//            linearLayout.setOrientation(LinearLayout.VERTICAL);
//            linearLayout.setPadding(60,20,30,20);
//            Spinner spinner = new Spinner(weakReferenceContext.get());
//            spinner.setAdapter(dataAdapter);
//            linearLayout.addView(spinner);
//            builder.setView(linearLayout);
//
//// Set up the buttons
//            builder.setPositiveButton("Save & Update", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    if(list.size() > 0){
//                        int id = (int) spinner.getSelectedItemId();
//                        shared.setPrinterName(list.get(id));
//                        printOPT.reInitialize();
//                    }
//
//                }
//            });
//            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                }
//            });
//
//            builder.show();
//
//            return;
//        }
//
//
//        if(printOPT.getMac()){
//
//            OnTaskCompleted onTaskCompleted = new OnTaskCompleted() {
//                @Override
//                public void onTaskCompleted() {
//                    dismissProgressDialog();
//                    onTaskCompletedParent.onTaskCompleted();
//                }
//            };
//
//            showProgressDialog();
//
//            if (invoicePrintModel == null) {
//                return;
//            }
//
//            new PrintAsyncTask(onTaskCompleted, printOPT).execute(invoicePrintModel);
//
//            onTaskCompleted.onTaskCompleted();
//            onTaskCompletedParent.onTaskCompleted();
//
//
//        }
//        else{
//
//            Toast.makeText(weakReferenceContext.get(), "Printer is disconnected", Toast.LENGTH_LONG).show();
//        }
//
//
//
//
//
//    }
//
//
//    private class PrintAsyncTask extends AsyncTask<InvoicePrintModel, Integer, Boolean> {
//
//        private OnTaskCompleted onTaskCompleted;
//        private PrintOPT printOPT;
//
//        public PrintAsyncTask(OnTaskCompleted onTaskCompleted, PrintOPT printOPT) {
//            this.onTaskCompleted = onTaskCompleted;
//            this.printOPT = printOPT;
//        }
//
//        @Override
//        protected Boolean doInBackground(InvoicePrintModel... invoicePrintModels) {
//            if (printOPT.isConnect()) {
//                doPrint(invoicePrintModels[0]);
//            }
//
//            return true;
//        }
//
//        protected void onProgressUpdate(Integer... progress) {
//
//        }
//
//        protected void onPostExecute(Boolean value) {
//            onTaskCompleted.onTaskCompleted();
//
//        }
//    }
//
//
//    private void doPrint(InvoicePrintModel invoicePrintModel) {
//
//
//        DecimalFormat df2 = new DecimalFormat("#,##0.00");
//        DecimalFormat df3 = new DecimalFormat("#,##0.0");
//
//        String name = invoicePrintModel.getCustomer().getName();
//        String address = invoicePrintModel.getCustomer().getFormatedAddress();
//        String contact = invoicePrintModel.getCustomer().getMobile();
//        String customer_code = String.valueOf(invoicePrintModel.getCustomer().getId());
//        String mobile = invoicePrintModel.getCustomer().getMobile();
//
//        Double otherDiscount = invoicePrintModel.getInvoice().invoice_other_discount;
//
//
//        try {
//            printOPT.setDefault();
//        } catch (Exception e) {
//        }
//        printOPT.leftAndRightInv("", "INV" + shared.getCurrentUser().deviceId + "-" + invoicePrintModel.getInvoice().invoice_code, 1);
//        printOPT.leftAndRightInv("", invoicePrintModel.getInvoice().getFormatedEffDate(), 1);
//
//        try {
//            printOPT.setDefault();
//        } catch (Exception e) {
//        }
//        printOPT.leftAndRight("", "", 1);
//        printOPT.writeAddressRow(" " + name + "(" + customer_code + ")", " ");
//
//        if (address.length() > 25) {
//            int length = address.length();
//            String address1 = address.substring(0, 22);
//            String address2 = address.substring(22, length);
//            printOPT.writeAddressRow(" " + address1, " ");
//            printOPT.writeAddressRow(" " + address2, "");
//        } else {
//            printOPT.writeAddressRow(" " + address, " ");
//        }
//
//
//        try {
//            printOPT.setSmall();
//            printOPT.writeRow("", "", "", "");
//            printOPT.writeRow("", "", "", "");
//            printOPT.writeRow("", "", "", "");
//            printOPT.setSmall();
//        } catch (Exception e) {
//        }
//
//        // invoice items
//
//        List<InvoiceItem> saleItemsList = invoicePrintModel.getSalesItems();
//        if (saleItemsList.size() > 0) {
//            for (InvoiceItem invoiceItem : saleItemsList) {
//                int type = 1;
//                String itemName = invoiceItem.item_name;
//                double itemPrice = invoiceItem.invoiceItemmrp;
//                double itemTotal = invoiceItem.invoiceItemTotalAmount+invoiceItem.invoiceItemTotalDiscount;
//                if (type == 1) {
//                    if (itemName.length() > 25) {
//
//                        checkLineCount("small");
//                        printOPT.write(itemName);
//                        checkLineCount("small");
//                        printOPT.writeRowNew(
//                                String.valueOf(invoiceItem.invoice_item_qty_s),
//                                String.format("%.2f", itemPrice),
//                                String.format("%.2f", itemTotal));
//                    } else {
//                        checkLineCount("small");
//                        printOPT.writeRow(itemName,
//                                String.valueOf(invoiceItem.invoice_item_qty_s),
//                                String.format("%.2f", itemPrice),
//                                String.format("%.2f", itemTotal));
//                    }
//
//                }
//            }
//        }
//
//        try {
//            printOPT.setDefault();
//            checkLineCount("default");
//            printOPT.writeVerticalLine();
//            String netSales = String.valueOf(String.format("%.2f", invoicePrintModel.getInvoice().invoice_net_total));
//            String total = String.valueOf(String.format("%.2f", invoicePrintModel.getInvoice().invoice_total));
//            checkLineCount("default");
//            printOPT.writeTotalRow("Total (after discount)", "", netSales);
//            checkLineCount("default");
//            printOPT.writeVerticalLine();
//            checkLineCount("default");
//            printOPT.write("");
//        } catch (Exception e) {
//        }
//
//
//        //RETURN ITEMS
//        List<InvoiceItem> returnItemsList = invoicePrintModel.getReturnItems();
//        if (returnItemsList.size() > 0) {
//            try {
//                checkLineCount("small");
//                printOPT.setSmall();
//                printOPT.write("Credit Note (Good Returns)");
//                printOPT.setDefault();
//                checkLineCount("default");
//                printOPT.writeVerticalLine();
//                printOPT.setSmall();
//            } catch (Exception e) {
//            }
//
//
//            for (InvoiceItem invoiceItem : returnItemsList) {
//                String rtnItem = invoiceItem.item_name;
//                double rtnItemPrice = invoiceItem.invoiceItemmrp;
//                double rtnItemTotal = invoiceItem.invoiceItemTotalAmount+invoiceItem.invoiceItemTotalDiscount;
//                if (invoiceItem.invoice_item_qty_s == 0) {
//                    Log.d("COLUMN_QTY", String.valueOf(invoiceItem.invoice_item_qty_s));
//                    if (rtnItem.length() > 25) {
//                        checkLineCount("small");
//                        printOPT.write(rtnItem);
//                        checkLineCount("small");
//                        printOPT.writeRowNew(
//                                String.valueOf((invoiceItem.invoice_item_qty_s * -1)),
//                                String.format("%.2f", rtnItemPrice),
//                                String.format("%.2f", rtnItemTotal));
//                    } else {
//                        checkLineCount("small");
//                        printOPT.writeRow(rtnItem,
//                                String.valueOf((invoiceItem.invoice_item_qty_s * -1)),
//                                String.format("%.2f", rtnItemPrice),
//                                String.format("%.2f", rtnItemTotal));
//                    }
//
//                } else {
//                    if (rtnItem.length() > 25) {
//                        checkLineCount("small");
//                        printOPT.write(rtnItem);
//                        checkLineCount("small");
//                        printOPT.writeRowNew(
//                                String.valueOf((invoiceItem.invoice_item_qty_s * -1)),
//                                String.format("%.2f", rtnItemPrice),
//                                String.format("%.2f", rtnItemTotal));
//                    } else {
//                        checkLineCount("small");
//                        printOPT.writeRow(rtnItem,
//                                String.valueOf((invoiceItem.invoice_item_qty_s * -1)),
//                                String.format("%.2f", rtnItemPrice),
//                                String.format("%.2f", rtnItemTotal));
//                    }
//                }
//            }
//
//            try {
//                printOPT.setDefault();
//                checkLineCount("default");
//                printOPT.writeVerticalLine();
//            } catch (Exception e) {
//            }
//
//            String rtn = String.valueOf(String.format("%.2f", invoicePrintModel.getInvoice().invoice_return_total));
//            checkLineCount("default");
//            printOPT.writeTotalRow("Total", "", rtn);
//
//            try {
//                printOPT.setDefault();
//                checkLineCount("default");
//                printOPT.writeVerticalLine();
//                checkLineCount("default");
//                printOPT.write("");
//            } catch (Exception e) {
//            }
//
//        }
//
//        // sample free issue
//        List<InvoiceItem> freeItemsList = invoicePrintModel.getFreeItems();
//        if (freeItemsList.size() > 0) {
//            try {
//
//                printOPT.setSmall();
//                checkLineCount("small");
//                printOPT.write(" Free Issue");
//                printOPT.setDefault();
//                checkLineCount("default");
//                printOPT.writeVerticalLine();
//                printOPT.setSmall();
//            } catch (Exception e) {
//            }
//            for (InvoiceItem invoiceItem : freeItemsList) {
//                String sampleItem = invoiceItem.item_name;
//                if (sampleItem.length() > 25) {
//                    checkLineCount("small");
//                    printOPT.write(sampleItem);
//                    checkLineCount("small");
//                    printOPT.writeRowNew(
//                            String.valueOf(invoiceItem.invoice_item_qty_s),
//                            String.valueOf(invoiceItem.invoiceItemmrp),
//                            String.valueOf(invoiceItem.invoiceItemTotalAmount));
//
//                } else {
//                    checkLineCount("small");
//                    printOPT.writeRow(invoiceItem.item_name,
//                            String.valueOf(invoiceItem.invoice_item_qty_s),
//                            String.valueOf(invoiceItem.invoiceItemmrp),
//                            String.valueOf(invoiceItem.invoiceItemTotalAmount));
//
//                }
//
//
//            }
//        }
//
//
//        // sample free issue
//        List<InvoiceItem> sampleItemsList = invoicePrintModel.getSalesItems();
//        if (freeItemsList.size() > 0) {
//            try {
//
//                printOPT.setSmall();
//                checkLineCount("small");
//                printOPT.write("Sample Issue");
//                printOPT.setDefault();
//                checkLineCount("default");
//                printOPT.writeVerticalLine();
//                printOPT.setSmall();
//            } catch (Exception e) {
//            }
//            for (InvoiceItem invoiceItem : sampleItemsList) {
//                String sampleItem = invoiceItem.item_name;
//                if (sampleItem.length() > 25) {
//                    checkLineCount("small");
//                    printOPT.write(sampleItem);
//                    checkLineCount("small");
//                    printOPT.writeRowNew(
//                            String.valueOf(invoiceItem.invoice_item_qty_s),
//                            "0.0",
//                            "0.0");
//
//                } else {
//                    checkLineCount("small");
//                    printOPT.writeRow(invoiceItem.item_name,
//                            String.valueOf(invoiceItem.invoice_item_qty_s),
//                            "0.0",
//                            "0.0");
//
//                }
//
//
//            }
//        }
//
//        try {
//            printOPT.setDefault();
//            checkLineCount("default");
//            printOPT.writeVerticalLine();
//            checkLineCount("default");
//            printOPT.write("");
//        } catch (Exception e) {
//        }
//
//        try {
//            printOPT.setDefault();
//
//            double discountTotalValue = invoicePrintModel.getInvoice().invoice_total_discount + invoicePrintModel.getInvoice().invoice_other_discount;
//            String total = String.valueOf(String.format("%.2f", invoicePrintModel.getInvoice().invoice_total));
//            String net = String.valueOf(String.format("%.2f", invoicePrintModel.getInvoice().invoice_net_total+discountTotalValue));
//            String rtnTotal = String.valueOf(String.format("%.2f", invoicePrintModel.getInvoice().invoice_return_total));
//            String discount = String.valueOf(String.format("%.2f", invoicePrintModel.getInvoice().invoice_total_discount));
//            String discount_val = String.valueOf(String.format("%.2f", invoicePrintModel.getInvoice().invoice_total_discount + invoicePrintModel.getInvoice().invoice_other_discount));
//
//
//            checkLineCount("default");
//            printOPT.writeVerticalLine();
//            printOPT.setDefault();
//            checkLineCount("default");
//            printOPT.writeTotalRow("Gross Total", ":", net);
//            checkLineCount("default");
//            printOPT.writeTotalRow("Discount", ":", discount_val);
//            checkLineCount("default");
//            printOPT.writeTotalRow("Return", ":", rtnTotal);
//            checkLineCount("default");
//            printOPT.writeTotalRow("Total", ":", total);
//            checkLineCount("default");
//            printOPT.write("");
//            checkLineCount("default");
//            printOPT.write("");
//            checkLineCount("default");
//            printOPT.setDefault();
//            checkLineCount("default");
//            printOPT.writeTotalRow("_________", "", "________");
//            checkLineCount("default");
//            printOPT.writeTotalRow("Signature", "", "Customer");
//            checkLineCount("default");
//            printOPT.writeRow("", "", "", "");
//
//            printOPT.setSmall();
//
//            checkLineCount("default");
//            printOPT.write("");
//            checkLineCount("default");
//            printOPT.write("");
//            checkLineCount("default");
//            printOPT.write("Technical Support :");
//        } catch (Exception e) {
//        }
//
//        printOPT.close();
//    }
//
//
//
//
//
//
//    public int checkLineCount(String type) {
//        if (lineCount > 50) {
//            nextPage = true;
////            printHeader(type);
//            lineCount = 1;
//            nextPage = false;
//            Log.d("Linecountexeed", String.valueOf(lineCount));
//            return lineCount;
//        } else {
//            lineCount++;
//            Log.d("Linecount", String.valueOf(lineCount));
//            return lineCount;
//        }
//    }
//
////    public void printHeader(String type) {
////        InvoiceDao invoiceDao = new InvoiceDao();
////        String name;
////        String address;
////        String customer_code;
////
////        name = invoiceDao.getCustomerName();
////        address = invoiceDao.getAddress();
////
////        customer_code = invoiceDao.getCustomerCode();
////        try {
////            printOPT.setSmall();
////            printOPT.writeRow("", "", "", "");
////
////        } catch (Exception e) {
////        }
////        try {
////            printOPT.setDefault();
////            printOPT.writeRow("", "", "", "");
////            printOPT.writeRow("", "", "", "");
////            printOPT.writeRow("", "", "", "");
////            printOPT.setSmall();
////        } catch (Exception e) {
////        }
////        try {
////            printOPT.setDefault();
////        } catch (Exception e) {
////        }
////        printOPT.leftAndRightInv("", "INV" + SessionManager.getInstance().getDevice() + "-" + SessionManager.getInstance().getPrintInvoice(), 1);
////        printOPT.leftAndRightInv("", invoiceDao.getInvDate(), 1);
////            /*printOPT.writeRowRight("INV"+SessionManager.getInstance().getDevice()+"-"+ SessionManager.getInstance().getPrintInvoice());
////            printOPT.writeRowRight(invoiceDao.getInvDate());*/
////        try {
////            printOPT.setDefault();
////        } catch (Exception e) {
////        }
//////            printOPT.writeRow("", "", "",  "");
////        printOPT.leftAndRight("", "", 1);
////            /*printOPT.leftAndRight( name + "(" + customer_code + ")","Stokist",1);
////            printOPT.leftAndRight( address,"Stokist",1);*/
////        printOPT.writeAddressRow(" " + name + "(" + customer_code + ")", " " + SessionManager.getInstance().getStockistName());
////
////        if (address.length() > 25) {
////            int length = address.length();
////            String address1 = address.substring(0, 22);
////            String address2 = address.substring(22, length);
////            printOPT.writeAddressRow(" " + address1, " " + SessionManager.getInstance().getStockistCode());
////            printOPT.writeAddressRow(" " + address2, "");
////        } else {
////            printOPT.writeAddressRow(" " + address, " " + SessionManager.getInstance().getStockistCode());
////        }
////
////
////        try {
////            printOPT.setSmall();
////            printOPT.writeRow("", "", "", "");
////            printOPT.writeRow("", "", "", "");
////            printOPT.writeRow("", "", "", "");
////            if (type == "default") {
////                printOPT.setDefault();
////            } else {
////                printOPT.setSmall();
////            }
////
////        } catch (Exception e) {
////        }
////    }
//
//
//    public void showProgressDialog() {
//        if (mProgressDialog == null) {
//            mProgressDialog = ProgressDialog.show(weakReferenceContext.get(), "Processing...", "Please wait");
//            mProgressDialog.setCancelable(false);
//        } else {
//            mProgressDialog.show();
//        }
//    }
//
//    public void dismissProgressDialog() {
//        if (mProgressDialog != null) {
//            mProgressDialog.dismiss();
//            mProgressDialog = null;
//        }
//    }
//}
