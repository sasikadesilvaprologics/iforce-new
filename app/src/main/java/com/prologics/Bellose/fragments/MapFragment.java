package com.prologics.Bellose.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.RetrofitInstance;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener,LocationListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private GoogleMap mMap;

    LocationManager locationManager;
    LocationListener locationListener;
    int minDistance = 0;
    int minTime = 10000;
    View v;
    private ProgressDialog mProgressDialog;
    double latitude = 0.0;
    double longitude = 0.0;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private CustomerViewModel customerViewModel;
    private Shared shared;
    private double selectedLatitude = 0.0;
    private double selectedLongitude = 0.0;

    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapFragment newInstance(String param1, String param2) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment

        mapFragment.getMapAsync(this);

        ImageView navBtn = rootView.findViewById(R.id.navigationButton);
        navBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNavigation(v);
            }
        });

         shared = Shared.getInstance(getContext());
         selectedLatitude = shared.getLocation().latitude;
         selectedLongitude = shared.getLocation().longitude;
        customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        Button update = rootView.findViewById(R.id.btn_updateNewLocation);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Customer customer =  customerViewModel.getCustomer(shared.getSelectedCustomerId());
              customer.setLatitude(String.valueOf(mMap.getMyLocation().getLatitude()));
                customer.setLongitude(String.valueOf(mMap.getMyLocation().getLongitude()));

                customerViewModel.insertAll(Collections.singletonList(customer), new OnTaskCompleted() {
                    @Override
                    public void onTaskCompleted() {
//                        customerViewModel.
                        updateCustomer();
//                        mMap.clear();
//                        LatLng customerLocation = new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude() );
//                        mMap.addMarker(new MarkerOptions()
//                                .position(customerLocation)
//                                .title(customer.getName()));
//
//                        mMap.animateCamera(CameraUpdateFactory.zoomIn());
//                        CameraPosition cameraPosition = new CameraPosition.Builder()
//                                .target(customerLocation)      // Sets the center of the map to Mountain View
//                                .zoom(17)                   // Sets the zoom
//                                //  .bearing(90)                // Sets the orientation of the camera to east
//                                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
//                                .build();                   // Creates a CameraPosition from the builder
//                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                });
            }
        });



        return rootView;
    }


    private void updateCustomer(){
        Customer customer =  customerViewModel.getCustomer(shared.getSelectedCustomerId());
        customer.setLatitude(String.valueOf(mMap.getMyLocation().getLatitude()));
        customer.setLongitude(String.valueOf(mMap.getMyLocation().getLongitude()));
        mMap.clear();
        LatLng customerLocation = new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude() );
        mMap.addMarker(new MarkerOptions()
                .position(customerLocation)
                .title(customer.getName()));

        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(customerLocation)      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom
                //  .bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.updateCustomer(shared.getSelectedCustomerId(),
                customerLocation.latitude,customerLocation.longitude);

        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    System.out.println("====locatin update====1===>>>>>>"+response.body());
                    Toast.makeText(getContext(),"Updated",Toast.LENGTH_LONG).show();
                    selectedLatitude = customerLocation.latitude;
                    selectedLongitude = customerLocation.longitude;
                }
                System.out.println("====locatin update==3=====>>>>>>"+response.body());




            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("====locatin update err=======>>>>>>"+t.getMessage());
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);

         customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        Shared shared = Shared.getInstance(getContext());
        Customer customer = customerViewModel.getCustomer(shared.getSelectedCustomerId());

        latitude = Double.parseDouble(customer.getLatitude());
        longitude = Double.parseDouble(customer.getLongitude());


        LatLng customerLocation = new LatLng(Double.parseDouble(customer.getLatitude()), Double.parseDouble(customer.getLongitude()));
        selectedLatitude = customerLocation.latitude;
        selectedLongitude = customerLocation.longitude;

        mMap.addMarker(new MarkerOptions()
                .position(customerLocation)
                .title(customer.getName()));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(customerLocation, 15));

        // Zoom in, animating the camera.
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(customerLocation)      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom
                //  .bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));




    }

    @Override
    public void onLocationChanged(Location location) {


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }




    public void openNavigation(View v){
//        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
        Shared shared = Shared.getInstance(getContext());
        String uri = "http://maps.google.com/maps?saddr=" + shared.getLocation().latitude + "," + shared.getLocation().longitude+ "&daddr=" + selectedLatitude + "," + selectedLongitude;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMap.clear();
    }
}
