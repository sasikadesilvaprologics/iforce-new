package com.prologics.Bellose.fragments.dialogs;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.InputFilterMinMax;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.services.ConnectivityViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.FREE_ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_DRAFT_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_PRINTED_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_SAVED_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.SAMPLE_ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.getDate;

public class ItemAddAndUpdateDialog extends DialogFragment {

    private ConnectivityViewModel connectivityViewModel;
    private View.OnClickListener cancelListner;
    private ImageView close;
    private Item item = new Item();
    private EditText batchPrice;
    private EditText discount;
    private Button addButton;
    private ImageView addCount;
    private ImageView removeCount;
    private Button freeIssueButton;
    private Button sampleButton;
    private EditText countText;
    private Shared shared;
    private InvoiceViewModel invoiceViewModel;
    private InvoiceItemViewModel invoiceItemViewModel;
    private CustomerViewModel customerViewModel;
    private ItemViewModel itemViewModel;
    private Invoice invoice;
    private RadioButton percentageRadioButton;
    private RadioButton valueRadioButton;
    private Boolean isEnablePercentage = true;
    private boolean isPreSale = false;


    public void setInvoiceItem(Item item) {
        this.item = item;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public ItemAddAndUpdateDialog(Item item, boolean isPreSale) {


        setInvoiceItem(item);
        this.isPreSale = isPreSale;


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_item_popup, container, false);

        shared = Shared.getInstance(getContext());
        invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);


        System.out.println("========min====" + item.minPrice + "==========max======" + item.maxPrice);

        batchPrice = v.findViewById(R.id.batchPrice);
        discount = v.findViewById(R.id.discount);
        addButton = v.findViewById(R.id.addButton);
        addCount = v.findViewById(R.id.addCountButton);
        removeCount = v.findViewById(R.id.removeCountButton);
        freeIssueButton = v.findViewById(R.id.freeIssueButton);
        sampleButton = v.findViewById(R.id.sampleButton);
        countText = v.findViewById(R.id.countText);
        close = v.findViewById(R.id.closeButton);
        percentageRadioButton = v.findViewById(R.id.percentageRadioButton);
        valueRadioButton = v.findViewById(R.id.valueRadioButton);
        batchPrice.setText(String.valueOf(item.sellingPrice));


//        batchPrice.setFilters(new InputFilter[]{
//                new InputFilterMinMax(0, item.maxPrice+1),
//        });
        if(!isPreSale){
            countText.setFilters(new InputFilter[]{
                    new InputFilterMinMax(1, item.qty),
            });
        }


//        discount.setFilters(new InputFilter[]{
//                new InputFilterMinMax(0, 100),
//        });


        percentageRadioButton.toggle();
        System.out.println("===_____-------======>>>>>>>>"+shared.getSelectedCustomerDisc());
        discount.setText(String.valueOf(shared.getSelectedCustomerDisc()) );
        percentageRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isEnablePercentage = isChecked;
                discount.setText("");
                if (isChecked) {
                    discount.setText(String.valueOf(shared.getSelectedCustomerDisc()) );
//                    discount.setFilters(new InputFilter[]{
//                            new InputFilterMinMax(0, 100),
//                    });

                } else {
//                    discount.setFilters(new InputFilter[]{
//                            new InputFilterMinMax(0, item.maxPrice),
//                    });


                }
            }
        });


        addCount.setOnClickListener(new OnAddCountClickListner());
        removeCount.setOnClickListener(new OnRemoveountClickListner());
        addButton.setOnClickListener(new OnAddItemListner());
        freeIssueButton.setOnClickListener(new OnFreeIssueItemListner());
        sampleButton.setOnClickListener(new OnSampleItemListner());


        invoiceViewModel.getInvoiceById(shared.getSelectedInv()).observe(getViewLifecycleOwner(), new Observer<Invoice>() {
            @Override
            public void onChanged(Invoice invoice) {
                setInvoice(invoice);
            }
        });


        return v;
    }

    private class OnAddCountClickListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(countText.getText().toString().isEmpty()){
                return;
            }
            int currentValue = Integer.parseInt(countText.getText().toString());
            if(isPreSale){
                countText.setText(String.valueOf(currentValue += 1));
                return;
            }

            if (currentValue > item.qty - 1) {
                Toast.makeText(getContext(), "Max quantity reach", Toast.LENGTH_SHORT).show();
            } else {
                countText.setText(String.valueOf(currentValue += 1));
            }


        }
    }

    private class OnRemoveountClickListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(countText.getText().toString().isEmpty()){
                return;
            }
            int currentValue = Integer.parseInt(countText.getText().toString());
            if (currentValue - 1 >= 1) {
                countText.setText(String.valueOf(currentValue -= 1));
            }

        }
    }


    private class OnAddItemListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            double price = Double.parseDouble(batchPrice.getText().toString());
            double discountValue = discount.getText().toString().isEmpty() ? 0 : Double.parseDouble(discount.getText().toString());


            if(isEnablePercentage && discountValue > 100){
                Toast.makeText(getContext(), "Discount must be less than 100", Toast.LENGTH_SHORT).show();
                return;
            }

            if(!isEnablePercentage && discountValue > item.maxPrice){
                Toast.makeText(getContext(), "Discount must be less than "+item.maxPrice, Toast.LENGTH_SHORT).show();
                return;
            }

            if(price < item.minPrice || price > item.maxPrice){
                Toast.makeText(getContext(), "Price must be greater than "+item.minPrice+" and less than "+item.maxPrice, Toast.LENGTH_SHORT).show();
                return;
            }

            if(countText.getText().toString().length() > 4){
                Toast.makeText(getContext(), "Max quantity is 9999", Toast.LENGTH_SHORT).show();
                return;
            }

            addInvoiceItem(ITEM_CODE);

            if(isEnablePercentage && ITEM_CODE == 1){
                Customer customer = customerViewModel.getCustomer(shared.getSelectedCustomerId());
                customer.discount = discountValue;

                customerViewModel.insertAll(Collections.singletonList(customer), new OnTaskCompleted() {
                    @Override
                    public void onTaskCompleted() {
                        shared.setSelectedCustomerDisc(discountValue);
                    }
                });
            }


        }
    }

    private class OnFreeIssueItemListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            addInvoiceItem(FREE_ITEM_CODE);
        }
    }

    private class OnSampleItemListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            addInvoiceItem(SAMPLE_ITEM_CODE);

        }
    }

    private void addInvoiceItem(int type) {
        if(invoice != null){
            if(invoice.flag == INVOICE_SAVED_FLAG || invoice.flag == INVOICE_PRINTED_FLAG  ){
                return;
            }
        }
        addInvoiceDraft(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {
                insertInvoiceItem(type);

            }
        });
    }

    private void insertInvoiceItem(int type) {
        if(countText.getText().toString().isEmpty()){
            Toast.makeText(getContext(), "Please add item quantity", Toast.LENGTH_SHORT).show();
            return;
        }

        double price = Double.parseDouble(batchPrice.getText().toString());
        if (batchPrice.getText().toString().isEmpty()) {
            price = item.sellingPrice;
        }

        if (type == SAMPLE_ITEM_CODE ||
                type == FREE_ITEM_CODE) {
            price = 0.00;
        }

        double qtySellerble = Double.parseDouble(countText.getText().toString());



        InvoiceItem invoiceItem = invoiceItemViewModel.checkInvoiceItem(
                shared.getSelectedInv(),
                item.item_id,
                price,
                getDiscountValue(price),
                type
        );




        if (invoiceItem == null) {
            invoiceItem = new InvoiceItem();
        }

        System.out.println("=====<<KKKLLLLLLLLLLL>>?????" + invoiceItem.invoice_item_qty_s);
        qtySellerble = qtySellerble + invoiceItem.invoice_item_qty_s;
//        invoiceItem.customerId = shared.getSelectedCustomerId();

        invoiceItem.deviceId = Integer.parseInt(shared.getCurrentUser().deviceId);
        invoiceItem.invoiceId = shared.getSelectedInv();
        invoiceItem.items_id = item.item_id;
        invoiceItem.invoiceItemTotalDiscount = getDiscountValue(price)* qtySellerble;
        invoiceItem.invoiceItemTotalAmount = (price - getDiscountValue(price)) * qtySellerble;
        invoiceItem.invoiceItemDiscount = getDiscountValue(price);
        invoiceItem.invoiceItemmrp = price;
        if(isPreSale){
            invoiceItem.isPreSalse = true;
        }
        invoiceItem.invoice_item_qty_ns = 0;
        invoiceItem.invoice_item_qty_s = qtySellerble;
        invoiceItem.invoice_item_flag = type;
        invoiceItem.updatedAt = getDate();


        invoiceItemViewModel.insertAll(Collections.singletonList(invoiceItem), new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {

                onDismiss();
                if(!isPreSale){
                    stockUpdate(Double.parseDouble(countText.getText().toString()));
                }

            }
        });
    }

    private void stockUpdate(double qty){
        Item itemV2 = item;
        itemV2.qty = itemV2.qty - qty;
        itemV2.updatedAt = getDate();

        itemViewModel.insertItems(Collections.singletonList(itemV2), () -> {

        });
    }


    private double getDiscountValue(double price) {
        if (discount.getText().toString().isEmpty() || price == 0.00) {

            return 0.0;
        }
        double discountValue = Double.parseDouble(discount.getText().toString());
        if (isEnablePercentage) {
            return (price / 100) * discountValue;
        } else {
            return discountValue;
        }
    }

    private void addInvoiceDraft(OnTaskCompleted onTaskCompleted) {
        if (invoice == null) {

            Invoice invoice = new Invoice();
            invoice.invoice_code = shared.getSelectedInv();
//            invoice.customerId = shared.getSelectedCustomerId();
            invoice.deviceId = Integer.parseInt(shared.getCurrentUser().deviceId);
            invoice.isSynced = false;
            invoice.flag = INVOICE_DRAFT_FLAG;

            List<Invoice> invoiceList = new ArrayList<>();
            invoiceList.add(invoice);

            invoiceViewModel.insert(invoiceList, new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {
                    onTaskCompleted.onTaskCompleted();
                }
            });
        } else {
            onTaskCompleted.onTaskCompleted();
        }

    }

    private void onDismiss() {
        this.dismiss();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //anyText.setText(String.valueOf(anything));
        close.setOnClickListener(cancelListner);
//        execute.setOnClickListener(acceptListner);
    }

    public void setCancelListner(View.OnClickListener cancelListner) {
        this.cancelListner = cancelListner;
    }


}
