package com.prologics.Bellose.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.InvoiceReprintActivity;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnInvoicePreviewListner;
import com.prologics.Bellose.recyclerViews.InvoiceRecyclerViewAdapter;

import java.util.ArrayList;

import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_NUMBER_KEY;


public class InvoiceFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private InvoiceViewModel invoiceViewModel;
    private Shared shared;
    private  boolean isPreSale = false;
    private InvoiceRecyclerViewAdapter invoiceRecyclerViewAdapter;

    public  InvoiceFragment (boolean isPreSale) {


       this.isPreSale = isPreSale;

        // Required empty public constructor

    }

//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
//     * @return A new instance of fragment InvoiceFragment.
//     */
//    // TODO: Rename and change types and number of parameters
//    public static InvoiceFragment newInstance(String param1, String param2) {
//        InvoiceFragment fragment = new InvoiceFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invoice, container, false);


        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.listView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));



        shared = Shared.getInstance(getContext());


        invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);









        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        OnInvoicePreviewListner onInvoicePreviewListner = new OnInvoicePreviewListner() {
            @Override
            public void onPreview(Invoice invoice) {
                Intent intent = new Intent(getActivity(), InvoiceReprintActivity.class);
                intent.putExtra(INVOICE_NUMBER_KEY,invoice.invoice_code);
                intent.putExtra("isPreSale",isPreSale);
                startActivity(intent);
            }
        };

        ArrayList<Invoice> dummyData = new ArrayList<>();
        invoiceRecyclerViewAdapter = new InvoiceRecyclerViewAdapter(getActivity(),dummyData,onInvoicePreviewListner);
        recyclerView.setAdapter(invoiceRecyclerViewAdapter);



        if(isPreSale){
            invoiceViewModel.getCustomerAllPreInvoices(shared.getSelectedCustomerId()).observe(getViewLifecycleOwner(), invoices -> {
                invoiceRecyclerViewAdapter.invoices = invoices;
                recyclerView.getAdapter().notifyDataSetChanged();
            });
        }
        else{
            invoiceViewModel.getCustomerAllInvoices(shared.getSelectedCustomerId()).observe(getViewLifecycleOwner(), invoices -> {
                invoiceRecyclerViewAdapter.invoices = invoices;
                recyclerView.getAdapter().notifyDataSetChanged();

                for(Invoice invoice : invoices){
                    System.out.println("========>>====>>>>>"+invoice.customerId);
                }

            });
        }
    }




    @Override
    public void onStart() {
        super.onStart();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(invoiceViewModel != null && invoiceViewModel.getCustomerAllInvoices(shared.getSelectedCustomerId()).hasObservers()){
            invoiceViewModel.getCustomerAllInvoices(shared.getSelectedCustomerId()).removeObservers(this);
        }
    }
}
