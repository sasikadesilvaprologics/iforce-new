package com.prologics.Bellose.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prologics.Bellose.activities.DashboardActivity;
import com.prologics.Bellose.activities.RootViewActivity;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerTypeViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnApiResponse;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.Entity.CustomerTypeResponse;
import com.prologics.Bellose.network.Entity.RootListResponse;
import com.prologics.Bellose.network.RetrofitInstance;
import com.prologics.Bellose.recyclerViews.RootListRecyclerViewAdapter;
import com.prologics.Bellose.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prologics.Bellose.services.DataSyncService.getCustomers;
import static com.prologics.Bellose.services.DataSyncService.runStockDataListApiService;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class RootViewFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private  RecyclerView recyclerView;
    private ProgressDialog mProgressDialog;
    private  CustomerTypeViewModel customerTypeViewModel;
    private CustomerViewModel customerViewModel;
    private ItemViewModel itemViewModel;
    private  Shared shared;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RootViewFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static RootViewFragment newInstance(int columnCount) {
        RootViewFragment fragment = new RootViewFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.root_fragment_list, container, false);
        customerTypeViewModel = new ViewModelProvider(this).get(CustomerTypeViewModel.class);
        customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);

        itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);

        shared = Shared.getInstance(getContext());
        mListener = new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(RootListResponse.Data data) {
                getCustomers(data.areaId,customerViewModel);
                Shared shared = Shared.getInstance(getContext());
                shared.setSelectedRoot(data);
                shared.setRootStatus(true);
                RootViewActivity rootViewActivity = (RootViewActivity)getContext();
                rootViewActivity.startActivity(new Intent(getActivity(), DashboardActivity.class));
                rootViewActivity.finish();
            }
        };

            Context context = view.getContext();
             recyclerView = view.findViewById(R.id.listView);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

        getRootList();

        return view;
    }

    private void getRootList(){
        showProgressDialog();


        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<RootListResponse> stringCall = getNoticeDataService.getRootList(Integer.parseInt(shared.getCurrentUser().deviceId));
        stringCall.enqueue(new Callback<RootListResponse>() {
            @Override
            public void onResponse(Call<RootListResponse> call, Response<RootListResponse> response) {

                if (response.isSuccessful()) {
                    RootListResponse rootListResponse = response.body();
                    recyclerView.setAdapter(new RootListRecyclerViewAdapter(rootListResponse.data, mListener));
                }
                getCustomerTypeList();
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<RootListResponse> call, Throwable t) {
                dismissProgressDialog();
                System.out.println("============>>>>>>>>"+t.getMessage());
            }
        });
    }

    private void getCustomerTypeList(){
        showProgressDialog();
        Shared shared = Shared.getInstance(getContext());

        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<CustomerTypeResponse> stringCall = getNoticeDataService.getCustomerTypeList(Integer.parseInt(shared.getCurrentUser().deviceId));
        stringCall.enqueue(new Callback<CustomerTypeResponse>() {
            @Override
            public void onResponse(Call<CustomerTypeResponse> call, Response<CustomerTypeResponse> response) {
                runStockDataListApiService(itemViewModel, Integer.parseInt(shared.getCurrentUser().deviceId), new OnApiResponse() {
                    @Override
                    public void onSuccess(String message) {

                    }

                    @Override
                    public void onError(String message) {

                    }
                });
                if (response.isSuccessful()) {
                    CustomerTypeResponse rootListResponse = response.body();

                    customerTypeViewModel.insertDummy(rootListResponse.dataList, new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted() {
                            System.out.println("=======>>>>MMMMMMBBBBBBB======>>"+rootListResponse.dataList.size());
                        }
                    });
                }
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<CustomerTypeResponse> call, Throwable t) {
                dismissProgressDialog();
                System.out.println("============>>>>>>>>"+t.getMessage());
                runStockDataListApiService(itemViewModel, Integer.parseInt(shared.getCurrentUser().deviceId), new OnApiResponse() {
                    @Override
                    public void onSuccess(String message) {

                    }

                    @Override
                    public void onError(String message) {

                    }
                });
            }
        });
    }






    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            mListener = (OnListFragmentInteractionListener) context;
////            throw new RuntimeException(context.toString()
////                    + " must implement OnListFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(RootListResponse.Data data);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getContext(), "Downloading data...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}
