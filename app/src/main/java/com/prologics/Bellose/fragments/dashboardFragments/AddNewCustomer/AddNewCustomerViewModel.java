package com.prologics.Bellose.fragments.dashboardFragments.AddNewCustomer;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AddNewCustomerViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AddNewCustomerViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is gallery fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}