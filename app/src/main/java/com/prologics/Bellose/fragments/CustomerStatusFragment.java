package com.prologics.Bellose.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.BATTERY_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CustomerStatusFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CustomerStatusFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CustomerStatusFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private Spinner typeSpinner;
    private EditText remarkText;
    private Button updateButton;
    private JSONArray typeList;
    private Shared shared;

    public CustomerStatusFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CustomerStatusFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CustomerStatusFragment newInstance(String param1, String param2) {
        CustomerStatusFragment fragment = new CustomerStatusFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_customer_status, container, false);

        typeSpinner = view.findViewById(R.id.customer_status);
        remarkText = view.findViewById(R.id.customer_remark);
        updateButton = view.findViewById(R.id.btn_remark_update);
        shared = Shared.getInstance(getContext());

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCustomerStatus();


            }
        });

        loadCustomerStatusList();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void updateCustomerStatus() {
        int id = (int) typeSpinner.getSelectedItemId();
        String remark = remarkText.getText().toString();
        int batLevel = 0;
        try {
            BatteryManager bm = (BatteryManager) getActivity().getSystemService(BATTERY_SERVICE);
             batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
             System.out.println("=====+++++++???>>>>>>"+batLevel);
        }catch (Exception e){

        }

        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.updateCustomerStatus(
                shared.getSelectedCustomerId(),
                Integer.parseInt(shared.getCurrentUser().deviceId),
                id,
                batLevel,
                remark,
                shared.getLocation().latitude,
                shared.getLocation().longitude
                );


        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
//                System.out.println("=======response ======>>>>>>>>"+response.body());
                if(response.isSuccessful()){
                    Toast.makeText(getContext(),"Customer status updated successfull",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }


    private void loadCustomerStatusList() {

        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.loadCustomerStatusList();

        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                System.out.println("======>>>???>>>>>???______1");
                if (response.isSuccessful()) {
                    System.out.println("======>>>???>>>>>???______2");
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        typeList = jsonObject.getJSONArray("data");
                        System.out.println("======>>>?????>>>>" + typeList);
                        List<String> list = new ArrayList<String>();
                        for (int i = 0; i < typeList.length(); i++) {
                            JSONObject type = typeList.getJSONObject(i);
                            list.add(type.getString("name"));
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        typeSpinner.setAdapter(dataAdapter);
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("======>>>???>>>>>???______3" + t.getMessage());
            }
        });
    }
}
