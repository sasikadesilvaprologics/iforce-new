package com.prologics.Bellose.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.fragments.dummy.DummyContent;
import com.prologics.Bellose.helper.callbacks.OnInvoiceItemClickListner;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.recyclerViews.NewInvoiceRecyclerViewAdapter;

import java.util.Collections;
import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.RETURN_ITEM_CODE;

public class NewInvoiceReturnFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private NewInvoiceFragment.OnListFragmentInteractionListener mListener;
    private InvoiceItemViewModel invoiceItemViewModel;
    private Shared shared;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewInvoiceReturnFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static NewInvoiceReturnFragment newInstance(int columnCount) {
        NewInvoiceReturnFragment fragment = new NewInvoiceReturnFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_invoice, container, false);

        shared = Shared.getInstance(getContext());
        invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        // Set the adapter

        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        OnInvoiceItemClickListner invoiceItemRemoveListner = new OnInvoiceItemClickListner() {
            @Override
            public void onDelete(InvoiceItem invoiceItem) {
                onDeleteItems(Collections.singletonList(invoiceItem));
            }
            @Override
            public void onClick(InvoiceItem invoiceItem) {
//                ChangeInvoiceItemCountDialog itemAddAndUpdateDialog = new ChangeInvoiceItemCountDialog(invoiceItem);
//                itemAddAndUpdateDialog.show(getActivity().getSupportFragmentManager(), "ItemCountDialog");

            }
        };



        invoiceItemViewModel.getItemsByInvoiceId(shared.getSelectedInv(),RETURN_ITEM_CODE).observe(getViewLifecycleOwner(), new Observer<List<InvoiceItem>>() {
            @Override
            public void onChanged(List<InvoiceItem> invoiceItems) {
                recyclerView.setAdapter(new NewInvoiceRecyclerViewAdapter(invoiceItems, mListener,invoiceItemRemoveListner));
            }
        });



        return view;
    }

    private void onDeleteItems(List<InvoiceItem> invoiceItems){
        new AlertDialog.Builder(getContext())
                .setTitle("Warning !")
                .setMessage("Are you sure, do you need to remove this item?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        invoiceItemViewModel.onDelete(invoiceItems, new OnTaskCompleted() {
                            @Override
                            public void onTaskCompleted() {

                            }
                        });
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.cancel, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyContent.DummyItem item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(invoiceItemViewModel != null && invoiceItemViewModel.getItemsByInvoiceId(shared.getSelectedInv()).hasObservers()){
            System.out.println("===============.>>>>>>>>>>>>>remove self return new");
            invoiceItemViewModel.getItemsByInvoiceId(shared.getSelectedInv()).removeObservers(this);
        }
    }
}
