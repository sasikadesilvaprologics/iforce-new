package com.prologics.Bellose.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.NewInvoiceActivity;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.fragments.dialogs.ChangeInvoiceItemCountDialog;
import com.prologics.Bellose.fragments.dummy.DummyContent.DummyItem;
import com.prologics.Bellose.helper.callbacks.OnInvoiceItemClickListner;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.recyclerViews.NewInvoiceRecyclerViewAdapter;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_PRINTED_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_SAVED_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.getDate;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class NewInvoiceFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private InvoiceItemViewModel invoiceItemViewModel;
    private ItemViewModel itemViewModel;
    private Shared shared;
    private WeakReference<NewInvoiceActivity> newInvoiceActivityWeakReference;
    private boolean isPreSale = false;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewInvoiceFragment(boolean isPreSale) {
        this.isPreSale = isPreSale;
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static NewInvoiceFragment newInstance(int columnCount,boolean isPreSale) {
        NewInvoiceFragment fragment = new NewInvoiceFragment(isPreSale);
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_invoice, container, false);

        shared = Shared.getInstance(getContext());
        newInvoiceActivityWeakReference = new WeakReference<>((NewInvoiceActivity)getActivity());

        invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);

        // Set the adapter

        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        OnInvoiceItemClickListner itemRemoveListner = new OnInvoiceItemClickListner() {
            @Override
            public void onDelete(InvoiceItem invoiceItem) {
                Invoice invoice = newInvoiceActivityWeakReference.get().getSelectedInvoice();
                if(invoice.flag == INVOICE_SAVED_FLAG || invoice.flag == INVOICE_PRINTED_FLAG  ){
                    return;
                }
                onDeleteItems(Collections.singletonList(invoiceItem));
                if(!isPreSale){
                    onStockUpdate(invoiceItem);
                }

            }

            @Override
            public void onClick(InvoiceItem invoiceItem) {
                Invoice invoice = newInvoiceActivityWeakReference.get().getSelectedInvoice();
                if(invoice.flag == INVOICE_SAVED_FLAG || invoice.flag == INVOICE_PRINTED_FLAG  ){
                    return;
                }
                ChangeInvoiceItemCountDialog itemAddAndUpdateDialog = new ChangeInvoiceItemCountDialog(invoiceItem,isPreSale);
                itemAddAndUpdateDialog.show(getActivity().getSupportFragmentManager(), "ItemCountDialog");

            }
        };

        if(isPreSale){
            invoiceItemViewModel.getPreSalseItems(shared.getSelectedInv()).observe(getViewLifecycleOwner(), new Observer<List<InvoiceItem>>() {
                @Override
                public void onChanged(List<InvoiceItem> invoiceItems) {
                    newInvoiceActivityWeakReference.get().preSaleInvoiceItemList = invoiceItems;
                    recyclerView.setAdapter(new NewInvoiceRecyclerViewAdapter(invoiceItems, mListener, itemRemoveListner));
                }
            });
        }
        else{
            invoiceItemViewModel.getItemsWithoutReturns(shared.getSelectedInv()).observe(getViewLifecycleOwner(), new Observer<List<InvoiceItem>>() {
                @Override
                public void onChanged(List<InvoiceItem> invoiceItems) {
                    newInvoiceActivityWeakReference.get().preSaleInvoiceItemList = invoiceItems;
                    recyclerView.setAdapter(new NewInvoiceRecyclerViewAdapter(invoiceItems, mListener, itemRemoveListner));
                }
            });
        }




        return view;
    }


    private void onDeleteItems(List<InvoiceItem> invoiceItems) {
        new AlertDialog.Builder(getContext())
                .setTitle("Warning !")
                .setMessage("Are you sure, do you need to remove this item?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        invoiceItemViewModel.onDelete(invoiceItems, new OnTaskCompleted() {
                            @Override
                            public void onTaskCompleted() {

                            }
                        });
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.cancel, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    private void onStockUpdate(InvoiceItem invoiceItem) {

        Item item = itemViewModel.getItem(invoiceItem.items_id);
        if (item != null) {
            item.qty = item.qty + invoiceItem.invoice_item_qty_s;
            item.updatedAt = getDate();
            itemViewModel.insertItems(Collections.singletonList(item), () -> {

            });
        }

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if( invoiceItemViewModel != null && invoiceItemViewModel.getItemsWithoutReturns(shared.getSelectedInv()).hasObservers()){
            System.out.println("===============.>>>>>>>>>>>>>remove new in observe");
            invoiceItemViewModel.getItemsWithoutReturns(shared.getSelectedInv()).removeObservers(this);
        }


    }
}
