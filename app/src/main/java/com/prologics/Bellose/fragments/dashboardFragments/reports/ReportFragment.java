package com.prologics.Bellose.fragments.dashboardFragments.reports;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.reports.InventoryReportActivity;
import com.prologics.Bellose.activities.reports.InvoiceReportActivity;
import com.prologics.Bellose.activities.reports.SalesDetailsReportActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class ReportFragment extends Fragment {

    private static int INVOICE_START = 0;
    private static int INVOICE_END = 0;
//    private static int INVOICE_START = 0;
//    private static int INVOICE_START = 0;

    private String invoiceStartDate;
    private String invoiceEndDate;

    private String salesStartDate;
    private String salesEndDate;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_reports, container, false);


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat2.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        Date date = new Date();

        TextView invoiceFrom = root.findViewById(R.id.invoiceFrom);
        TextView invoiceTo = root.findViewById(R.id.invoiceTo);
        TextView salesFrom = root.findViewById(R.id.salesFrom);
        TextView salesTo = root.findViewById(R.id.salesTo);

        invoiceStartDate = dateFormat2.format(date);
        invoiceEndDate = dateFormat2.format(date);
        salesStartDate = dateFormat2.format(date);
        salesEndDate = dateFormat2.format(date);
        invoiceFrom.setText(dateFormat.format(date));
        invoiceTo.setText(dateFormat.format(date));
        salesFrom.setText(dateFormat.format(date));
        salesTo.setText(dateFormat.format(date));

        invoiceFrom.setOnClickListener(new DateOnclick(new OnDateSelect() {
            @Override
            public void onSelect(String date) {
                invoiceStartDate = date;
            }

        }));
        invoiceTo.setOnClickListener(new DateOnclick(new OnDateSelect() {
            @Override
            public void onSelect(String date) {
                invoiceEndDate = date;
            }
        }));

        salesFrom.setOnClickListener(new DateOnclick(new OnDateSelect() {
            @Override
            public void onSelect(String date) {
                salesStartDate = date;
            }
        }));

        salesTo.setOnClickListener(new DateOnclick(new OnDateSelect() {
            @Override
            public void onSelect(String date) {
                salesEndDate = date;
            }
        }));


        Button button = root.findViewById(R.id.btn_view_inventory_report);
        Button invoiceReportButton = root.findViewById(R.id.btn_view_invoice_report);
        Button salesReportButton = root.findViewById(R.id.btn_view_sales_details);
        invoiceReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (invoiceStartDate != null || invoiceEndDate != null) {
                    Intent intent = new Intent(getActivity(), InvoiceReportActivity.class);
                    System.out.println("============>>>>>>start======"+invoiceStartDate+"=======>>>>end=="+invoiceEndDate);
                    intent.putExtra("startDate", invoiceStartDate);
                    intent.putExtra("endDate", invoiceEndDate);
                    startActivity(intent);
                }

            }
        });
        salesReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (salesStartDate != null || salesEndDate != null) {
                    System.out.println("=========start date =====>>"+salesStartDate+" 01:00:00");
                    System.out.println("=========end date =====>>"+salesEndDate +" 23:00:00");
                    Intent intent = new Intent(getActivity(), SalesDetailsReportActivity.class);
                    intent.putExtra("startDate", salesStartDate);
                    intent.putExtra("endDate", salesEndDate);
                    startActivity(intent);
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), InventoryReportActivity.class));
            }
        });


        return root;
    }


    private class DateOnclick implements View.OnClickListener {

        private DatePickerDialog picker;
        private OnDateSelect onDateSelect;

        DateOnclick(OnDateSelect onDateSelect) {
            this.onDateSelect = onDateSelect;
        }


        @Override
        public void onClick(View v) {
            TextView dateText = (TextView) v;
            final Calendar cldr = Calendar.getInstance();
            int day = cldr.get(Calendar.DAY_OF_MONTH);
            int month = cldr.get(Calendar.MONTH);
            int year = cldr.get(Calendar.YEAR);
            // date picker dialog

            picker = new DatePickerDialog(getContext(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//
                            String monthStr = Integer.toString((monthOfYear + 1));
                            String dayOfMonthStr = Integer.toString((dayOfMonth));
                            if (monthStr.length() == 1) {
                                monthStr = "0" + monthStr;
                            }
                            if (dayOfMonthStr.length() == 1) {
                                dayOfMonthStr = "0" + dayOfMonthStr;
                            }
                            onDateSelect.onSelect(year + "-" + monthStr + "-" + dayOfMonthStr);

                            dateText.setText(dayOfMonthStr + "/" + monthStr + "/" + year);
                        }
                    }, year, month, day);
            picker.getDatePicker().setMaxDate(new Date().getTime());
            picker.show();

        }
    }


    private interface OnDateSelect {
        void onSelect(String date);
    }
}