package com.prologics.Bellose.fragments.dashboardFragments.AddNewCustomer;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.DashboardActivity;
import com.prologics.Bellose.databaseModule.Entity.CustomerType;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerTypeViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.Entity.CustomerCreateResponse;
import com.prologics.Bellose.network.RetrofitInstance;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static com.prologics.Bellose.databaseModule.GlobalValues.getDate;


public class AddNewCustomerFragment extends Fragment implements
        NavigationView.OnNavigationItemSelectedListener, LocationListener, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener, OnMapReadyCallback {
    private Button createCusBtn;
    private ImageView imageview;
    private EditText cusName;
    private EditText addAddressN;
    private EditText addAddressL;
    private EditText addAddressL2;
    private EditText addAddStreet;
    private EditText contactP;
    private EditText addMobile;
    private EditText addLandLine;
    private EditText addDiscount;
    private Bitmap imgBitmap;
    private TextView type;
    private Spinner customerTypeSpinner;
    private ProgressDialog mProgressDialog;
    private static final String IMAGE_DIRECTORY = "/bellose";
    private int GALLERY = 1, CAMERA = 2;
    Location mLastLocation;
    String path;
    LocationManager locationManager;
    String mprovider;

    double latitude, longitude;
    String battery;
    boolean GpsStatus;
    Context context;

    private AddNewCustomerViewModel addNewCustomerViewModel;
    private CustomerViewModel customerViewModel;
    private CustomerTypeViewModel customerTypeViewModel;
    private FusedLocationProviderClient fusedLocationClient;
    private List<CustomerType> customerTypeList = new ArrayList<CustomerType>();
    private Shared shared;
    private WeakReference<DashboardActivity>  dashboardActivity;
    private File imageFile;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        addNewCustomerViewModel =
                new ViewModelProvider(this).get(AddNewCustomerViewModel.class);

        View root = inflater.inflate(R.layout.fragment_add_new_customer, container, false);
         dashboardActivity = new WeakReference<> ((DashboardActivity) getActivity());
//        final TextView textView = root.findViewById(R.id.text_gallery);
        customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        customerTypeViewModel = new ViewModelProvider(this).get(CustomerTypeViewModel.class);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        imageview = root.findViewById(R.id.openCam);
        cusName = root.findViewById(R.id.cusName);
        addAddressN = root.findViewById(R.id.addAddressN);
        addAddressL = root.findViewById(R.id.addAddressL);
        addAddressL2 = root.findViewById(R.id.addAddressL2);
        addAddStreet = root.findViewById(R.id.addAddStreet);
        contactP = root.findViewById(R.id.contactP);
        addMobile = root.findViewById(R.id.addMobile);
        addLandLine = root.findViewById(R.id.addLandLine);
        createCusBtn = root.findViewById(R.id.createCusBtn);
        type = root.findViewById(R.id.type);
        addDiscount = root.findViewById(R.id.addDiscount);
        customerTypeSpinner = root.findViewById(R.id.customer_type);
        shared = Shared.getInstance(getContext());
        customerTypeViewModel.getAllTypes().observe(getViewLifecycleOwner(), customerTypes -> {
            customerTypeList = customerTypes;
            List<String> list = new ArrayList<String>();
            for (CustomerType customerType : customerTypes) {
                list.add(customerType.customer_type_name);
            }
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            customerTypeSpinner.setAdapter(dataAdapter);
        });


        createCusBtn.setOnClickListener(v -> onSaveCustomer());
        imageview.setOnClickListener(v -> showPictureDialog());


        requestMultiplePermissions();


        return root;
    }


    private void setLocation() {

        fusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            System.out.println("=========location====<<<>>>>>>>>>>" + location);
            if (location != null) {
//
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            } else {
                latitude = 0.00;
                longitude = 0.00;
            }
        });

//
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            choosePhotoFromGallary();
                            break;
                        case 1:
                            takePhotoFromCamera();
                            break;
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    path = saveImage(bitmap);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    Bitmap bmp = BitmapFactory.decodeFile(path, options);
//                    imgBitmap =bitmap;
                    imgBitmap = bmp;
//                    Toast.makeText(AddNewCustomerActivity.this, path, Toast.LENGTH_SHORT).show();
                    imageview.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            imageview.setImageBitmap(bitmap);
            path = saveImage(bitmap);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            Bitmap bmp = BitmapFactory.decodeFile(path, options);
            imgBitmap = bitmap;
//            Toast.makeText(AddNewCustomerActivity.this, path, Toast.LENGTH_SHORT).show();
        }


    }


    private String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        Bitmap resized = Bitmap.createScaledBitmap(myBitmap,(int)(myBitmap.getWidth()*0.5), (int)(myBitmap.getHeight()*0.5), true);
        Bitmap resized = Bitmap.createScaledBitmap(myBitmap, 600, 400, true);
        resized.compress(Bitmap.CompressFormat.PNG, 90, bytes);

        File wallpaperDirectory = new File(
                getActivity().getExternalCacheDir() + "/bellose");
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getContext(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved: " + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void requestMultiplePermissions() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {

                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            setLocation();
                            Toast.makeText(getContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();


    }

    private void onSaveCustomer() {
        int id = (int) customerTypeSpinner.getSelectedItemId();

        if (cusName.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Name Field is required", Toast.LENGTH_SHORT).show();
        } else if (addAddressN.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Address Field is required", Toast.LENGTH_SHORT).show();
        } else if (addAddressL.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Address Field is required", Toast.LENGTH_SHORT).show();
        } else if (addAddressL2.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Address Field is required", Toast.LENGTH_SHORT).show();
        } else if (addAddStreet.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Address Field is required", Toast.LENGTH_SHORT).show();
        } else if (contactP.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Telephone Number Field is required", Toast.LENGTH_SHORT).show();
        } else if (addLandLine.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Telephone Field is required", Toast.LENGTH_SHORT).show();
        } else if (addAddStreet.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Address Field is required", Toast.LENGTH_SHORT).show();
        } else if (addDiscount.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Discount Field is required", Toast.LENGTH_SHORT).show();
        }
        else if (Double.parseDouble(addDiscount.getText().toString())  > 100) {
            Toast.makeText(getContext(), "Discount must be lower than 100", Toast.LENGTH_SHORT).show();
        }

        else {
//          Toast.makeText(getContext(), "======>>>>>>>>2", Toast.LENGTH_SHORT).show();

            if(path == null){
                Toast.makeText(getContext(), "Please upload an image", Toast.LENGTH_LONG).show();

                return;
            }


            File file = new File(path);



            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("image/jpeg"),file);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part imagenPerfil = MultipartBody.Part.createFormData("imagenPerfil", file.getName(), requestFile);


            APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
            Call<CustomerCreateResponse> stringCall = getNoticeDataService.createCustomer(
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(customerTypeList.get(id).customer_type_id)),
                    RequestBody.create(MediaType.parse("multipart/form-data"), shared.getSelectedRoot().id),
                    RequestBody.create(MediaType.parse("multipart/form-data"), "113"),
                    RequestBody.create(MediaType.parse("multipart/form-data"), cusName.getText().toString()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), addAddressN.getText().toString()),
                    RequestBody.create(MediaType.parse("multipart/form-data"),  addAddressL.getText().toString()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), addAddressL2.getText().toString()),
                    RequestBody.create(MediaType.parse("multipart/form-data"),  addAddStreet.getText().toString()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), contactP.getText().toString()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), addMobile.getText().toString()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), addLandLine.getText().toString()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), ""),
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(latitude)),
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(longitude)),
                    RequestBody.create(MediaType.parse("multipart/form-data"), "1"),
                    RequestBody.create(MediaType.parse("multipart/form-data"), getDate()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), getDate()),
                    RequestBody.create(MediaType.parse("multipart/form-data"), "0"),
                    RequestBody.create(MediaType.parse("multipart/form-data"), addDiscount.getText().toString()),
                    imagenPerfil,
                    RequestBody.create(MediaType.parse("multipart/form-data"), "1")


            );


            requestMultiplePermissions(new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {
                    LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                    boolean gpsEnable = true;
                    try{
                        gpsEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    if(!gpsEnable){

                        Toast.makeText(getContext(),"Please enable your GPS ervice",Toast.LENGTH_LONG).show();
                        return;
                    }

                    showProgressDialog();
                    stringCall.enqueue(new Callback<CustomerCreateResponse>() {
                        @Override
                        public void onResponse(Call<CustomerCreateResponse> call, Response<CustomerCreateResponse> response) {
                            dismissProgressDialog();
                            if (response.isSuccessful()) {

                                CustomerCreateResponse commonResponse = response.body();
                                if (commonResponse != null) {
                                    if(commonResponse.success){
//                                            System.out.println("======>>>>>>>>>>>?????"+commonResponse.data.areas_id);
//                                        Customer customer = new Customer();
//                                        customer.setId(commonResponse.data.id);
//                                        customer.setName(cusName.getText().toString());
//                                        if (customerTypeList.size() > 0) {
//                                            customer.setCustomerTypeId(customerTypeList.get(id).customer_type_id);
//                                        }
//                                        customer.setAddress1(addAddressL.getText().toString());
//                                        customer.setAddress2(addAddressL2.getText().toString());
//                                        customer.setAddressNo(addAddressN.getText().toString());
//                                        customer.setStreet(addAddStreet.getText().toString());
//                                        customer.setLandline(addLandLine.getText().toString());
//                                        customer.setMobile(contactP.getText().toString());
//                                        customer.setAreaId(Integer.parseInt(shared.getSelectedRoot().id));
//                                        customer.setLatitude(String.valueOf(latitude));
//                                        customer.setLongitude(String.valueOf(longitude));
                                        customerViewModel.insertNewCustomer(commonResponse.data, () -> {
                                            Toast.makeText(getContext(), "New customer added successful", Toast.LENGTH_SHORT).show();

                                            if (dashboardActivity != null) {
                                                dashboardActivity.get().getNavigationView().getMenu().performIdentifierAction(R.id.nav_customer_list, 0);
                                                //
                                            }
                                        });



                                    }else {
                                        Toast.makeText(getContext(), commonResponse.message, Toast.LENGTH_LONG).show();
                                    }
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<CustomerCreateResponse> call, Throwable t) {
                            dismissProgressDialog();
                            Toast.makeText(getContext(), "Network error", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            });






        }


    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getContext(), "Processing...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


    private void requestMultiplePermissions(OnTaskCompleted onTaskCompleted) {
        Dexter.withActivity(getActivity())
                .withPermissions(

                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {

                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            onTaskCompleted.onTaskCompleted();
//                            Toast.makeText(getContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }
                        else{

                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                            Toast.makeText(getContext(), "Please grant your location permission", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();


    }
}