package com.prologics.Bellose.fragments.dashboardFragments.customerList;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.CustomerActivity;
import com.prologics.Bellose.activities.DashboardActivity;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Entity.CustomerWithRelation;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.fragments.dummy.DummyContent.DummyItem;
import com.prologics.Bellose.helper.callbacks.OnCustomerSelectCallback;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.Entity.CustomerArrayResponse;
import com.prologics.Bellose.network.RetrofitInstance;
import com.prologics.Bellose.recyclerViews.CustomerListItemRecyclerViewAdapter;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prologics.Bellose.services.DataSyncService.getCustomers;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class CustomerListItemFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private CustomerViewModel customerViewModel;
    private TextView notificationText;
    private DashboardActivity dashboardActivity;
    private EditText searchTextView;
    public ImageView addNewCustomerButton;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CustomerListItemFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CustomerListItemFragment newInstance(int columnCount) {
        CustomerListItemFragment fragment = new CustomerListItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customerlist, container, false);
        notificationText = view.findViewById(R.id.notificationText);
        customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
         dashboardActivity = (DashboardActivity) getActivity();


         searchTextView = view.findViewById(R.id.searchView);
//        FloatingActionButton fab = view.findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (dashboardActivity != null) {
//                    dashboardActivity.getNavigationView().getMenu().performIdentifierAction(R.id.nav_customer, 0);
////
//                }
////
//            }
//        });

        addNewCustomerButton = view.findViewById(R.id.addNewCustomerButton);


        addNewCustomerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               dashboardActivity.getNavigationView().getMenu().performIdentifierAction(R.id.nav_customer, 0);
//

//
            }
        });


        // Set the adapter
//        if (view instanceof RecyclerView) {
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        OnCustomerSelectCallback onCustomerSelectCallback = new OnCustomerSelectCallback() {
            @Override
            public void onClick(Customer customer) {

                requestMultiplePermissions(new OnTaskCompleted() {
                    @Override
                    public void onTaskCompleted() {

                        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                        boolean gpsEnable = true;
                        try{
                            gpsEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if(!gpsEnable){
                            Toast.makeText(getContext(),"Please enable your GPS ervice",Toast.LENGTH_LONG).show();
                            return;
                        }

                        Shared shared = Shared.getInstance(getContext());
                        shared.setSelectedCustomerId(customer.getId());

                        // sending data to new activity
                        Intent intent = new Intent(getActivity(), CustomerActivity.class);

                        intent.putExtra("CUSTOMER_KEY",customer.getId());

                        getActivity().startActivity(intent);
                    }
                });

            }
        };


        CustomerListItemRecyclerViewAdapter customerListItemRecyclerViewAdapter = new CustomerListItemRecyclerViewAdapter(getContext(), new ArrayList<CustomerWithRelation>(),onCustomerSelectCallback);
        recyclerView.setAdapter(customerListItemRecyclerViewAdapter);

        customerViewModel.getAll().observe(getViewLifecycleOwner(), item -> {


            if (item.size() == 0) {
                notificationText.setVisibility(View.VISIBLE);
            } else {
                notificationText.setVisibility(View.GONE);
            }
//
//            System.out.println("========>>>>>>"+item.get(0).name);
            customerListItemRecyclerViewAdapter.setmValues(item);

            recyclerView.getAdapter().notifyDataSetChanged();


//            recyclerView.setAdapter(new CustomerListItemRecyclerViewAdapter(getContext(), item));

        });


        searchTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                customerListItemRecyclerViewAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });



         mSwipeRefreshLayout =  view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadCustomers();

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);





        requestMultiplePermissions(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {

            }
        });
        return view;
    }


    private void loadCustomers(){
        Shared shared = Shared.getInstance(getContext());
            APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
            Call<CustomerArrayResponse> stringCall = getNoticeDataService.getCustomers(Integer.parseInt(shared.getSelectedRoot().id));
            stringCall.enqueue(new Callback<CustomerArrayResponse>() {
                @Override
                public void onResponse(Call<CustomerArrayResponse> call, Response<CustomerArrayResponse> response) {

                    if (response.isSuccessful()) {
                        if (response.body().success) {
                            if (customerViewModel != null) {
                                customerViewModel.insertAll(response.body().data, new OnTaskCompleted() {
                                    @Override
                                    public void onTaskCompleted() {
                                        mSwipeRefreshLayout.setRefreshing(false);
                                    }
                                });
                            }
                            else{
                                mSwipeRefreshLayout.setRefreshing(false);
                            }

                        }
                        else{
                            mSwipeRefreshLayout.setRefreshing(false);
                        }

                    }
                    else{
                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                }

                @Override
                public void onFailure(Call<CustomerArrayResponse> call, Throwable t) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
    }


    private void requestMultiplePermissions(OnTaskCompleted onTaskCompleted) {
        Dexter.withActivity(getActivity())
                .withPermissions(

                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {

                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                                onTaskCompleted.onTaskCompleted();
//                            Toast.makeText(getContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                            Toast.makeText(getContext(), "Please grant your location permission", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }


    @Override
    public void onStop() {


        super.onStop();
    }
}
