package com.prologics.Bellose.fragments.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.RootViewActivity;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.services.ConnectivityViewModel;

public class SessionOffDialog extends DialogFragment {

    private ConnectivityViewModel connectivityViewModel;
    private View.OnClickListener cancelListner;
    private  ImageView close;

    public static SessionOffDialog newInstance(int title) {
        SessionOffDialog frag = new SessionOffDialog();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_session_dialog, container, false);
        connectivityViewModel = ConnectivityViewModel.getInstance(getContext());
        close = v.findViewById(R.id.closeButton);


        Button executeButton = v.findViewById(R.id.execute);
        executeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(connectivityViewModel.onCheckConnectivity(getContext())){
                    Shared shared = Shared.getInstance(getContext());
                    shared.setRootStatus(false);
                    startActivity(new Intent(getActivity(), RootViewActivity.class));
                    getActivity().finish();
                }
                else{
                    Toast.makeText(getContext(), "Can't find internet connectivity", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //anyText.setText(String.valueOf(anything));
        close.setOnClickListener(cancelListner);
//        execute.setOnClickListener(acceptListner);
    }

    public void setCancelListner(View.OnClickListener cancelListner){
        this.cancelListner = cancelListner;
    }
}
