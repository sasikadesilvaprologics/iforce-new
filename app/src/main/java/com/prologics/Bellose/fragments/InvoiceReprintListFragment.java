package com.prologics.Bellose.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.InvoiceReprintActivity;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.fragments.dummy.DummyContent.DummyItem;
import com.prologics.Bellose.recyclerViews.InvoiceReprintListRecyclerViewAdapter;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_NUMBER_KEY;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class InvoiceReprintListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private InvoiceItemViewModel invoiceItemViewModel;
    private WeakReference<InvoiceReprintActivity> invoiceReprintActivityWeakReference;
    private int id;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public InvoiceReprintListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static InvoiceReprintListFragment newInstance(int columnCount) {
        InvoiceReprintListFragment fragment = new InvoiceReprintListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invoicereprintlist_list, container, false);

         id  = Objects.requireNonNull(getActivity()).getIntent().getIntExtra(INVOICE_NUMBER_KEY,1);
        invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        invoiceReprintActivityWeakReference = new WeakReference<>((InvoiceReprintActivity)getActivity());

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            invoiceItemViewModel.getItemsByInvoiceId(id).observe(getViewLifecycleOwner(), new Observer<List<InvoiceItem>>() {
                @Override
                public void onChanged(List<InvoiceItem> invoiceItems) {

                    invoiceReprintActivityWeakReference.get().invoiceItemList = invoiceItems;
                    recyclerView.setAdapter(new InvoiceReprintListRecyclerViewAdapter(invoiceItems, mListener));
                    invoiceReprintActivityWeakReference.get().negativeCount = 0;
                    System.out.println("=====>>>>>>"+invoiceItems.size());
                    for (InvoiceItem invoiceItem : invoiceItems) {
                        System.out.println("=====>>>>>>"+invoiceItem.qty+"====>>"+invoiceItem.invoice_item_qty_s);
                        if (invoiceItem.invoice_item_qty_s > invoiceItem.qty) {
                            invoiceReprintActivityWeakReference.get().negativeCount++;
                        }
                    }
                }
            });

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(invoiceItemViewModel != null && invoiceItemViewModel.getItemsByInvoiceId(id).hasObservers()){
            System.out.println("===============.>>>>>>>>>>>>>remove reprint observe");
            invoiceItemViewModel.getItemsByInvoiceId(id).removeObservers(this);
        }
    }
}
