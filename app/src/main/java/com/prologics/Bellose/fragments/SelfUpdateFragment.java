package com.prologics.Bellose.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.CustomerActivity;
import com.prologics.Bellose.databaseModule.Entity.Self;
import com.prologics.Bellose.databaseModule.Entity.SelfUpdate;
import com.prologics.Bellose.databaseModule.Entity.SelfWithItem;
import com.prologics.Bellose.databaseModule.EntityViewModel.SelfUpdateViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.SelfViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnSelfItemChangeListner;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.RetrofitInstance;
import com.prologics.Bellose.recyclerViews.SelfRecyclerViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SelfUpdateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SelfUpdateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelfUpdateFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;

    private OnFragmentInteractionListener mListener;
    private  SelfUpdateViewModel selfUpdateViewModel;
    private Shared shared;
    private SelfViewModel selfViewModel;
    private ProgressDialog mProgressDialog;
    private List<SelfWithItem> selfWithItems = new ArrayList<>();
    private CustomerActivity customerActivity;


    public SelfUpdateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SelfUpdateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SelfUpdateFragment newInstance(String param1, String param2) {
        SelfUpdateFragment fragment = new SelfUpdateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        View view = inflater.inflate(R.layout.fragment_self_update, container, false);

        selfViewModel = new ViewModelProvider(this).get(SelfViewModel.class);
        selfUpdateViewModel = new ViewModelProvider(this).get(SelfUpdateViewModel.class);
        shared = Shared.getInstance(getContext());

        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.shelf_recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        customerActivity = (CustomerActivity) getContext();

        OnSelfItemChangeListner onSelfItemChangeListner = self -> {
//                if(self.size() > 0){
//                    selves.add(self.get(0));
//                }

        };

        SelfRecyclerViewAdapter selfRecyclerViewAdapter = new SelfRecyclerViewAdapter(getContext(), new ArrayList<SelfWithItem>(), onSelfItemChangeListner);
        recyclerView.setAdapter(selfRecyclerViewAdapter);

        selfViewModel.getCustomerSelf(shared.getSelectedCustomerId()).observe(getViewLifecycleOwner(), new Observer<List<SelfWithItem>>() {
            @Override
            public void onChanged(List<SelfWithItem> selves) {
                selfWithItems = selves;
                selfRecyclerViewAdapter.setmValues(selves);
                recyclerView.getAdapter().notifyDataSetChanged();
//                recyclerView.setAdapter(new SelfRecyclerViewAdapter(getContext(), selves, onSelfItemChangeListner));
            }
        });



//
        Button update = view.findViewById(R.id.updateShelf);
        update.setOnClickListener(v -> {


            if (selfRecyclerViewAdapter.getmValues() != null) {
                JSONArray itemListArray = new JSONArray();
                List<Self> selves = new ArrayList<>();
                for (SelfWithItem selfWithItem : selfRecyclerViewAdapter.getmValues()) {
                    System.out.println("=========" +selfWithItem.self_id);
                    System.out.println("=========" +selfWithItem.itemsid);
                    System.out.println("=========" +selfWithItem.ownQty);
                    System.out.println("=========------------------------------------------------");
                    Self self = new Self();
                    self.self_id = selfWithItem.self_id;
                    self.ownQty = selfWithItem.ownQty;
                    self.other_qty = selfWithItem.other_qty;
                    self.customerId = selfWithItem.customerId;
                    self.itemsid = selfWithItem.itemsid;
                    selves.add(self);

                    if(selfWithItem.newOtherQty > 0 || selfWithItem.newQty > 0){
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("items_id",selfWithItem.itemsid);
                            jsonObject.put("qty",selfWithItem.ownQty);
                            jsonObject.put("other_qty",selfWithItem.other_qty);
                            itemListArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if(itemListArray.length() > 0){
                    updateSelf(itemListArray.toString(), new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted() {

                        }
                    });
                }

                customerActivity.tabs.getTabAt(3).select();
                Toast.makeText(getContext(), "Updated", Toast.LENGTH_SHORT).show();

                selfViewModel.update(selves, this::addSelfUpdateStatus);


            }

        });


        return view;
    }


    private void updateSelf(String items, OnTaskCompleted onTaskCompleted){
        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.updateSelf(shared.getSelectedCustomerId(),items);
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                System.out.println("==============response===="+response.body());
                if(response.isSuccessful()){
                    onTaskCompleted.onTaskCompleted();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void addSelfUpdateStatus(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        Date date = new Date();
        SelfUpdate selfUpdate = new SelfUpdate();
        selfUpdate.customer_id = shared.getSelectedCustomerId();
        selfUpdate.updated = dateFormat.format(date);
        selfUpdateViewModel.insertAll(Collections.singletonList(selfUpdate), () -> {

        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }

//        if(selfWithItems.size() > 30){
//            showProgressDialog();
//        }
//
//        Runnable runnable = new Runnable() {
//            public void run() {
//                dismissProgressDialog();
//            }
//        };
//
//        Handler handler = new android.os.Handler();
//        handler.postDelayed(runnable, 1000);
//
//        handler.removeCallbacks(runnable);
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(selfViewModel != null && selfViewModel.getCustomerSelf(shared.getSelectedCustomerId()).hasObservers()){
            System.out.println("===============.>>>>>>>>>>>>>remove self observe");
            selfViewModel.getCustomerSelf(shared.getSelectedCustomerId()).removeObservers(this);
        }
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getContext(), "Processing...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


}
