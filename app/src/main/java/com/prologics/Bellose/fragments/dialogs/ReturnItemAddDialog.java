package com.prologics.Bellose.fragments.dialogs;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.InputFilterMinMax;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.services.ConnectivityViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_DRAFT_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_PRINTED_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_SAVED_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.RETURN_ITEM_CODE;

public class ReturnItemAddDialog extends DialogFragment {

    private ConnectivityViewModel connectivityViewModel;
    private View.OnClickListener cancelListner;
    private ImageView close;
    private Item item = new Item();
    private EditText batchPrice;
    private EditText discount;
    private Button addButton;
    private ImageView addCount;
    private ImageView removeCount;
    private EditText countText;
    private Shared shared;
    private InvoiceViewModel invoiceViewModel;
    private InvoiceItemViewModel invoiceItemViewModel;
    private Invoice invoice;
    private RadioButton percentageRadioButton;
    private RadioButton valueRadioButton;
    private Boolean isEnablePercentage = true;

    private RadioButton sellableRadioButton;
    private RadioButton nonSellableRadioButton;
    private Boolean isSellableOne = true;


    public void setInvoiceItem(Item item) {
        this.item = item;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public ReturnItemAddDialog(Item item) {


        setInvoiceItem(item);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.return_add_item_popup, container, false);

        shared = Shared.getInstance(getContext());
        invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);


        System.out.println("========min====" + item.minPrice + "==========max======" + item.maxPrice);

        batchPrice = v.findViewById(R.id.batchPrice);
        discount = v.findViewById(R.id.discount);
        addButton = v.findViewById(R.id.addButton);
        addCount = v.findViewById(R.id.addCountButton);
        removeCount = v.findViewById(R.id.removeCountButton);

        countText = v.findViewById(R.id.countText);
        close = v.findViewById(R.id.closeButton);
        percentageRadioButton = v.findViewById(R.id.percentageRadioButton);
        valueRadioButton = v.findViewById(R.id.valueRadioButton);
        sellableRadioButton = v.findViewById(R.id.sQtyRadioButton);
        nonSellableRadioButton = v.findViewById(R.id.nonSQtyRadioButton);
        batchPrice.setText(String.valueOf(item.sellingPrice));


//        batchPrice.setFilters(new InputFilter[]{
//                new InputFilterMinMax(0, item.maxPrice+1),
//        });

        discount.setFilters(new InputFilter[]{
                new InputFilterMinMax(0, 100),
        });


        percentageRadioButton.toggle();
        sellableRadioButton.toggle();
        percentageRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isEnablePercentage = isChecked;
                discount.setText("");
                if (isChecked) {
                    discount.setFilters(new InputFilter[]{
                            new InputFilterMinMax(0, 100),
                    });
                } else {
                    discount.setFilters(new InputFilter[]{
                            new InputFilterMinMax(0, item.maxPrice),
                    });
                }
            }
        });

        sellableRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isSellableOne = isChecked;
            }
        });




        addCount.setOnClickListener(new ReturnItemAddDialog.OnAddCountClickListner());
        removeCount.setOnClickListener(new ReturnItemAddDialog.OnRemoveountClickListner());
        addButton.setOnClickListener(new ReturnItemAddDialog.OnAddItemListner());



        invoiceViewModel.getInvoiceById(shared.getSelectedInv()).observe(getViewLifecycleOwner(), new Observer<Invoice>() {
            @Override
            public void onChanged(Invoice invoice) {
                setInvoice(invoice);
            }
        });


        return v;
    }

    private class OnAddCountClickListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int currentValue = Integer.parseInt(countText.getText().toString());

                countText.setText(String.valueOf(currentValue += 1));



        }
    }

    private class OnRemoveountClickListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int currentValue = Integer.parseInt(countText.getText().toString());
            if (currentValue - 1 >= 1) {
                countText.setText(String.valueOf(currentValue -= 1));
            }

        }
    }


    private class OnAddItemListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
//            double price = Double.parseDouble(batchPrice.getText().toString());
//            if(price < item.maxPrice || price > item.maxPrice){
//                Toast.makeText(getContext(), "Price must be greater than "+item.minPrice+" and less than "+item.maxPrice, Toast.LENGTH_SHORT).show();
//                return;
//            }


            if(countText.getText().toString().length() > 4){
                Toast.makeText(getContext(), "Max quantity is 9999", Toast.LENGTH_SHORT).show();
                return;
            }

            addInvoiceItem(RETURN_ITEM_CODE);

        }
    }



    private void addInvoiceItem(int type) {
        if(invoice != null){
            if(invoice.flag == INVOICE_SAVED_FLAG || invoice.flag == INVOICE_PRINTED_FLAG  ){
                return;
            }
        }
        addInvoiceDraft(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {
                insertInvoiceItem(type);

            }
        });
    }

    private void insertInvoiceItem(int type) {
        if(countText.getText().toString().isEmpty()){
            Toast.makeText(getContext(), "Please add item quantity", Toast.LENGTH_SHORT).show();
            return;
        }


        double price = Double.parseDouble(batchPrice.getText().toString());
//        if(price < item.minPrice){
//            return;
//        }
        if (batchPrice.getText().toString().isEmpty()) {
            price = item.sellingPrice;
        }


        double qtySellerble = Double.parseDouble(countText.getText().toString());
        InvoiceItem invoiceItem = invoiceItemViewModel.checkInvoiceItem(
                shared.getSelectedInv(),
                item.item_id,
                price,
                getDiscountValue(price),
                type
        );

        if (invoiceItem == null || !isSellableOne) {
            invoiceItem = new InvoiceItem();
        }


        System.out.println("=====<<KKKLLLLLLLLLLL>>?????" + invoiceItem.invoice_item_qty_s);
        qtySellerble = qtySellerble + invoiceItem.invoice_item_qty_s;
//        invoiceItem.customerId = shared.getSelectedCustomerId();

        invoiceItem.deviceId = Integer.parseInt(shared.getCurrentUser().deviceId);
        invoiceItem.invoiceId = shared.getSelectedInv();
        invoiceItem.items_id = item.item_id;
        invoiceItem.invoiceItemTotalDiscount = getDiscountValue(price)* qtySellerble;
        invoiceItem.invoiceItemTotalAmount = (price - getDiscountValue(price)) * qtySellerble;
        invoiceItem.invoiceItemmrp = price;
        if(isSellableOne){
            invoiceItem.invoice_item_qty_s = qtySellerble;
        }
        else{
            invoiceItem.invoice_item_qty_ns = qtySellerble;
        }


        invoiceItem.invoice_item_flag = type;

        invoiceItemViewModel.insertAll(Collections.singletonList(invoiceItem), new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {
                onDismiss();
            }
        });
    }


    private double getDiscountValue(double price) {
        if (discount.getText().toString().isEmpty()) {

            return 0.0;
        }
        double discountValue = Double.parseDouble(discount.getText().toString());
        if (isEnablePercentage) {
            return (price / 100) * discountValue;
        } else {
            return discountValue;
        }
    }

    private void addInvoiceDraft(OnTaskCompleted onTaskCompleted) {
        if (invoice == null) {
            Invoice invoice = new Invoice();
            invoice.invoice_code = shared.getSelectedInv();
//            invoice.customerId = shared.getSelectedCustomerId();
            invoice.deviceId = Integer.parseInt(shared.getCurrentUser().deviceId);
            invoice.isSynced = false;
            invoice.flag = INVOICE_DRAFT_FLAG;

            List<Invoice> invoiceList = new ArrayList<>();
            invoiceList.add(invoice);

            invoiceViewModel.insert(invoiceList, new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {
                    onTaskCompleted.onTaskCompleted();
                }
            });
        } else {
            onTaskCompleted.onTaskCompleted();
        }

    }

    private void onDismiss() {
        this.dismiss();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //anyText.setText(String.valueOf(anything));
        close.setOnClickListener(cancelListner);
//        execute.setOnClickListener(acceptListner);
    }

    public void setCancelListner(View.OnClickListener cancelListner) {
        this.cancelListner = cancelListner;
    }


}
