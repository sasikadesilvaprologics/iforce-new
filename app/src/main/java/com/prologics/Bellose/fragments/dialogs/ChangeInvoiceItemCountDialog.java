package com.prologics.Bellose.fragments.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;

import java.util.Collections;

import static com.prologics.Bellose.databaseModule.GlobalValues.ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.RETURN_ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.getDate;

public class ChangeInvoiceItemCountDialog extends DialogFragment {

    private InvoiceItem invoiceItem;
    private ImageView addCount;
    private ImageView removeCount;
    private EditText countText;
    private boolean isPreSale = false;

    public ChangeInvoiceItemCountDialog(InvoiceItem invoiceItem, boolean isPreSale) {
        this.invoiceItem = invoiceItem;
        this.isPreSale = isPreSale;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.item_count_popup, container, false);

        ItemViewModel itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);
        InvoiceItemViewModel invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        addCount = v.findViewById(R.id.addCountButton);
        removeCount = v.findViewById(R.id.removeCountButton);
        countText = v.findViewById(R.id.countText);
        ImageView close = v.findViewById(R.id.closeButton);
        Button addButton = v.findViewById(R.id.addButton);
        Item item = itemViewModel.getItem(invoiceItem.items_id);
        countText.setText(String.valueOf(Math.round(invoiceItem.invoice_item_qty_s)));


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();
            }
        });

        addCount.setOnClickListener(new OnAddCountClickListner());
        removeCount.setOnClickListener(new OnRemoveountClickListner());

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(countText.getText().toString().isEmpty()){

                    return;
                }

                int newQty = Integer.parseInt(countText.getText().toString());
                if(countText.getText().toString().length() > 4){
                    Toast.makeText(getContext(), "Max quantity is 9999", Toast.LENGTH_SHORT).show();
                    return;
                }
                System.out.println("===========>>"+newQty+"=======>>>"+(invoiceItem.invoice_item_qty_s+item.qty));

                if (newQty > (invoiceItem.invoice_item_qty_s+item.qty) && !isPreSale) {
                    double availability = item.qty+invoiceItem.invoice_item_qty_s;
                    Toast.makeText(getContext(), "Max quantity reach , currently "+availability+" items available", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (item != null) {
                    item.qty = item.qty - (newQty-invoiceItem.invoice_item_qty_s);
                    item.updatedAt = getDate();
                    double itemDiscount = invoiceItem.invoiceItemTotalDiscount / invoiceItem.invoice_item_qty_s;
                    if(invoiceItem.invoice_item_flag == ITEM_CODE || invoiceItem.invoice_item_flag == RETURN_ITEM_CODE){
                        invoiceItem.invoiceItemTotalAmount = (invoiceItem.invoiceItemmrp-itemDiscount) * newQty;
                        invoiceItem.invoiceItemTotalDiscount = itemDiscount * newQty;
                    }

                    if(!isPreSale){
                        itemViewModel.insertItems(Collections.singletonList(item), new OnTaskCompleted() {
                            @Override
                            public void onTaskCompleted() {

                                invoiceItem.invoice_item_qty_s = newQty;
                                invoiceItemViewModel.insertAll(Collections.singletonList(invoiceItem), new OnTaskCompleted() {
                                    @Override
                                    public void onTaskCompleted() {
                                        closeDialog();
                                    }
                                });
                            }
                        });
                    }
                    else{
                        invoiceItem.invoice_item_qty_s = newQty;
                        invoiceItemViewModel.insertAll(Collections.singletonList(invoiceItem), new OnTaskCompleted() {
                            @Override
                            public void onTaskCompleted() {
                                closeDialog();
                            }
                        });
                    }

                }


            }
        });

        return v;
    }

    private class OnAddCountClickListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(countText.getText().toString().isEmpty()){
                countText.setText(String.valueOf(1));
                return;
            }
            int currentValue = Integer.parseInt(countText.getText().toString());
            countText.setText(String.valueOf(currentValue += 1));





        }
    }

    private class OnRemoveountClickListner implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(countText.getText().toString().isEmpty()){
                countText.setText(String.valueOf(1));
                return;
            }
            int currentValue = Integer.parseInt(countText.getText().toString());
            if (currentValue - 1 >= 1) {
                countText.setText(String.valueOf(currentValue -= 1));
            }
            else{
                countText.setText(String.valueOf(1));
            }

        }
    }

    private void closeDialog() {
        this.dismiss();
    }
}
