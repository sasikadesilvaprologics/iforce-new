package com.prologics.Bellose.fragments.dashboardFragments.Settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.DashboardActivity;
import com.prologics.Bellose.activities.LoginActivity;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.SelfViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnApiResponse;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.RetrofitInstance;
import com.prologics.Bellose.printModule.InvoiceLargePrint;
import com.prologics.Bellose.printModule.InvoiceSmallPrint;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prologics.Bellose.databaseModule.GlobalValues.IS_SMALL_PRINT;
import static com.prologics.Bellose.printModule.PrintActivity.changePrinter;
import static com.prologics.Bellose.services.DataSyncService.runStockDataListApiService;


public class SettingFragment extends Fragment {

    private SettingViewModel settingViewModel;
    private ProgressDialog mProgressDialog;
    private TextView printerName;
    private Shared shared;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        settingViewModel =
                new ViewModelProvider(this).get(SettingViewModel.class);
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        ItemViewModel itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);
        InvoiceViewModel invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        InvoiceItemViewModel invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        SelfViewModel selfViewModel = new ViewModelProvider(this).get(SelfViewModel.class);
        CustomerViewModel customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        shared = Shared.getInstance(getContext());
        LinearLayout updateStock = root.findViewById(R.id.stockSyncButton);
        printerName = root.findViewById(R.id.printerName);
        LinearLayout saveSettings = root.findViewById(R.id.saveSettings);
        printerName.setText(shared.getPrinterName());

        LinearLayout checkPrinterCon = root.findViewById(R.id.testBluetooth);
        DashboardActivity dashboardActivity = (DashboardActivity) getContext();
        Context fragmentContext = getContext();
        checkPrinterCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IS_SMALL_PRINT) {
                    InvoiceSmallPrint invoiceSmallPrint = InvoiceSmallPrint.getInstance(dashboardActivity, fragmentContext);
                    try {

                        if (invoiceSmallPrint.isPrinterConnected()) {
                            Toast.makeText(getContext(), "Connected", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), "Disconnected", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Disconnected", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    InvoiceLargePrint.getInstance(dashboardActivity, fragmentContext);
                }
            }
        });


        updateStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shared.isHaveUnSyncInVoices()) {
                    return;
                }
                showProgressDialog();
                runStockDataListApiService(itemViewModel, Integer.valueOf(shared.getCurrentUser().deviceId), new OnApiResponse() {
                    @Override
                    public void onSuccess(String message) {
                        dismissProgressDialog();
                    }

                    @Override
                    public void onError(String message) {
                        dismissProgressDialog();
                    }
                });
            }
        });

        saveSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shared.setPrinterName(printerName.getText().toString());
            }
        });


        printerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangePrinter();
            }
        });


        LinearLayout cleanDatabase = root.findViewById(R.id.cleanDatabase);
        cleanDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shared.isHaveUnSyncInVoices()) {
                    Toast.makeText(getContext(), "Please sync pending invoices.", Toast.LENGTH_LONG).show();
                    return;
                }

                showProgressDialog();
                new databaseDeleteAsyncTask(
                        customerViewModel,
                        itemViewModel,
                        invoiceViewModel,
                        invoiceItemViewModel,
                        selfViewModel,
                        new OnTaskCompleted() {
                            @Override
                            public void onTaskCompleted() {

                                getNewStock(itemViewModel);
                                getLastInvId();

                                shared.setLoginStatus(false);
                                shared.setRootStatus(false);

                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                dismissProgressDialog();
                                getActivity().finish();
                                startActivity(intent);
                            }
                        }
                ).execute(true);

            }
        });


        LinearLayout testServer = root.findViewById(R.id.testServerButton);
        testServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
                Call<String> stringCall = getNoticeDataService.testServerConnection();

                stringCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject obj = new JSONObject(response.body());
                                if (obj.getBoolean("success")) {
                                    Toast.makeText(getContext(), "Server is Working ...", Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });

            }
        });


//


        return root;
    }


    private void getLastInvId() {
        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.getLastInvoiceId(Integer.parseInt(shared.getCurrentUser().deviceId));
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    System.out.println("===============" + response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getBoolean("success")) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            shared.setCurrentInv(dataObj.getInt("inv_code"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("===============" + t.getMessage());
            }
        });
    }

    private void onChangePrinter() {
        DashboardActivity dashboardActivity = (DashboardActivity) getContext();
        Context fragmentContext = getContext();
        changePrinter(getContext(), new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {
                if (IS_SMALL_PRINT) {
                    InvoiceSmallPrint.getNewInstance(dashboardActivity, fragmentContext);
                } else {
                    InvoiceLargePrint.getNewInstance(dashboardActivity, fragmentContext);
                }

                printerName.setText(shared.getPrinterName());
            }
        });


    }


    private void getNewStock(ItemViewModel itemViewModel) {
        runStockDataListApiService(
                itemViewModel,
                Integer.parseInt(shared.getCurrentUser().deviceId), new OnApiResponse() {
                    @Override
                    public void onSuccess(String message) {
                        dismissProgressDialog();
                    }

                    @Override
                    public void onError(String message) {
                        dismissProgressDialog();
                    }
                });
    }


    private static class databaseDeleteAsyncTask extends AsyncTask<Boolean, Void, Void> {

        ItemViewModel itemViewModel;
        InvoiceViewModel invoiceViewModel;
        InvoiceItemViewModel invoiceItemViewModel;
        SelfViewModel selfViewModel;
        CustomerViewModel customerViewModel;
        OnTaskCompleted onTaskCompleted;


        public databaseDeleteAsyncTask(CustomerViewModel customerViewModel,
                                       ItemViewModel itemViewModel,
                                       InvoiceViewModel invoiceViewModel,
                                       InvoiceItemViewModel invoiceItemViewModel,
                                       SelfViewModel selfViewModel, OnTaskCompleted onTaskCompleted) {

            this.invoiceViewModel = invoiceViewModel;
            this.invoiceItemViewModel = invoiceItemViewModel;
            this.itemViewModel = itemViewModel;
            this.selfViewModel = selfViewModel;
            this.onTaskCompleted = onTaskCompleted;
            this.customerViewModel = customerViewModel;

        }


        @Override
        protected Void doInBackground(Boolean... booleans) {
            customerViewModel.deleteAll();
            invoiceViewModel.deleteAll();
            invoiceItemViewModel.deleteAll();
            selfViewModel.deleteAll();
            itemViewModel.deleteAll();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            onTaskCompleted.onTaskCompleted();
        }
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getContext(), "Processing...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }
}