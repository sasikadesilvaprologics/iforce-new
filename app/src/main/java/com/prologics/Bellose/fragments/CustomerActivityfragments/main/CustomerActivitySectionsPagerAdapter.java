package com.prologics.Bellose.fragments.CustomerActivityfragments.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.prologics.Bellose.R;
import com.prologics.Bellose.fragments.CustomerStatusFragment;
import com.prologics.Bellose.fragments.InvoiceFragment;
import com.prologics.Bellose.fragments.MapFragment;
import com.prologics.Bellose.fragments.SelfUpdateFragment;
import com.prologics.Bellose.fragments.customerHistory.CustomerHistory;


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class CustomerActivitySectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{
            R.string.dashboard,
            R.string.status_update,
            R.string.self_update,
            R.string.invoices,
            R.string.preSale,
            R.string.map,};
    private final Context mContext;


    public CustomerActivitySectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if(position == 0){
            return CustomerHistory.newInstance();
        }
        if(position == 1){
            return CustomerStatusFragment.newInstance("","");
        }
        else if(position == 2){
            return SelfUpdateFragment.newInstance("","");
        }
        else if(position == 3){
            return new  InvoiceFragment(false);

        }
        else if(position == 4){
            return new InvoiceFragment(true);

        }
//        else if(position == 4){
//            return new PaymentFragment();
//        }
        else{
            return   MapFragment.newInstance("","");
        }

    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 6;
    }
}