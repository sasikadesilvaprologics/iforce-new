package com.prologics.Bellose.fragments.customerHistory;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.RetrofitInstance;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;


public class CustomerHistory extends Fragment {

    private Shared shared;
    private LineChart chart;
    private TextView totalOutstandingText;
    private TextView dueDatesText;
    private TextView totalSalesText;
    private TextView totalReturnsText;
    private TextView totalInvoiceText;
    private TextView customerSinceText;
    private TextView lastInvoiceText;
    private TextView chequeReturnText;
    private TextView blurViewText;
    private  BlurView blurView;

    public static CustomerHistory newInstance() {
        return new CustomerHistory();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.customer_history_fragment, container, false);

        chart = view.findViewById(R.id.barchart);
        totalOutstandingText = view.findViewById(R.id.totalOutstandingText);
        dueDatesText = view.findViewById(R.id.dueDatesTextt);
        totalSalesText = view.findViewById(R.id.totalSalesText);
        totalReturnsText = view.findViewById(R.id.totalReturnsText);
        totalInvoiceText = view.findViewById(R.id.totalInvoiceText);
        customerSinceText = view.findViewById(R.id.customerSinceText);
        lastInvoiceText = view.findViewById(R.id.lastInvoiceText);
        chequeReturnText = view.findViewById(R.id.chequeReturnText);
        blurViewText = view.findViewById(R.id.blurViewText);



        float radius = 10f;

        View decorView = getActivity().getWindow().getDecorView();
        //ViewGroup you want to start blur from. Choose root as close to BlurView in hierarchy as possible.
        ViewGroup rootView = (ViewGroup) decorView.findViewById(android.R.id.content);
        //Set drawable to draw in the beginning of each blurred frame (Optional).
        //Can be used in case your layout has a lot of transparent space and your content
        //gets kinda lost after after blur is applied.
        Drawable windowBackground = decorView.getBackground();
        blurView = view.findViewById(R.id.blurView);
        blurView.setupWith(rootView)
                .setFrameClearDrawable(windowBackground)
                .setBlurAlgorithm(new RenderScriptBlur(getContext()))
                .setBlurRadius(radius)
                .setHasFixedTransformationMatrix(true);

//



        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       shared = Shared.getInstance(getContext());






        // TODO: Use the ViewModel
    }

    @Override
    public void onStart() {
        super.onStart();
        getCustomerHistory();
    }

    private void getCustomerHistory(){
        blurViewText.setText("Loading...");
        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.getCustomerHistory(shared.getSelectedCustomerId());

        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                System.out.println("====+++++++)))))(((((((((((------------"+response.body());
                if(response.isSuccessful()){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(jsonObject.getBoolean("success")){

                            JSONObject data = jsonObject.getJSONObject("data");
                            totalOutstandingText.setText(DECIMAL_FORMAT.format(data.getDouble("outstanding")));
                            totalSalesText.setText(DECIMAL_FORMAT.format(data.getDouble("total_sales")));
                            totalReturnsText.setText(DECIMAL_FORMAT.format(data.getDouble("total_returns")));
                            customerSinceText.setText(data.getString("customer_days"));
                            totalInvoiceText.setText(data.getString("total_invoices"));
                            chequeReturnText.setText(data.getString("chq_returns"));
                            dueDatesText.setText(data.getString("due_days"));
                            lastInvoiceText.setText(data.getString("last_invoice_days"));
                            loadChart(data.getJSONObject("sales"));
                            blurView.setVisibility(View.GONE);
                        }
                        else{
                            blurViewText.setText("Sory, Something went wrong");
                            Toast.makeText(getContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException | NullPointerException | IllegalArgumentException e) {
                        blurViewText.setText("Sory, You are in offline");
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                blurViewText.setText("Sory, You are in offline");
                System.out.println("====+++++++)))))(((((((((((------------"+t.getMessage());
            }
        });
    }


    private void loadChart(JSONObject sales){

        chart.setDrawGridBackground(false);

        // no description text
        chart.getDescription().setEnabled(false);

        // enable touch gestures
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.getAxisLeft().setDrawGridLines(true);
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setDrawGridLines(true);
        chart.getXAxis().setDrawAxisLine(true);



        // don't forget to refresh the drawing
        chart.invalidate();


        ArrayList<Entry> values = new ArrayList<>();

        try {
            values.add(new Entry((float) 0.4, (float) sales.getDouble("1")));
            values.add(new Entry((float) 1.6, (float) sales.getDouble("2")));
            values.add(new Entry((float) 2.8, (float) sales.getDouble("3")));
            values.add(new Entry((float) 4.0, (float) sales.getDouble("4")));
            values.add(new Entry((float) 5.2, (float) sales.getDouble("5")));
            values.add(new Entry((float) 6.4, (float) sales.getDouble("6")));
        } catch (JSONException | NullPointerException | IllegalArgumentException e) {
            e.printStackTrace();
        }


//        for (int i = 0; i < 20; i++) {
//                float val = (float) (Math.random() * 10);
//            values.add(new Entry(i * 0.001f, val));
//        }

        // create a dataset and give it a type

        LineDataSet set1 = new LineDataSet(values, "Sales");

        set1.setColor(Color.BLUE);
        set1.setLineWidth((float) 2);
        set1.setDrawValues(false);
        set1.setDrawCircles(false);
        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set1.setDrawFilled(false);

        // create a data object with the data sets
        LineData data = new LineData(set1);


        // set data
        chart.setData(data);
        chart.animateX(2000);
        chart.animateY(2000);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setEnabled(false);
    }

}
