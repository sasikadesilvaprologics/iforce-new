package com.prologics.Bellose.fragments.dashboardFragments.home;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.prologics.Bellose.R;
import com.prologics.Bellose.activities.InvoiceSyncActivity;
import com.prologics.Bellose.databaseModule.Entity.DashboardData;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.fragments.dialogs.SessionOffDialog;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.RetrofitInstance;
import com.prologics.Bellose.services.ConnectivityViewModel;
import com.ramijemli.percentagechartview.PercentageChartView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;


public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private ConnectivityViewModel connectivityViewModel;
    private Shared shared;
    private TextView productiveCalls;
    private TextView monthProductiveCalls;
    private TextView totalsales;
    private TextView monthTotalText;
    private TextView todayReturns;
    private TextView monthTargetText;
    private TextView grossTotal;
    private TextView monthBalanceToTarget;
    private TextView todayTarget;

    private TextView monthTarget;

    private PercentageChartView salesChart;
    private PercentageChartView monthSalesChart;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        InvoiceViewModel invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        shared = Shared.getInstance(getContext());
//        final TextView textView = root.findViewById(R.id.text_home);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        Date date = new Date();

        productiveCalls = root.findViewById(R.id.productiveCalls);
        monthProductiveCalls = root.findViewById(R.id.monthlyProductiveCalls);
        totalsales = root.findViewById(R.id.todaySales);
        monthTotalText = root.findViewById(R.id.monthSales);
        todayReturns = root.findViewById(R.id.todayReturns);
        monthTargetText = root.findViewById(R.id.monthReturns);
        grossTotal = root.findViewById(R.id.grossTotal);
        monthBalanceToTarget = root.findViewById(R.id.monthGrossTotal);
        salesChart = root.findViewById(R.id.salesGraph);
        monthSalesChart = root.findViewById(R.id.salesGraphMonth);
        todayTarget = root.findViewById(R.id.todayTarget);

        monthTarget = root.findViewById(R.id.monthTarget);




        Button syncButton = root.findViewById(R.id.manualSync);
        invoiceViewModel.getProductiveInvoiceCount(dateFormat.format(date)).observe(getViewLifecycleOwner(), new Observer<DashboardData>() {
            @Override
            public void onChanged(DashboardData dashboardData) {

                syncButton.setText(String.format("Manual Sync(%d)", dashboardData.syncCount));

                if (dashboardData.syncCount > 0) {
                    shared.setHaveUnSyncInvoices(true);
                } else {
                    shared.setHaveUnSyncInvoices(false);
                }

                System.out.println("======sync===>>" + dashboardData.syncCount);
                System.out.println("======sync===>>" + dashboardData.todaySales);
                System.out.println("======sync===>>" + dashboardData.todayReturns);
                System.out.println("======sync===>>" + dashboardData.todaySubTotal);
            }
        });


        connectivityViewModel = ConnectivityViewModel.getInstance(getContext());
        connectivityViewModel.onCheckConnectivity(getContext());
//        Button manualSync = root.findViewById(R.id.manualSync);
//        manualSync.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                connectivityViewModel.onCheckConnectivity();
//            }
//        });
        LinearLayout networkStatusIcon = root.findViewById(R.id.networkStatusIcon);
        connectivityViewModel.getNetworkStatus().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    networkStatusIcon.setAlpha((float) 1);
                    if (shared.isHaveUnSyncInVoices()) {
                        Intent intent = new Intent(getActivity(), InvoiceSyncActivity.class);
                        startActivity(intent);
                    }

                } else {
                    networkStatusIcon.setAlpha((float) 0.1);

                }


            }
        });


        Button sessionOff = root.findViewById(R.id.sessionOff);
        sessionOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SessionOffDialog sessionDialog = SessionOffDialog.newInstance(R.string.app_name);
                sessionDialog.setCancelListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sessionDialog.dismiss();
                    }
                });
                sessionDialog.show(getChildFragmentManager(), "sessionOffDialog");

            }
        });


        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), InvoiceSyncActivity.class);
                startActivity(intent);
            }
        });

        TextView versionText = root.findViewById(R.id.versionNumber);

        try {
            PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            versionText.setText(String.format("Version %s", version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        return root;
    }


    @Override
    public void onStart() {
        super.onStart();

        getCustomerDashboard();
    }

    private void getCustomerDashboard() {
        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.getCustomerDashboard(Integer.parseInt(shared.getCurrentUser().deviceId));
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getBoolean("success")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            double salesTotal = data.getDouble("today_sale");
                            double salesReturn = data.getDouble("today_returns");
                            double dayTarget = data.getDouble("day_target");
                            double dayAcheve = data.getDouble("bal_to_target");
                            double monthTotalValue = data.getDouble("month_total");
                            double monthTargetValue = data.getDouble("month_target");
                            double monthBalanceTargetValue = data.getDouble("bal_to_monthtarget");

                            double monthAcheveValue = data.getDouble("bal_to_monthtarget");
                            double salesgrossTotal = data.getDouble("today_sale")-data.getDouble("today_returns");
                            productiveCalls.setText(data.getString("calls"));
                            monthProductiveCalls.setText(data.getString("month_calls"));

                            totalsales.setText( DECIMAL_FORMAT.format(salesTotal));
                            todayReturns.setText(DECIMAL_FORMAT.format(salesReturn));
                            grossTotal.setText(DECIMAL_FORMAT.format(salesgrossTotal));



                            todayTarget.setText(DECIMAL_FORMAT.format(dayAcheve));


                            monthTotalText.setText(DECIMAL_FORMAT.format(monthTotalValue));
                            monthTargetText.setText(DECIMAL_FORMAT.format(monthTargetValue));
                            monthBalanceToTarget.setText(DECIMAL_FORMAT.format(monthBalanceTargetValue));

//
                            monthTotalText.setText(DECIMAL_FORMAT.format(monthTotalValue));
                            monthTarget.setText(DECIMAL_FORMAT.format(monthBalanceTargetValue));


                            if(salesgrossTotal > dayTarget){
                                salesChart.setProgress(100,true);
                            }
                            else if(salesgrossTotal > 0){
                                salesChart.setProgress((float) ((salesgrossTotal/dayTarget)*100),true);
                            }

                            if( monthTotalValue > monthTargetValue ){
                                monthSalesChart.setProgress(100,true);
                            }
                           else if(monthTotalValue > 0){
                                monthSalesChart.setProgress((float) ((monthTotalValue/(monthTargetValue))*100),true);
                            }
                        }
                    } catch (JSONException | NullPointerException | IllegalArgumentException  e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}