package com.prologics.Bellose.fragments.dashboardFragments.rootPlan;

import androidx.lifecycle.ViewModelProviders;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.Entity.RootPlanResponse;
import com.prologics.Bellose.network.RetrofitInstance;
import com.prologics.Bellose.recyclerViews.RootPlanRecyclerAdapter;

import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RootPlanFragment extends Fragment {

    private RootPlanViewModel mViewModel;
    private Shared shared;
    private TextView dateFrom;
    private TextView dateTo;
    private  RecyclerView recyclerView;
    private ProgressDialog mProgressDialog;
    private OnViewHolderListner onViewHolderListner;

    public static RootPlanFragment newInstance() {
        return new RootPlanFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_root_plan, container, false);

        shared = Shared.getInstance(getContext());

         dateFrom = view.findViewById(R.id.dateRangeFrom);
         dateTo = view.findViewById(R.id.dateRangeTo);


        dateFrom.setOnClickListener(new DateOnclick(date -> dateFrom.setText(date)));

        dateTo.setOnClickListener(new DateOnclick(date -> dateTo.setText(date)));

        Button loadDateBtn = view.findViewById(R.id.loadDates);
        loadDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        recyclerView = view.findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        onViewHolderListner = new OnViewHolderListner() {
            @Override
            public void onColorChange(View view, int code) {

                if(code == 1){
                    view.setBackgroundColor(getResources().getColor(R.color.yellowAlpha));
                }
                else if(code == 2){
                    view.setBackgroundColor(getResources().getColor(R.color.redAlpha));
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.white));
                }

            }

            @Override
            public void onClick() {

            }

            @Override
            public void onRemove() {

            }
        };


        return view;
    }


    private void loadData(){
        showProgressDialog();
        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<RootPlanResponse> stringCall = getNoticeDataService.getRootPlanList(
                Integer.valueOf(shared.getCurrentUser().deviceId) ,
                dateFrom.getText().toString(),
                dateTo.getText().toString());

        stringCall.enqueue(new Callback<RootPlanResponse>() {
            @Override
            public void onResponse(Call<RootPlanResponse> call, Response<RootPlanResponse> response) {
                dismissProgressDialog();
                if(response.isSuccessful()){
                    if(response.body().success){
                        System.out.println("======{{{}}}}}"+dateFrom.getText().toString());
                        System.out.println("======{{{}}}}}"+dateTo.getText().toString());
                        System.out.println("======{{{}}}}}=="+Integer.valueOf(shared.getCurrentUser().deviceId)+"====>>>>>>>>"+response.body().data.get(0).is_holiday);
                        recyclerView.setAdapter(RootPlanRecyclerAdapter.getInstance(response.body().data,onViewHolderListner));
                    }
                    else{
                        Toast.makeText(getContext(),response.body().message,Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<RootPlanResponse> call, Throwable t) {
                dismissProgressDialog();
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RootPlanViewModel.class);
        // TODO: Use the ViewModel
    }



    private class DateOnclick implements View.OnClickListener {

        private DatePickerDialog picker;
        private OnDateSelect onDateSelect;

        DateOnclick(OnDateSelect onDateSelect) {
            this.onDateSelect = onDateSelect;
        }


        @Override
        public void onClick(View v) {
            TextView dateText = (TextView) v;
            final Calendar cldr = Calendar.getInstance();
            int day = cldr.get(Calendar.DAY_OF_MONTH);
            int month = cldr.get(Calendar.MONTH);
            int year = cldr.get(Calendar.YEAR);
            // date picker dialog

            picker = new DatePickerDialog(getContext(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//
                            String monthStr = Integer.toString((monthOfYear + 1));
                            if (monthStr.length() == 1) {
                                monthStr = "0" + monthStr;
                            }
                            onDateSelect.onSelect(year + "-" + monthStr + "-" + dayOfMonth);

                            dateText.setText(year + "-" + monthStr + "-" + dayOfMonth);
                        }
                    }, year, month, day);
            picker.getDatePicker().setMaxDate(new Date().getTime());
            picker.show();

        }
    }

    private interface OnDateSelect {
        void onSelect(String date);
    }

    public interface OnViewHolderListner{
        void onColorChange(View view, int code);
        void onClick();
        void onRemove();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(getContext(), "Processing...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

}
