package com.prologics.Bellose.fragments.AddNewInvoiceFragments.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.prologics.Bellose.R;
import com.prologics.Bellose.fragments.NewInvoiceFragment;
import com.prologics.Bellose.fragments.NewInvoiceReturnFragment;


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class NewInvoiceActivitySectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.invoices, R.string.returns};
    private static final int[] TAB_TITLES_PRESALE = new int[]{R.string.preSale};
    private final Context mContext;
    private boolean isPreSale = false;

    public NewInvoiceActivitySectionsPagerAdapter(Context context, FragmentManager fm, boolean isPreSale) {
        super(fm);
        mContext = context;
        this.isPreSale = isPreSale;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if(isPreSale){
            if(position == 0){
                return new NewInvoiceFragment(true);
            }
            else{
                return new NewInvoiceReturnFragment();
            }
        }
        else{
            if(position == 0){
                return new NewInvoiceFragment(false);
            }
            else{
                return new NewInvoiceReturnFragment();
            }
        }


    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
//        if(isPreSale){
//            return mContext.getResources().getString(TAB_TITLES_PRESALE[position]);
//        }
//        else{
            return mContext.getResources().getString(TAB_TITLES[position]);
//        }

    }

    @Override
    public int getCount() {
        // Show 2 total pages.

//        if(isPreSale){
//            return 1;
//        }
//        else{
            return 2;
//        }
    }
}