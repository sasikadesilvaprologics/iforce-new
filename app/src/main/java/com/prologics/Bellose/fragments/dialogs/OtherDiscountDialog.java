package com.prologics.Bellose.fragments.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.prologics.Bellose.R;
import com.prologics.Bellose.helper.callbacks.OnTextValueListner;
import com.prologics.Bellose.services.ConnectivityViewModel;

public class OtherDiscountDialog extends DialogFragment {

    private ConnectivityViewModel connectivityViewModel;
    private View.OnClickListener cancelListner;
    private OnTextValueListner submitValueListner;
    private ImageView close;

    public  OtherDiscountDialog () {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_other_discount_dialog, container, false);
        connectivityViewModel = ConnectivityViewModel.getInstance(getContext());
        close = v.findViewById(R.id.closeButton);


        Button executeButton = v.findViewById(R.id.execute);
        TextView otherDisc = v.findViewById(R.id.discountvalue);
        executeButton.setOnClickListener(v1 -> submitValueListner.onSubmit(otherDisc.getText().toString()));

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //anyText.setText(String.valueOf(anything));
        close.setOnClickListener(cancelListner);
//        execute.setOnClickListener(acceptListner);
    }

    public void setCancelListner(View.OnClickListener cancelListner){
        this.cancelListner = cancelListner;
    }

    public void setSubmitValueListner(OnTextValueListner submitValueListner){
        this.submitValueListner = submitValueListner;
    }


}

