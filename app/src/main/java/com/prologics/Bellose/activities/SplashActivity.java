package com.prologics.Bellose.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Shared;

public class SplashActivity extends AppCompatActivity {

    public Shared shared;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_view);
        Shared shared = Shared.getInstance(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(shared.getRootStatus() && shared.getLoginStatus()){
                    openDashboard();
                }
                else if(shared.getLoginStatus() && !shared.getRootStatus() ){
                    openRootView();
                }
                else{
                    openLoginView();
                }

            }
        }, 2000);
    }

    private void openLoginView(){
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }

    private void openDashboard(){
        startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
        finish();
    }

    private void openRootView(){
        startActivity(new Intent(SplashActivity.this, RootViewActivity.class));
        finish();
    }
}
