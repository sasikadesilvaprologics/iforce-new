package com.prologics.Bellose.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Shared;

public class RootViewActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.root_view);

        Button logoutBtn = findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(v -> logout());

    }


    private void logout(){



        Shared shared = Shared.getInstance(this);

        if(shared.isHaveUnSyncInVoices()){

            Toast.makeText(this, "Please sync your offline invoices", Toast.LENGTH_SHORT).show();
            return;
        }
        shared.setLoginStatus(false);
        shared.setRootStatus(false);

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(intent);

    }






}
