package com.prologics.Bellose.activities.reports;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;

import com.prologics.Bellose.databaseModule.Entity.StockReport;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.recyclerViews.SalesReportRecyclerViewAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import com.prologics.Bellose.R;

import java.util.ArrayList;
import java.util.List;

public class SalesDetailsReportActivity extends AppCompatActivity {

    private String startDate;
    private String endDate;
    private RecyclerView recyclerView;
    private SalesReportRecyclerViewAdapter salesReportRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_details_report);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ItemViewModel itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);

        salesReportRecyclerViewAdapter = new SalesReportRecyclerViewAdapter(getContext(),new ArrayList<StockReport>());

        recyclerView = findViewById(R.id.list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(salesReportRecyclerViewAdapter);

        startDate = getIntent().getStringExtra("startDate");
        endDate = getIntent().getStringExtra("endDate");
        TextView totalTextView = findViewById(R.id.totalText);
        TextView dateRange = findViewById(R.id.dateRange);
        dateRange.setText(String.format("%s to %s", startDate, endDate));

        TextView notification = findViewById(R.id.notification);
        itemViewModel.getStockReport(startDate,endDate).observe(this, new Observer<List<StockReport>>() {
            @Override
            public void onChanged(List<StockReport> stockReports) {
                if(stockReports.size() > 0){
                    notification.setVisibility(View.GONE);
                }
                else {
                    notification.setVisibility(View.VISIBLE);
                }

                salesReportRecyclerViewAdapter.setmValues(stockReports);
                recyclerView.getAdapter().notifyDataSetChanged();

            }
        });

    }


    private Context getContext(){
        return this;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onBackPressed(View v) {
        onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_stock_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                System.out.println(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(salesReportRecyclerViewAdapter != null){
                    salesReportRecyclerViewAdapter.getFilter().filter(newText);
                    salesReportRecyclerViewAdapter.notifyDataSetChanged();
                }

                return false;
            }
        });
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
//        return super.onCreateOptionsMenu(menu);

    }

}
