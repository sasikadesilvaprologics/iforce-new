package com.prologics.Bellose.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.prologics.Bellose.BuildConfig;
import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.CustomerWithRelation;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnCheckAppVersion;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.Entity.CommonResponse;
import com.prologics.Bellose.network.Entity.login.LoginUserApiEntity;
import com.prologics.Bellose.network.RetrofitInstance;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prologics.Bellose.helper.Common.getDeviceId;
import static com.prologics.Bellose.services.DataSyncService.checkVersion;

public class LoginActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private EditText pinNumber;
    private ProgressDialog mProgressDialog;
    private Shared shared;
    private boolean isExpired = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);
        progressBar = findViewById(R.id.progressBar);
        pinNumber = findViewById(R.id.pinNumberText);
        shared = Shared.getInstance(this);
        showProgressDialog();
        checkVersion(new OnCheckAppVersion() {
            @Override
            public void onAppUpdate() {
                dismissProgressDialog();
                Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://play.google.com/store/apps/details?id="+BuildConfig.APPLICATION_ID));
                startActivity(i);
            }

            @Override
            public void onExpired() {
                dismissProgressDialog();
                isExpired = true;
            }

            @Override
            public void onSuccess() {
                dismissProgressDialog();
            }
        });



        TextView versionText = findViewById(R.id.versionNumber);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            versionText.setText(String.format("Version %s", version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }



    public void onLoginClick(View view) {
        if(isExpired){
            Toast.makeText(this, "Please renew your app key", Toast.LENGTH_SHORT).show();
            return;
        }
        if (pinNumber.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter valid pin number!", Toast.LENGTH_SHORT).show();
        } else {

            int versionCode = BuildConfig.VERSION_CODE;

            System.out.println("=========version code========="+versionCode);
            showProgressDialog();
            APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
            Call<CommonResponse> stringCall = getNoticeDataService.userLogin(pinNumber.getText().toString(), getDeviceId(this),versionCode);
            stringCall.enqueue(new Callback<CommonResponse>() {


                @Override
                public void onResponse(retrofit2.Call<CommonResponse> call, Response<CommonResponse> response) {
//                System.out.println(response);
                    dismissProgressDialog();
                    if (response.isSuccessful()) {
                        CommonResponse commonResponse = response.body();
                        System.out.println("================>>>MMMMMMMmm>>>>" + commonResponse.data.toString());
                        if (commonResponse.success) {
                            try {
                                JSONObject jsonResponse = new JSONObject(commonResponse.data.toString());
                                LoginUserApiEntity loginUserApiEntity = new LoginUserApiEntity();
                                loginUserApiEntity.code = jsonResponse.getString("code");
                                loginUserApiEntity.deviceId = String.valueOf(jsonResponse.getInt("device_id"));
                                loginUserApiEntity.name = "Kurunagala 01";
                                loginUserApiEntity.target = jsonResponse.getString("target");
                                loginUserApiEntity.message = jsonResponse.getString("message");

                                if(!shared.getCurrentUser().deviceId.isEmpty()  && Integer.parseInt(shared.getCurrentUser().deviceId)  != Integer.parseInt(loginUserApiEntity.deviceId) ){

                                    Toast.makeText(LoginActivity.this, "You can't login this user id with in this device.", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                shared.setLoginStatus(true);
                                shared.setCurrentUser(loginUserApiEntity);
                                shared.setCurrentInv(jsonResponse.getInt("inv_code"));


                                startActivity(new Intent(LoginActivity.this, RootViewActivity.class));
                                finish();
                            } catch (Exception e) {
                                Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, commonResponse.message, Toast.LENGTH_LONG).show();
                        }

//
                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(LoginActivity.this, jObjError.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                }

                @Override
                public void onFailure(retrofit2.Call<CommonResponse> call, Throwable t) {
//                dismissProgress();
                    dismissProgressDialog();
                    System.out.println(t.getMessage());
                    Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//                    Toast.makeText(LoginActivity.this, "Wrong pin number, Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        }




    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, "Processing...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


}
