package com.prologics.Bellose.activities.reports;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;

import com.prologics.Bellose.databaseModule.Entity.InvoiceReport;
import com.prologics.Bellose.databaseModule.Entity.InvoiceReportSummery;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.printModule.InvoiceSmallPrint;
import com.prologics.Bellose.recyclerViews.InvoiceReportRecyclerViewAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import com.prologics.Bellose.R;

import java.util.ArrayList;
import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;
import static com.prologics.Bellose.databaseModule.GlobalValues.IS_SMALL_PRINT;

public class InvoiceReportActivity extends AppCompatActivity {


    private InvoiceViewModel invoiceViewModel;
    private String startDate;
    private String endDate;
    private InvoiceSmallPrint invoicePrint;
    private List<InvoiceReport> invoiceList;
    private InvoiceReportSummery reportSummery;
    private RecyclerView recyclerView;
    private InvoiceReportRecyclerViewAdapter invoiceRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_report);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);

        invoicePrint = InvoiceSmallPrint.getInstance(InvoiceReportActivity.this,this);
        Shared shared = Shared.getInstance(this);
        FloatingActionButton fab = findViewById(R.id.fab);
        if(!IS_SMALL_PRINT){
            fab.hide();
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shared.isPrintModeEnabled()){
                    printReport();
                }

            }
        });


        recyclerView = findViewById(R.id.list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        startDate = getIntent().getStringExtra("startDate");
        endDate = getIntent().getStringExtra("endDate");
        TextView totalTextView = findViewById(R.id.totalText);
        TextView dateRange = findViewById(R.id.dateRange);
        dateRange.setText(String.format("%s to %s", startDate, endDate));
        TextView notification = findViewById(R.id.notification);
        List<InvoiceReport> dummyData = new ArrayList<>();
        invoiceRecyclerViewAdapter = new InvoiceReportRecyclerViewAdapter(dummyData);
        recyclerView.setAdapter(invoiceRecyclerViewAdapter);
        invoiceViewModel.getInvoiceReport(startDate,endDate).observe(this, new Observer<List<InvoiceReport>>() {
            @Override
            public void onChanged(List<InvoiceReport> invoiceReports) {
                System.out.println("=======>>"+invoiceReports.size());
                invoiceList = invoiceReports;
                if(invoiceReports.size() > 0){
                    notification.setVisibility(View.GONE);
                }
                else {
                    notification.setVisibility(View.VISIBLE);
                }

                invoiceRecyclerViewAdapter.setmValues(invoiceReports);
                recyclerView.getAdapter().notifyDataSetChanged();
                for(InvoiceReport invoiceReport : invoiceReports){
                    System.out.println("=======>>"+invoiceReport.customer_name);
                    System.out.println("=======>>"+invoiceReport.invoice.invoice_code);
                    System.out.println("=======>>"+invoiceReport.invoice.effDate);

                }

            }
        });



        invoiceViewModel.getInvoiceReportTotal(startDate,endDate).observe(this, new Observer<InvoiceReportSummery>() {
            @Override
            public void onChanged(InvoiceReportSummery invoiceReportSummery) {
                reportSummery = invoiceReportSummery;
                totalTextView.setText(DECIMAL_FORMAT.format(invoiceReportSummery.invoiceTotalValue));
            }
        });


    }


    private  void printReport(){
        if(invoiceList == null || invoiceList.size() == 0 ){
            return;
        }
        invoicePrint.printInvoiceReport(startDate,endDate,invoiceList,reportSummery);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(invoiceViewModel != null){
            if(invoiceViewModel.getInvoiceReport(startDate,endDate).hasObservers()){
                invoiceViewModel.getInvoiceReport(startDate,endDate).removeObservers(this);
            }

            if(invoiceViewModel.getInvoiceReportTotal(startDate,endDate).hasObservers()){
                invoiceViewModel.getInvoiceReportTotal(startDate,endDate).removeObservers(this);
            }
        }
    }







    public void onBackPressed(View v) {
        onBackPressed();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_stock_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                System.out.println(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(invoiceRecyclerViewAdapter != null){
                    invoiceRecyclerViewAdapter.getFilter().filter(newText);
                    invoiceRecyclerViewAdapter.notifyDataSetChanged();
                }

                return false;
            }
        });
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
//        return super.onCreateOptionsMenu(menu);

    }



}
