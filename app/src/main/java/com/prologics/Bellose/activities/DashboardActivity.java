package com.prologics.Bellose.activities;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.prologics.Bellose.BuildConfig;
import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.DashboardData;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnCheckAppVersion;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.helper.callbacks.OntaskCompleteInvoice;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.RetrofitInstance;
import com.prologics.Bellose.services.ConnectivityViewModel;

import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prologics.Bellose.services.DataSyncService.checkVersion;
import static com.prologics.Bellose.services.DataSyncService.getCustomerInvoicesWithApi;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    private NavigationView navigationView;
    private ConnectivityViewModel connectivityViewModel;
    private InvoiceViewModel invoiceViewModel;
    private InvoiceItemViewModel invoiceItemViewModel;
    private Shared shared;



    public NavigationView getNavigationView() {
        return navigationView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        Date date = new Date();
        invoiceViewModel.getProductiveInvoiceCount(dateFormat.format(date)).observe(this, new Observer<DashboardData>() {
            @Override
            public void onChanged(DashboardData dashboardData) {



                if (dashboardData.syncCount > 0) {
                    shared.setHaveUnSyncInvoices(true);
                } else {
                    shared.setHaveUnSyncInvoices(false);
                }


            }
        });


        shared = Shared.getInstance(this);


        navigationView = findViewById(R.id.nav_view);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        FloatingActionButton fab = findViewById(R.id.fab);


        navigationView.getMenu().findItem(R.id.nav_logout).setOnMenuItemClickListener(item -> {
            new AlertDialog.Builder(this)
                    .setTitle("Warning !")
                    .setMessage("Do you need to logout now?")

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            logout();
                        }
                    })

                    // A null listener allows the button to dismiss the dialog and take no further action.
                    .setNegativeButton(android.R.string.cancel, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return true;
        });


//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home,
                R.id.nav_customer,
                R.id.nav_customer_list,
                R.id.nav_expenses,
                R.id.nav_root_plan,
                R.id.nav_reports,
                R.id.nav_settings,
                R.id.nav_logout,
                R.id.action_settings
        )
                .setDrawerLayout(drawer)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);



//        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
//            @Override
//            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
//                System.out.println("======>>>>>>>>>>>>>nav change");
////                if(navigationView.getCheckedItem()){
////                    System.out.println("======>>>>>>>>>>>>>nav change");
////                }
//            }
//        });

//        navigationView.getMenu().findItem(R.id.action_settings).setOnMenuItemClickListener(item -> {
//            System.out.println("=======");
//            return true;
//        });


        connectivityViewModel = ConnectivityViewModel.getInstance(this);




    }


    public void clearAllUserData(){
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        int mPendingIntentId = 2;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
        ((ActivityManager) Objects.requireNonNull(this.getSystemService(Context.ACTIVITY_SERVICE))).clearApplicationUserData();
////        startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
////        finish();


    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            if (connectivityViewModel.onCheckConnectivity(this)) {
                Toast.makeText(this, "You have internet connectivity", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Can't find internet connectivity", Toast.LENGTH_SHORT).show();
            }


            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onLogout(View view) {
        System.out.println("===========???>>>>>>>>>>>");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void logout() {

        if (shared.isHaveUnSyncInVoices()) {

            Toast.makeText(this, "Please sync your offline invoices", Toast.LENGTH_SHORT).show();
            return;
        }

        Shared shared = Shared.getInstance(this);
        shared.setLoginStatus(false);
        shared.setRootStatus(false);

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(intent);

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        System.out.println("======>>>>>>>>>>>>>nav select");
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkVersion(new OnCheckAppVersion() {
            @Override
            public void onAppUpdate() {

                logout();
            }

            @Override
            public void onExpired() {
                logout();
            }

            @Override
            public void onSuccess() {

            }
        });

    }




}
