package com.prologics.Bellose.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.BatteryManager;
import android.os.Bundle;

import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceFull;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;

import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnInvoicePreviewListner;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.RetrofitInstance;
import com.prologics.Bellose.recyclerViews.SyncRecyclerAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.prologics.Bellose.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_NUMBER_KEY;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_PRE_FLAG;
import static com.prologics.Bellose.helper.Common.roundVal;

public class InvoiceSyncActivity extends AppCompatActivity {

    private List<InvoiceFull> invoices;
    private RecyclerView recyclerView;
    private InvoiceViewModel invoiceViewModel;
    private Shared shared;
    private SyncRecyclerAdapter syncRecyclerAdapter;
    private boolean isApiServiceRunning = false;
    private ProgressDialog mProgressDialog;
    private boolean isStopSync = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_sync);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.listView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));



        shared = Shared.getInstance(this);


        invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);


        OnInvoicePreviewListner onInvoicePreviewListner = new OnInvoicePreviewListner() {
            @Override
            public void onPreview(Invoice invoice) {
                shared.setSelectedCustomerId(invoice.customerId);
                Intent intent = new Intent(InvoiceSyncActivity.this, InvoiceReprintActivity.class);
                intent.putExtra(INVOICE_NUMBER_KEY,invoice.invoice_code);
                intent.putExtra("isPreSale",invoice.flag == INVOICE_PRE_FLAG);
                startActivity(intent);
            }
        };

        ArrayList<InvoiceFull> dummyData = new ArrayList<>();
        syncRecyclerAdapter = new SyncRecyclerAdapter(this,dummyData,onInvoicePreviewListner);
        recyclerView.setAdapter(syncRecyclerAdapter);

        invoiceViewModel.getNotSyncInvoices().observe(this, new Observer<List<InvoiceFull>>() {
            @Override
            public void onChanged(List<InvoiceFull> invoicePrints) {


                invoices = invoicePrints;
                syncRecyclerAdapter.invoices = invoicePrints;
                recyclerView.getAdapter().notifyDataSetChanged();


            }
        });


        Button startButton = findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(invoices.size() > 0 && !isApiServiceRunning){
                    isApiServiceRunning = true;
                    isStopSync = false;
                    showProgressDialog();
                    for(int i = 0 ; i < invoices.size(); i++ ){
                        InvoiceFull invoiceFull = invoices.get(i);
                        if(!isStopSync){
                            runApi(invoiceFull.invoice,invoiceFull.invoiceItemList,i == invoices.size()-1);
                        }

                    }
                }
            }
        });

       
    }


    private  synchronized void runApi(Invoice invoice, List<InvoiceItem> invoiceItems,boolean isLast){
        ArrayList<JSONObject> itemList = new ArrayList<>();

        System.out.println("=====sync res===="+invoice.customerId+"---------->>>>>>>>>>"+invoice.invoice_code);
        for(InvoiceItem invoiceItem : invoiceItems){
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("invoice_item_id", invoiceItem.invoice_item_id);
                jsonObject.put("invoice_code", invoice.invoice_code);
                jsonObject.put("customers_id", invoice.customerId);
                jsonObject.put("device_id", Integer.parseInt(shared.getCurrentUser().deviceId));
                jsonObject.put("items_id", invoiceItem.items_id);
                jsonObject.put("items_name", invoiceItem.item_name);
                jsonObject.put("qty_selable", invoiceItem.invoice_item_qty_s);
                jsonObject.put("qty_nonselable", invoiceItem.invoice_item_qty_ns);
                jsonObject.put("mrp", roundVal(invoiceItem.invoiceItemmrp,2) );
                jsonObject.put("discount", roundVal(invoiceItem.invoiceItemDiscount,2) );
                jsonObject.put("discount_type", 0);
                jsonObject.put("discount_amount", roundVal(invoiceItem.invoiceItemTotalDiscount,2) );
                jsonObject.put("is_manual_dis", "");
                jsonObject.put("total", roundVal(invoiceItem.invoiceItemTotalAmount,2) );
                jsonObject.put("eff_date", invoiceItem.updatedAt);
                jsonObject.put("item_type", invoiceItem.invoice_item_flag);
                itemList.add(jsonObject);

            } catch (JSONException e) {

                e.printStackTrace();
                return;
            }
        }




        BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);
        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);








        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.createInvoice(
                invoice.customerId,
                invoice.invoice_code,
                roundVal(invoice.invoice_net_total,2) ,
                roundVal(invoice.invoice_return_total,2) ,
                roundVal(invoice.invoice_total,2) ,
                roundVal(invoice.invoice_other_discount,2) ,
                roundVal(invoice.invoice_total_discount,2) ,
                1,
                invoice.effDate,
                "",
                invoice.isSynced,
                batLevel,
                shared.getLocation().latitude,
                shared.getLocation().longitude,
                invoice.created,
                invoice.flag,
                Integer.parseInt(shared.getCurrentUser().deviceId),
                itemList.toString()
        );

        System.out.println("======>>>>>>>"+itemList);
        System.out.println("===api call----------3------===>>>>>>>");
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    System.out.println("=======success 123======>>>>>>>" + response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(jsonObject.getBoolean("success")){
                            invoice.isSynced = true;
                            invoiceViewModel.update(Collections.singletonList(invoice), new OnTaskCompleted() {
                                @Override
                                public void onTaskCompleted() {
                                    if(isLast){
                                        isStopSync = true;
                                        showAlert("Completed");
                                        onBackPressed();
                                    }
                                }
                            });
                        }
                        else{
                            isStopSync = true;
                            dismissProgressDialog();
                            showAlert("Error occurred, Please try again..");

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        dismissProgressDialog();
                        showAlert("Error occurred, Please try again..");
                    }
                }
                System.out.println("=======success 22>>>>>>" + response.message());
//                dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("=======error======>>>>>>>" + t.getMessage());
                dismissProgressDialog();
            }
        });
    }


    private  void showAlert(String msg){
        Toast.makeText(this,msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismissProgressDialog();
    }

    public void onBackPressed(View view) {
        onBackPressed();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, "Processing...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

}
