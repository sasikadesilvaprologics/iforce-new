package com.prologics.Bellose.activities;

import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.printModule.InvoiceLargePrint;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.prologics.Bellose.R;
import com.prologics.Bellose.printModule.InvoicePrintModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;
import static com.prologics.Bellose.databaseModule.GlobalValues.FREE_ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_DRAFT_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_NUMBER_KEY;
import static com.prologics.Bellose.databaseModule.GlobalValues.ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.RETURN_ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.SAMPLE_ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.getDate;

public class InvoiceReprintActivity extends AppCompatActivity {

    private InvoiceViewModel invoiceViewModel;
    private ItemViewModel itemViewModel;
    private InvoiceItemViewModel invoiceItemViewModel;
    private boolean isPreSale = false;
    private Shared shared;
    private int selectedInv = 0;
    public int negativeCount = 1;
    public List<InvoiceItem> invoiceItemList = new ArrayList<>();
    private Invoice invoice;
//    private InvoiceSmallPrint invoicePrint;
    private InvoiceLargePrint invoicePrint;
    private List<InvoiceItem> invoicePrintList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_reprint);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        CustomerViewModel customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);

        isPreSale = getIntent().getBooleanExtra("isPreSale", false);
        shared = Shared.getInstance(this);

        if(shared.isPrintModeEnabled()){
            invoicePrint = InvoiceLargePrint.getInstance(InvoiceReprintActivity.this, this);
        }



        TextView customerName = findViewById(R.id.customerName);
        TextView productDiscount = findViewById(R.id.productDiscount);
        TextView productNetTotal = findViewById(R.id.productNetTotal);
        TextView productReturnTotal = findViewById(R.id.returnTotal);
        TextView productInvoiceTotal = findViewById(R.id.invoiceTotal);
        TextView productOtherDiscount = findViewById(R.id.productOtherDiscount);
        TextView invoiceDate = findViewById(R.id.invDate);
        TextView invNo = findViewById(R.id.invNumber);
        Button printButton = findViewById(R.id.printButton);
        Customer customer = customerViewModel.getCustomer(shared.getSelectedCustomerId());
        customerName.setText(customer.getName());

//        if (isPreSale) {
//            printButton.setText("Confirm");
//        }


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));


        invoiceViewModel.getInvoiceById(getIntent().getIntExtra(INVOICE_NUMBER_KEY, 0)).observe(this, invoiceItem -> {
            this.invoice = invoiceItem;
            productDiscount.setText(DECIMAL_FORMAT.format(invoiceItem.invoice_total_discount));
            productInvoiceTotal.setText(DECIMAL_FORMAT.format(invoiceItem.invoice_total));
            productNetTotal.setText(DECIMAL_FORMAT.format(invoiceItem.invoice_net_total+invoiceItem.invoice_total_discount));
            productReturnTotal.setText(String.format("-%s", DECIMAL_FORMAT.format(invoiceItem.invoice_return_total)));
            productOtherDiscount.setText(DECIMAL_FORMAT.format(invoiceItem.invoice_other_discount));
            invNo.setText(String.format("INV%d", invoiceItem.invoice_code));
            selectedInv = invoiceItem.invoice_code;
            String formatedDate = "N/A";
            try {
                Date date = dateFormat.parse(invoiceItem.created);
                formatedDate = dateFormat.format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }

            invoiceDate.setText(formatedDate);
        });


        invoiceItemViewModel.getItemsByInvoiceId(getIntent().getIntExtra(INVOICE_NUMBER_KEY, 0)).observe(this, new Observer<List<InvoiceItem>>() {
            @Override
            public void onChanged(List<InvoiceItem> invoiceItems) {

                invoicePrintList = invoiceItems;

            }
        });




        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (isPreSale) {
//                    loadPreSaleInvoice();
//                } else {
                    if (invoicePrintList == null || !shared.isPrintModeEnabled()) {
                        return;
                    }

                    List<InvoiceItem> salesItems = new ArrayList<>();
                    List<InvoiceItem> samplesItems = new ArrayList<>();
                    List<InvoiceItem> freeItems = new ArrayList<>();
                    List<InvoiceItem> returnItems = new ArrayList<>();


                    for (InvoiceItem invoicePrint : invoicePrintList) {
                        if (invoicePrint.invoice_item_flag == ITEM_CODE) {
                            salesItems.add(invoicePrint);
                        } else if (invoicePrint.invoice_item_flag == SAMPLE_ITEM_CODE) {
                            samplesItems.add(invoicePrint);
                        } else if (invoicePrint.invoice_item_flag == FREE_ITEM_CODE) {
                            freeItems.add(invoicePrint);
                        } else if (invoicePrint.invoice_item_flag == RETURN_ITEM_CODE) {
                            returnItems.add(invoicePrint);
                        }
                    }

                    invoicePrint.startPrint(InvoicePrintModel.getInstance(
                            true,
                            "Re Print",
                            customer,
                            invoice,
                            salesItems,
                            samplesItems,
                            freeItems,
                            returnItems
                    ), new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted() {

                        }
                    });
                }
//            }
        });


    }


    private void loadPreSaleInvoice() {


        if (negativeCount == 0) {
            onBackPressed();
            shared.setSelectedInv(selectedInv);
            invoice.flag = INVOICE_DRAFT_FLAG;
            invoiceViewModel.insert(Collections.singletonList(invoice), new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {

                    List<Item> itemList = new ArrayList<>();
                    for (InvoiceItem invoiceItem : invoiceItemList) {
                        Item item = itemViewModel.getItem(invoiceItem.items_id);
                        item.qty = item.qty - invoiceItem.invoice_item_qty_s;
                        item.updatedAt = getDate();
                        itemList.add(item);
                    }
                    itemViewModel.insertItems(itemList, new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted() {

                        }
                    });
                    Intent intent = new Intent(InvoiceReprintActivity.this, NewInvoiceActivity.class);
                    intent.putExtra("isPreSaleInvoice", true);
                    startActivity(intent);
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "Stock isn't enough to proceed this invoice", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (invoiceViewModel != null && invoiceViewModel.getInvoiceById(getIntent().getIntExtra(INVOICE_NUMBER_KEY, 0)).hasObservers()) {
            invoiceViewModel.getInvoiceById(getIntent().getIntExtra(INVOICE_NUMBER_KEY, 0)).removeObservers(this);
        }

    }

    public void onBackPressed(View v) {
        onBackPressed();
    }

}
