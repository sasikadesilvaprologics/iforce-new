package com.prologics.Bellose.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.BatteryManager;
import android.os.Bundle;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceFull;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.InvoiceStoreItem;
import com.prologics.Bellose.databaseModule.Entity.InvoiceSummery;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.Entity.Self;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.SelfViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.fragments.dialogs.OtherDiscountDialog;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.helper.callbacks.OntaskCompleteInvoice;
import com.prologics.Bellose.network.APIClient;
import com.prologics.Bellose.network.RetrofitInstance;
import com.prologics.Bellose.printModule.InvoiceLargePrint;
import com.prologics.Bellose.printModule.InvoicePrintModel;
import com.google.android.material.tabs.TabLayout;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prologics.Bellose.fragments.AddNewInvoiceFragments.main.NewInvoiceActivitySectionsPagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;
import static com.prologics.Bellose.databaseModule.GlobalValues.FREE_ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_DRAFT_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_PRE_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_PRINTED_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.INVOICE_SAVED_FLAG;
import static com.prologics.Bellose.databaseModule.GlobalValues.ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.ITEM_TYPE_KEY;
import static com.prologics.Bellose.databaseModule.GlobalValues.RETURN_ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.SAMPLE_ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.getDate;
import static com.prologics.Bellose.helper.Common.roundVal;

public class NewInvoiceActivity extends AppCompatActivity {


//    APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
//    Call<CommonResponse> stringCall = getNoticeDataService.userLogin(pinNumber.getText().toString(), "5f97fa5f9e9e6958");

    private int tabIndex = 0;
    private double netTotal = 0.0;
    private double discount = 0.0;
    private double returnTotal = 0.0;
    private double invoiceTotal = 0.0;
    private double otherDiscountValue = 0.0;
    private int itemCount = 0;
    private List<InvoiceStoreItem> invoiceItemList = new ArrayList<>();
    private ItemViewModel itemViewModel;
    private Shared shared;
    private InvoiceItemViewModel invoiceItemViewModel;
    private InvoiceViewModel invoiceViewModel;
    private TextView productDiscount;
    private TextView productNetTotal;
    private TextView productReturnTotal;
    private TextView productInvoiceTotal;
    private TextView productOtherDiscount;
    private boolean isInvoiceCompleted = false;
    private boolean isInvoiceAutoLoaded = false;
    private List<Item> reverseStoreitemList = new ArrayList<>();
    public List<InvoiceItem> preSaleInvoiceItemList = new ArrayList<>();
    public boolean isPreSaleInvoice = false;

    private boolean isPreSale = false;

    private Invoice selectedInvoice;

//    private InvoiceSmallPrint invoicePrint;
    private InvoiceLargePrint invoicePrint;
    private Customer customer;

    private List<InvoiceItem> invoicePrintList;
    private ProgressDialog mProgressDialog;
    private SelfViewModel selfViewModel;
    private List<InvoiceFull> notSyncInvoices;
    private boolean isStartOldSyncService = false;

    private boolean isPrintedOne = false;



    public Invoice getSelectedInvoice() {
        return selectedInvoice;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_invoice);
        CustomerViewModel customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);
        selfViewModel = new ViewModelProvider(this).get(SelfViewModel.class);
        shared = Shared.getInstance(this);

        isPreSale = getIntent().getBooleanExtra("isPreSale", false);
        isPreSaleInvoice = getIntent().getBooleanExtra("isPreSaleInvoice", false);

        if(shared.isPrintModeEnabled()){
            invoicePrint = InvoiceLargePrint.getInstance(NewInvoiceActivity.this, this);
        }




        customer = customerViewModel.getCustomer(shared.getSelectedCustomerId());
        TextView customerName = findViewById(R.id.customerName);
        customerName.setText(customer.getName());
        TextView customerAddress = findViewById(R.id.customerAddress);
        customerAddress.setText(String.format("%s ,%s ,%s", customer.getAddressNo(), customer.getAddress1(), customer.getAddress2()));
        TextView customerType = findViewById(R.id.customerType);
        TextView invNo = findViewById(R.id.invoiceNumber);
        invNo.setText("INV" + String.valueOf(shared.getSelectedInv()));


        Button saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new OnClickSaveButtonListner(invoiceViewModel));
        Button printButton = findViewById(R.id.printButton);
        Button autoLoadButton = findViewById(R.id.autoLoadButton);
        LinearLayout otherDiscountButton = findViewById(R.id.otherDiscountButton);
        productDiscount = findViewById(R.id.productDiscount);
        productNetTotal = findViewById(R.id.productNetTotal);
        productReturnTotal = findViewById(R.id.returnTotal);
        productInvoiceTotal = findViewById(R.id.invoiceTotal);
        productOtherDiscount = findViewById(R.id.productOtherDiscount);

        invoiceItemViewModel.getItemsByInvoiceId(shared.getSelectedInv()).observe(this, new Observer<List<InvoiceItem>>() {
            @Override
            public void onChanged(List<InvoiceItem> invoiceItems) {
                invoicePrintList = invoiceItems;
            }
        });


        invoiceViewModel.getNotSyncInvoices().observe(this, new Observer<List<InvoiceFull>>() {
            @Override
            public void onChanged(List<InvoiceFull> invoicePrints) {
                notSyncInvoices = invoicePrints;
            }
        });





        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(NewInvoiceActivity.this, SignatureActivity.class));


                onSaveInvoice(new InvoiceSaveCallback() {
                    @Override
                    public void completed(Invoice invoice) {
                        if(shared.isPrintModeEnabled()){
                            startPrint(invoice);
                        }
                        else{

                            onBackPressed();
                        }

                    }
                });

            }
        });

//        if(isPreSale){
//            printButton.setVisibility(View.GONE);
//            autoLoadButton.setVisibility(View.GONE);
//        }


        otherDiscountButton.setOnClickListener(v -> {

            OtherDiscountDialog otherDiscountDialog = new OtherDiscountDialog();
            otherDiscountDialog.show(getSupportFragmentManager(), "otherDiscountDialog");
            otherDiscountDialog.setCancelListner(v1 ->
                    otherDiscountDialog.dismiss()
            );

            otherDiscountDialog.setSubmitValueListner(value -> {
                otherDiscountValue = Double.parseDouble(value);
                if (otherDiscountValue < 0 || invoiceTotal == 0) {
                    otherDiscountValue = 0.00;
                }
                productOtherDiscount.setText(DECIMAL_FORMAT.format(otherDiscountValue));
                productInvoiceTotal.setText(DECIMAL_FORMAT.format(invoiceTotal - otherDiscountValue));
                invoiceTotal = invoiceTotal-otherDiscountValue;
                otherDiscountDialog.dismiss();
            });
        });

        invoiceViewModel.getInvoiceById(shared.getSelectedInv()).observe(this, new Observer<Invoice>() {
            @Override
            public void onChanged(Invoice invoice) {
                selectedInvoice = invoice;
            }
        });

        invoiceViewModel.getInvoiceSummery(shared.getSelectedInv()).observe(this, invoiceSummeries -> {
            netTotal = 0.0;
            discount = 0.0;
            returnTotal = 0.0;
            invoiceTotal = 0.0;
            for (InvoiceSummery invoiceSummery : invoiceSummeries) {

                if (invoiceSummery.invoice_item_flag == ITEM_CODE) {
                    netTotal = netTotal + invoiceSummery.total;
                    discount = discount + invoiceSummery.discount_total;
                } else if (invoiceSummery.invoice_item_flag == RETURN_ITEM_CODE) {
                    returnTotal = returnTotal + invoiceSummery.total;
                }
            }

            if ((netTotal - returnTotal) == 0) {
                otherDiscountValue = 0.00;
            }
            invoiceTotal = netTotal - (returnTotal + otherDiscountValue);

            productDiscount.setText(DECIMAL_FORMAT.format(discount));
            productNetTotal.setText(DECIMAL_FORMAT.format((netTotal+discount)));
            productReturnTotal.setText(String.format("-%s", DECIMAL_FORMAT.format(returnTotal)));
            productInvoiceTotal.setText(DECIMAL_FORMAT.format(invoiceTotal));
            productOtherDiscount.setText(DECIMAL_FORMAT.format(otherDiscountValue));

        });


        invoiceItemViewModel.getItemsWithStoreData(shared.getSelectedInv()).observe(this, invoiceStoreItems -> {
            System.out.println("==------------<<<>>>>>>" + invoiceStoreItems.size());
            itemCount = invoiceStoreItems.size();
            invoiceItemList = invoiceStoreItems;
            if (isPreSaleInvoice) {
                for (InvoiceStoreItem invoiceItem : invoiceStoreItems) {
                    Item item = itemViewModel.getItem(invoiceItem.item_id);
                    item.qty = item.qty + invoiceItem.invoice_item_qty_s;
                    item.updatedAt = getDate();
                    reverseStoreitemList.add(item);
                }
            }
        });


        LinearLayout add = findViewById(R.id.addNewItemButtom);
        add.setOnClickListener(v -> {

            Intent intent = new Intent(NewInvoiceActivity.this, StockActivity.class);
            intent.putExtra(ITEM_TYPE_KEY, tabIndex);
            intent.putExtra("isPreSale", isPreSale);
            startActivity(intent);
        });

        autoLoadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAutoLoadInvoice();
            }
        });


        NewInvoiceActivitySectionsPagerAdapter newInvoiceActivitySectionsPagerAdapter = new NewInvoiceActivitySectionsPagerAdapter(this, getSupportFragmentManager(), isPreSale);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(newInvoiceActivitySectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabIndex = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void startPrint(Invoice invoice) {

        if (invoicePrintList == null) {
            return;
        }

        List<InvoiceItem> salesItems = new ArrayList<>();
        List<InvoiceItem> samplesItems = new ArrayList<>();
        List<InvoiceItem> freeItems = new ArrayList<>();
        List<InvoiceItem> returnItems = new ArrayList<>();


        for (InvoiceItem invoicePrint : invoicePrintList) {
            if (invoicePrint.invoice_item_flag == ITEM_CODE) {
                salesItems.add(invoicePrint);
            } else if (invoicePrint.invoice_item_flag == SAMPLE_ITEM_CODE) {
                samplesItems.add(invoicePrint);
            } else if (invoicePrint.invoice_item_flag == FREE_ITEM_CODE) {
                freeItems.add(invoicePrint);
            } else if (invoicePrint.invoice_item_flag == RETURN_ITEM_CODE) {
                returnItems.add(invoicePrint);
            }
        }






        invoicePrint.startPrint(InvoicePrintModel.getInstance(
                true,
                "Re Print",
                customer,
                invoice,
                salesItems,
                samplesItems,
                freeItems,
                returnItems
        ), new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {
                onBackPressed();
            }
        });

    }


    private class OnClickSaveButtonListner implements View.OnClickListener {

        private InvoiceViewModel invoiceViewModel;

        OnClickSaveButtonListner(InvoiceViewModel invoiceViewModel) {
            this.invoiceViewModel = invoiceViewModel;
        }

        @Override
        public void onClick(View v) {

            onSaveInvoice(new InvoiceSaveCallback() {
                @Override
                public void completed(Invoice invoice) {
                    onBackPressed();
                }
            });

        }
    }


    private void onSaveInvoice(InvoiceSaveCallback invoiceSaveCallback) {

        if( notSyncInvoices.size() > 0 && !isStartOldSyncService ){
            isStartOldSyncService = true;
            for(InvoiceFull invoiceFull :  notSyncInvoices){
                runApi(invoiceFull.invoice,invoiceFull.invoiceItemList,false);
            }


        }


        if (itemCount == 0) {
            showToastMessage("Please add invoice items");
            return;
        }
        Button saveButton = findViewById(R.id.saveButton);
        Button printButton = findViewById(R.id.printButton);
        Button autoLoadButton = findViewById(R.id.autoLoadButton);
        saveButton.setVisibility(View.GONE);
        printButton.setVisibility(View.GONE);
        autoLoadButton.setVisibility(View.GONE);
        showProgressDialog();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        Date date = new Date();

        Invoice invoice = selectedInvoice;


        if(invoice == null){
            dismissProgressDialog();
            return;

        }
        if(invoice.flag == INVOICE_PRINTED_FLAG || invoice.flag == INVOICE_SAVED_FLAG){
            dismissProgressDialog();
            return;
        }




        invoice.invoice_code = shared.getSelectedInv();
        invoice.isSynced = false;
        invoice.flag = INVOICE_SAVED_FLAG;
        invoice.deviceId = Integer.parseInt(shared.getCurrentUser().deviceId);
        invoice.customerId = shared.getSelectedCustomerId();
        invoice.created = dateFormat.format(date);
        invoice.effDate = dateFormat.format(date);
        invoice.invoice_net_total = netTotal;
        invoice.invoice_return_total = returnTotal;
        invoice.invoice_total_discount = discount;
        invoice.invoice_total = invoiceTotal;
        invoice.invoice_other_discount = otherDiscountValue;
        if (isPreSale) {
            invoice.flag = INVOICE_PRE_FLAG;
        }

        invoiceViewModel.update(Collections.singletonList(invoice), () -> {
            if (!isPreSaleInvoice) {
                shared.setCurrentInv(shared.getSelectedInv());
            }
            isInvoiceCompleted = true;

            if (!isPreSale) {
                onStockUpdate(() -> System.out.println("===========>>>>>>>>store updated"));
            }

            showProgressDialog();
            syncInvoice(invoice, new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {
                    dismissProgressDialog();
                    invoiceSaveCallback.completed(invoice);
                }
            });


        });
    }


    private void syncInvoice(Invoice invoice, OnTaskCompleted onTaskCompleted) {

        ArrayList<JSONObject> itemList = new ArrayList<>();
        for(InvoiceItem invoiceItem : invoicePrintList){
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("invoice_item_id", invoiceItem.invoice_item_id);
                jsonObject.put("invoice_code", invoice.invoice_code);
                jsonObject.put("customers_id", invoice.customerId);
                jsonObject.put("device_id", Integer.parseInt(shared.getCurrentUser().deviceId));
                jsonObject.put("items_id", invoiceItem.items_id);
                jsonObject.put("items_name", invoiceItem.item_name);
                jsonObject.put("qty_selable", invoiceItem.invoice_item_qty_s);
                jsonObject.put("qty_nonselable", invoiceItem.invoice_item_qty_ns);
                jsonObject.put("mrp", roundVal(invoiceItem.invoiceItemmrp,2) );
                jsonObject.put("discount", roundVal(invoiceItem.invoiceItemDiscount,2) );
                jsonObject.put("discount_type", 0);
                jsonObject.put("discount_amount", roundVal(invoiceItem.invoiceItemTotalDiscount,2) );
                jsonObject.put("is_manual_dis", "");
                jsonObject.put("total", roundVal(invoiceItem.invoiceItemTotalAmount,2) );
                jsonObject.put("eff_date", invoiceItem.updatedAt);
                jsonObject.put("item_type", invoiceItem.invoice_item_flag);
                itemList.add(jsonObject);

            } catch (JSONException e) {

                e.printStackTrace();
                return;
            }
        }




        BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);
        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);








        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.createInvoice(
                invoice.customerId,
                invoice.invoice_code,
                roundVal(invoice.invoice_net_total,2) ,
                roundVal(invoice.invoice_return_total,2) ,
                roundVal(invoice.invoice_total,2) ,
                roundVal(invoice.invoice_other_discount,2) ,
                roundVal(invoice.invoice_total_discount,2) ,
                1,
                invoice.effDate,
                "",
                invoice.isSynced,
                batLevel,
                shared.getLocation().latitude,
                shared.getLocation().longitude,
                invoice.created,
                invoice.flag,
                Integer.parseInt(shared.getCurrentUser().deviceId),
                itemList.toString()
        );

        System.out.println("======>>>>>>>"+itemList);
        System.out.println("===api call----------1------===>>>>>>>");
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    System.out.println("=======success 123======>>>>>>>" + response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(jsonObject.getBoolean("success")){
                            invoice.isSynced = true;
                            invoiceViewModel.update(Collections.singletonList(invoice), new OnTaskCompleted() {
                                @Override
                                public void onTaskCompleted() {


                                        onTaskCompleted.onTaskCompleted();



                                }
                            });
                        }
                        else{
                            onTaskCompleted.onTaskCompleted();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        onTaskCompleted.onTaskCompleted();
                    }
                }
                else{
                    onTaskCompleted.onTaskCompleted();
                }
                System.out.println("=======success 22>>>>>>" + response.message());
//                dismissProgressDialog();


            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("=======error======>>>>>>>" + t.getMessage());
                onTaskCompleted.onTaskCompleted();
            }
        });
    }


//


    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void onStockUpdate(OnTaskCompleted onTaskCompleted) {

        List<Item> itemList = new ArrayList<>();
        Map<Integer, Self> selves = new HashMap<>();

        for (InvoiceStoreItem invoiceStoreItem : invoiceItemList) {


            if (invoiceStoreItem.invoice_item_flag == RETURN_ITEM_CODE) {
                Item item = new Item();
                item.qty = invoiceStoreItem.qty + invoiceStoreItem.invoice_item_qty_s;
                item.qtyNs = invoiceStoreItem.qtyNs + invoiceStoreItem.invoice_item_qty_ns;
                item.minPrice = invoiceStoreItem.minPrice;
                item.maxPrice = invoiceStoreItem.maxPrice;
                item.item_id = invoiceStoreItem.item_id;
                item.code = invoiceStoreItem.code;
                item.id = invoiceStoreItem.id;
                item.sellingPrice = invoiceStoreItem.sellingPrice;
                item.itemName = invoiceStoreItem.itemName;
                item.updatedAt = getDate();
                itemList.add(item);
            } else {

                Self self = selfViewModel.getItem(shared.getSelectedCustomerId(), invoiceStoreItem.item_id);
                System.out.println("=========self8888====>>>>>" + self);

                if (self == null) {
                    self = new Self();
                    self.customerId = shared.getSelectedCustomerId();
                    self.itemsid = invoiceStoreItem.item_id;
                    self.ownQty = invoiceStoreItem.invoice_item_qty_s;
                    if (selves.containsKey(invoiceStoreItem.item_id)) {
                        self.ownQty = Objects.requireNonNull(selves.get(invoiceStoreItem.item_id)).ownQty + invoiceStoreItem.invoice_item_qty_s;
                        selves.remove(invoiceStoreItem.item_id);
                    }
                    selves.put(invoiceStoreItem.item_id, self);
                } else {
                    self.ownQty = self.ownQty + invoiceStoreItem.invoice_item_qty_s;
                    selves.put(invoiceStoreItem.item_id, self);
                }


            }


        }

        if (itemList.size() > 0) {
            itemViewModel.insertItems(itemList, onTaskCompleted);
        }

        if (selves.size() > 0) {
            List<Self> list = new ArrayList<>();
            for (Self self : selves.values()) {
                list.add(self);
            }
            selfViewModel.insertAll(list, onTaskCompleted);
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismissProgressDialog();
        if (!isInvoiceCompleted) {

            invoiceItemViewModel.deleteAutoloadItems(shared.getSelectedInv());
            if (reverseStoreitemList.size() > 0) {
                itemViewModel.insertItems(reverseStoreitemList, new OnTaskCompleted() {
                    @Override
                    public void onTaskCompleted() {

                    }
                });
            }

            if (isPreSaleInvoice && selectedInvoice != null) {
                selectedInvoice.flag = INVOICE_PRE_FLAG;
                assert invoiceViewModel != null;
                invoiceViewModel.update(Collections.singletonList(selectedInvoice), new OnTaskCompleted() {
                    @Override
                    public void onTaskCompleted() {

                    }
                });
            }

        }

        if (invoiceViewModel != null && invoiceViewModel.getInvoiceSummery(shared.getSelectedInv()).hasObservers()) {
            invoiceViewModel.getInvoiceSummery(shared.getSelectedInv()).removeObservers(this);
        }

        if (invoiceItemViewModel != null && invoiceItemViewModel.getItemsWithStoreData(shared.getSelectedInv()).hasObservers()) {
            invoiceItemViewModel.getItemsWithStoreData(shared.getSelectedInv()).removeObservers(this);
        }

        if (isPreSale && !isInvoiceCompleted) {
            if (preSaleInvoiceItemList.size() > 0) {
                invoiceItemViewModel.onDelete(preSaleInvoiceItemList, new OnTaskCompleted() {
                    @Override
                    public void onTaskCompleted() {

                    }
                });
            }
        }


    }

    public void onBackPressed(View v) {
        onBackPressed();

    }


    //auto load
    public void onAutoLoadInvoice() {

        if(selectedInvoice != null){
            System.out.println("====flag===="+selectedInvoice.flag);
            if(  selectedInvoice.flag == 1 || selectedInvoice.flag == 2){
                return;
            }
        }


        if (isInvoiceAutoLoaded) {
            showToastMessage("Already loaded");
            return;
        }
        Invoice lastInvoice = invoiceViewModel.getCustomerLastInvoice(shared.getSelectedCustomerId());
        System.out.println("======last======"+lastInvoice);
        if (lastInvoice != null) {
            System.out.println("======last===34==="+lastInvoice.invoice_code);
            Invoice invoice = new Invoice();
            invoice.invoice_code = shared.getSelectedInv();
//            invoice.customerId = shared.getSelectedCustomerId();
            invoice.deviceId = Integer.parseInt(shared.getCurrentUser().deviceId);
            invoice.isSynced = false;
            invoice.flag = INVOICE_DRAFT_FLAG;


            invoiceViewModel.insert(Collections.singletonList(invoice), new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {

                    addLastInvoiceItems(lastInvoice);
                    isInvoiceAutoLoaded = true;
                }
            });


        }

    }

    private void addLastInvoiceItems(Invoice invoice) {
        List<InvoiceItem> newInvoiceItemList = new ArrayList<>();
        List<InvoiceItem> positiveItems = invoiceItemViewModel.getPositiveItemsByInvoiceId(invoice.invoice_code);
        List<Item> storeitemList = new ArrayList<>();
        for (InvoiceItem invoiceItem : positiveItems) {


            InvoiceItem invoiceItem1 = new InvoiceItem();
            invoiceItem1.invoiceId = shared.getSelectedInv();
            invoiceItem1.items_id = invoiceItem.items_id;
            invoiceItem1.invoice_item_flag = invoiceItem.invoice_item_flag;
            invoiceItem1.invoice_item_qty_s = invoiceItem.invoice_item_qty_s;
            invoiceItem1.invoice_item_qty_ns = invoiceItem.invoice_item_qty_ns;
            invoiceItem1.invoiceItemTotalAmount = invoiceItem.invoiceItemTotalAmount;
            invoiceItem1.invoiceItemmrp = invoiceItem.invoiceItemmrp;
            invoiceItem1.invoiceItemTotalDiscount = invoiceItem.invoiceItemTotalDiscount;
            invoiceItem1.deviceId = Integer.parseInt(shared.getCurrentUser().deviceId);
            invoiceItem1.customerId = invoiceItem.customerId;
            invoiceItem1.invoice_item_is_autoLoad = true;

            if (isPreSale) {
                invoiceItem1.isPreSalse = true;
            }


            if (invoiceItem.qty > invoiceItem.invoice_item_qty_s) {
                newInvoiceItemList.add(invoiceItem1);
                Item item = itemViewModel.getItem(invoiceItem.items_id);
                Item reverseItem = itemViewModel.getItem(invoiceItem.items_id);
                reverseItem.updatedAt = getDate();
                item.updatedAt = getDate();
                reverseStoreitemList.add(reverseItem);
                if (item != null) {
                    item.qty = item.qty - invoiceItem.invoice_item_qty_s;

                    storeitemList.add(item);

                }
            } else if (invoiceItem.qty != 0) {
                invoiceItem1.invoice_item_qty_s = invoiceItem.qty;
                Item item = itemViewModel.getItem(invoiceItem.items_id);
                Item reverseItem = itemViewModel.getItem(invoiceItem.items_id);
                item.updatedAt = getDate();
                reverseStoreitemList.add(reverseItem);
                if (item != null) {
                    item.qty = 0;
                    storeitemList.add(item);
                }
                newInvoiceItemList.add(invoiceItem1);
            }


        }
        addLastInvoiceItemsToDb(newInvoiceItemList);
        if (!isPreSale) {
            itemViewModel.insertItems(storeitemList, new OnTaskCompleted() {

                @Override
                public void onTaskCompleted() {

                }
            });
        }


    }

    private void addLastInvoiceItemsToDb(List<InvoiceItem> newInvoiceItemList) {

        invoiceItemViewModel.insertAll(newInvoiceItemList, new OnTaskCompleted() {
            @Override
            public void onTaskCompleted() {
            }
        });
    }


    private interface InvoiceSaveCallback {
        void completed(Invoice invoice);
    }


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, "Processing...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


    private  synchronized void runApi(Invoice invoice, List<InvoiceItem> invoiceItems,boolean isLast){
        ArrayList<JSONObject> itemList = new ArrayList<>();
        for(InvoiceItem invoiceItem : invoiceItems){
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("invoice_item_id", invoiceItem.invoice_item_id);
                jsonObject.put("invoice_code", invoice.invoice_code);
                jsonObject.put("customers_id", invoice.customerId);
                jsonObject.put("device_id", Integer.parseInt(shared.getCurrentUser().deviceId));
                jsonObject.put("items_id", invoiceItem.items_id);
                jsonObject.put("items_name", invoiceItem.item_name);
                jsonObject.put("qty_selable", invoiceItem.invoice_item_qty_s);
                jsonObject.put("qty_nonselable", invoiceItem.invoice_item_qty_ns);
                jsonObject.put("mrp", roundVal(invoiceItem.invoiceItemmrp,2) );
                jsonObject.put("discount", roundVal(invoiceItem.invoiceItemDiscount,2) );
                jsonObject.put("discount_type", 0);
                jsonObject.put("discount_amount", roundVal(invoiceItem.invoiceItemTotalDiscount,2) );
                jsonObject.put("is_manual_dis", "");
                jsonObject.put("total", roundVal(invoiceItem.invoiceItemTotalAmount,2) );
                jsonObject.put("eff_date", invoiceItem.updatedAt);
                jsonObject.put("item_type", invoiceItem.invoice_item_flag);
                itemList.add(jsonObject);


                System.out.println("===========>>>>>>"+jsonObject);

            } catch (JSONException e) {

                e.printStackTrace();
                return;
            }
        }




        BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);
        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);








        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
        Call<String> stringCall = getNoticeDataService.createInvoice(
                invoice.customerId,
                invoice.invoice_code,
                roundVal(invoice.invoice_net_total,2) ,
                roundVal(invoice.invoice_return_total,2) ,
                roundVal(invoice.invoice_total,2) ,
                roundVal(invoice.invoice_other_discount,2) ,
                roundVal(invoice.invoice_total_discount,2) ,
                1,
                invoice.effDate,
                "",
                invoice.isSynced,
                batLevel,
                shared.getLocation().latitude,
                shared.getLocation().longitude,
                invoice.created,
                invoice.flag,
                Integer.parseInt(shared.getCurrentUser().deviceId),
                itemList.toString()
        );

        System.out.println("======>>>>>>>"+itemList);
        System.out.println("===api call----------2------===>>>>>>>");
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    System.out.println("=======success 123======>>>>>>>" + response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(jsonObject.getBoolean("success")){
                            invoice.isSynced = true;
                            invoiceViewModel.update(Collections.singletonList(invoice), new OnTaskCompleted() {
                                @Override
                                public void onTaskCompleted() {

                                }
                            });
                        }
                        else{


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
                System.out.println("=======success 22>>>>>>" + response.message());
//                dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("=======error======>>>>>>>" + t.getMessage());

            }
        });
    }



}