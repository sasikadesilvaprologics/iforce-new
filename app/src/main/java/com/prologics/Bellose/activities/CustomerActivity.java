package com.prologics.Bellose.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.prologics.Bellose.R;
import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.Entity.Invoice;
import com.prologics.Bellose.databaseModule.Entity.InvoiceItem;
import com.prologics.Bellose.databaseModule.Entity.Self;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.SelfUpdateViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.SelfViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.helper.callbacks.OnTaskCompleted;
import com.prologics.Bellose.helper.callbacks.OntaskCompleteInvoice;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.tabs.TabLayout;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prologics.Bellose.fragments.CustomerActivityfragments.main.CustomerActivitySectionsPagerAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import static com.prologics.Bellose.databaseModule.GlobalValues.RETURN_ITEM_CODE;
import static com.prologics.Bellose.helper.Common.getDeviceId;
import static com.prologics.Bellose.services.DataSyncService.getCustomerInvoicesWithApi;

public class CustomerActivity extends AppCompatActivity {

    private CustomerViewModel customerViewModel;
    private SelfUpdateViewModel selfUpdateViewModel;
    private InvoiceViewModel invoiceViewModel;
    private InvoiceItemViewModel invoiceItemViewModel;
    private SelfViewModel selfViewModel;
    private int invoiceCount = 0;
    private boolean isPresale = false;
    private Shared shared;
    private ProgressDialog mProgressDialog;
    public TabLayout tabs;


    private static final int[] TAB_ICONS = new int[]{
            R.drawable.ic_dashboard_black_24dp,
            R.drawable.ic_notifications_black_24dp,
            R.drawable.ic_self_black_24dp,
            R.drawable.ic_invoice_black_24dp,
            R.drawable.ic_pre_sale_black_24dp,
//            R.drawable.ic_payment_black_24dp,
            R.drawable.ic_map_black_24dp
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        selfUpdateViewModel = new ViewModelProvider(this).get(SelfUpdateViewModel.class);
        selfViewModel = new ViewModelProvider(this).get(SelfViewModel.class);
        customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        invoiceItemViewModel = new ViewModelProvider(this).get(InvoiceItemViewModel.class);
        shared = Shared.getInstance(this);
        Customer customer = customerViewModel.getCustomer(shared.getSelectedCustomerId());
        shared.setSelectedCustomerDisc(customer.discount);
        TextView customerName = findViewById(R.id.customerName);
        customerName.setText(customer.getName());
        TextView customerAddress = findViewById(R.id.customerAddress);
        customerAddress.setText(String.format("%s ,%s ,%s", customer.getAddressNo(), customer.getAddress1(), customer.getAddress2()));
        TextView customerType = findViewById(R.id.customerType);
        TextView customerId = findViewById(R.id.customerId);
        customerId.setText(String.format("%s%s", "", String.valueOf(customer.getId())));
        customerType.setText(customer.getCustomer_type_name());

        System.out.println("===================>>>>>>unique id=====" + getDeviceId(this));
//
        CustomerActivitySectionsPagerAdapter customerActivitySectionsPagerAdapter = new CustomerActivitySectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(customerActivitySectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(5);
        tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        int tabIconColor = getResources().getColor(R.color.colorPrimary);
        int tabIconDeselectColor = getResources().getColor(R.color.tabDeselected);
        Objects.requireNonNull(Objects.requireNonNull(tabs.getTabAt(0)).setIcon(TAB_ICONS[0]).getIcon()).setTint(tabIconColor);
        Objects.requireNonNull(Objects.requireNonNull(tabs.getTabAt(1)).setIcon(TAB_ICONS[1]).getIcon()).setTint(tabIconDeselectColor);
        Objects.requireNonNull(Objects.requireNonNull(tabs.getTabAt(2)).setIcon(TAB_ICONS[2]).getIcon()).setTint(tabIconDeselectColor);
        Objects.requireNonNull(Objects.requireNonNull(tabs.getTabAt(3)).setIcon(TAB_ICONS[3]).getIcon()).setTint(tabIconDeselectColor);
        Objects.requireNonNull(Objects.requireNonNull(tabs.getTabAt(4)).setIcon(TAB_ICONS[4]).getIcon()).setTint(tabIconDeselectColor);
        Objects.requireNonNull(Objects.requireNonNull(tabs.getTabAt(5)).setIcon(TAB_ICONS[5]).getIcon()).setTint(tabIconDeselectColor);


        LinearLayout downloadBtn = findViewById(R.id.downloadBtn);
        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showProgressDialog();
                getCustomerInvoicesWithApi(
                        Integer.parseInt(shared.getCurrentUser().deviceId),
                        shared.getSelectedCustomerId(),
                        new OntaskCompleteInvoice() {
                            @Override
                            public void OnComplete(ArrayList<Invoice> invoices, ArrayList<InvoiceItem> invoiceItems, List<Integer> invoiceIdArray) {
                                dismissProgressDialog();
                                System.out.println("=====invoiceItemss count ====<>><<>>>>" + invoiceItems.size());

                                invoiceViewModel.insertDownloaded(invoices, new OnTaskCompleted() {
                                    @Override
                                    public void onTaskCompleted() {


                                    }
                                });


                                invoiceItemViewModel.deleteAndInsertAll(invoiceItems, invoiceIdArray, new OnTaskCompleted() {
                                    @Override
                                    public void onTaskCompleted() {
                                        updateSelf(invoiceItems);
                                        getLastInvId();
                                    }
                                });
                            }

                            @Override
                            public void OnError(String err) {
                                dismissProgressDialog();
                            }


                        }
                );
            }
        });

        LinearLayout addNewInvoiceButton = findViewById(R.id.addNewInvoiceButtom);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                isPresale = false;
                int tabIconColor = getResources().getColor(R.color.colorPrimary);
                Objects.requireNonNull(tab.getIcon()).setTint(tabIconColor);

                if (tab.getPosition() == 3 || tab.getPosition() == 4) {
                    if (tab.getPosition() == 4) {
                        isPresale = true;
                    }
                    addNewInvoiceButton.setVisibility(View.VISIBLE);
                } else {
                    addNewInvoiceButton.setVisibility(View.GONE);

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = getResources().getColor(R.color.tabDeselected);
                Objects.requireNonNull(tab.getIcon()).setTint(tabIconColor);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        invoiceViewModel.getCustomerAllInvoices(shared.getSelectedCustomerId()).observe(this, new Observer<List<Invoice>>() {
            @Override
            public void onChanged(List<Invoice> invoices) {
                invoiceCount = invoices.size();
            }
        });

        addNewInvoiceButton.setOnClickListener(v -> {
//            if (tabs.getSelectedTabPosition() == 4) {
//                Intent intent = new Intent(getContext(), PaymentActivity.class);
//                startActivity(intent);
//                return;
//            }
            shared.setSelectedInv(shared.getCurrentInv() + 1);
            if (isPresale) {
                Intent intent = new Intent(getContext(), NewInvoiceActivity.class);
                intent.putExtra("isPreSale", isPresale);
                startActivity(intent);

                return;
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
            Date date = new Date();
            int selfUpdateCount = selfUpdateViewModel.getTodaySelfUpdates(dateFormat.format(date), shared.getSelectedCustomerId()).size();
            System.out.println("==========" + invoiceCount + "========" + selfUpdateCount);
            if (selfUpdateCount == 0 && invoiceCount > 0) {
                showToastMessage("Please complete self update");
                Objects.requireNonNull(tabs.getTabAt(2)).select();
                return;
            }
            startActivity(new Intent(getContext(), NewInvoiceActivity.class));
        });


        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            System.out.println("=========location====<<<>>>>>>>>>>" + location);
            if (location != null) {
//
                shared.setLocation(location.getLatitude(), location.getLongitude());
            } else {
                shared.setLocation(0.00, 0.00);
            }
        });


    }

    private void updateSelf(ArrayList<InvoiceItem> invoiceItems) {
        Map<Integer, Self> selves = new HashMap<>();
        for (InvoiceItem invoiceItem : invoiceItems) {

            if (invoiceItem.invoice_item_flag != RETURN_ITEM_CODE) {
                Self self = new Self();
                self.customerId = shared.getSelectedCustomerId();
                self.itemsid = invoiceItem.items_id;
                self.ownQty = invoiceItem.invoice_item_qty_s;
                if (selves.containsKey(invoiceItem.items_id)) {
                    self.ownQty = Objects.requireNonNull(selves.get(invoiceItem.items_id)).ownQty + invoiceItem.invoice_item_qty_s;
                    selves.remove(invoiceItem.items_id);
                }
                selves.put(invoiceItem.items_id, self);
            }
        }


        if (selves.size() > 0) {
            List<Self> list = new ArrayList<>();
            for (Self self : selves.values()) {
                list.add(self);
            }
            selfViewModel.refreshSelfCustomer(list, shared.getSelectedCustomerId(), new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {

                }
            });
        }

    }

    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private Context getContext() {
        return this;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismissProgressDialog();
        if (customerViewModel != null && customerViewModel.getAll().hasObservers()) {
            customerViewModel.getAll().removeObservers(this);
        }
        if (invoiceViewModel != null && invoiceViewModel.getCustomerAllInvoices(shared.getSelectedCustomerId()).hasObservers()) {
            invoiceViewModel.getCustomerAllInvoices(shared.getSelectedCustomerId()).removeObservers(this);
        }

    }

    public void onBackPressed(View v) {
        onBackPressed();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, "Processing...", "Please wait");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    private void getLastInvId() {
//        APIClient getNoticeDataService = RetrofitInstance.getRetrofitInstanceNew().create(APIClient.class);
//        Call<String> stringCall = getNoticeDataService.getLastInvoiceId(Integer.parseInt(shared.getCurrentUser().deviceId) );
//        stringCall.enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//                if(response.isSuccessful()){
//                    System.out.println("==============="+response.body());
//                    try {
//                        JSONObject jsonObject = new JSONObject(response.body());
//                        if(jsonObject.getBoolean("success")){
//                            JSONObject dataObj = jsonObject.getJSONObject("data");
//                            shared.setCurrentInv(dataObj.getInt("inv_code"));
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//                System.out.println("==============="+t.getMessage());
//            }
//        });
    }
}