package com.prologics.Bellose.activities.reports;


import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;

import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.recyclerViews.InventoryValuationReportRecyclerViewAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;

import com.prologics.Bellose.R;

import java.util.ArrayList;
import java.util.List;

public class InventoryReportActivity extends AppCompatActivity {

    private ItemViewModel itemViewModel;
    private RecyclerView recyclerView;
    private InventoryValuationReportRecyclerViewAdapter inventoryValuationReportRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_report);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ItemViewModel itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);

        RecyclerView recyclerView = findViewById(R.id.list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        inventoryValuationReportRecyclerViewAdapter = new InventoryValuationReportRecyclerViewAdapter(new ArrayList<Item>());
        recyclerView.setAdapter(inventoryValuationReportRecyclerViewAdapter);
        itemViewModel.getAllItems().observe(this, new Observer<List<Item>>() {
            @Override
            public void onChanged(List<Item> items) {

                inventoryValuationReportRecyclerViewAdapter.setmValues(items);
                inventoryValuationReportRecyclerViewAdapter.notifyDataSetChanged();

            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (itemViewModel != null && itemViewModel.getAllItems().hasObservers()) {
            itemViewModel.getAllItems().removeObservers(this);
        }
    }

    public void onBackPressed(View v) {
        onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_stock_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                System.out.println(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(inventoryValuationReportRecyclerViewAdapter != null){
                    inventoryValuationReportRecyclerViewAdapter.getFilter().filter(newText);
                    inventoryValuationReportRecyclerViewAdapter.notifyDataSetChanged();
                }

                return false;
            }
        });
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
//        return super.onCreateOptionsMenu(menu);

    }

}
