package com.prologics.Bellose.activities;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.prologics.Bellose.databaseModule.Entity.Customer;
import com.prologics.Bellose.databaseModule.EntityViewModel.CustomerViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.SelfViewModel;
import com.prologics.Bellose.databaseModule.Shared;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.view.View;
import android.widget.TextView;

import com.prologics.Bellose.R;

public class PaymentActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;
    private SelfViewModel selfViewModel;
    private InvoiceItemViewModel invoiceItemViewModel;
    private InvoiceViewModel invoiceViewModel;
    private ItemViewModel itemViewModel;
    private Customer customer;
    private Shared shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_invoice);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CustomerViewModel customerViewModel = new ViewModelProvider(this).get(CustomerViewModel.class);
        shared = Shared.getInstance(this);
        customer = customerViewModel.getCustomer(shared.getSelectedCustomerId());
        TextView customerName = findViewById(R.id.customerName);
        customerName.setText(customer.getName());
        TextView customerAddress = findViewById(R.id.customerAddress);
        customerAddress.setText(String.format("%s ,%s ,%s", customer.getAddressNo(), customer.getAddress1(), customer.getAddress2()));
        TextView customerType = findViewById(R.id.customerType);
        TextView invNo = findViewById(R.id.invoiceNumber);



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onBackPressed(View v) {
        onBackPressed();
    }
}
