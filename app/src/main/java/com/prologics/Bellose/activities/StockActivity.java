package com.prologics.Bellose.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import com.prologics.Bellose.databaseModule.Entity.InvoiceSummery;
import com.prologics.Bellose.databaseModule.Entity.Item;
import com.prologics.Bellose.databaseModule.EntityViewModel.InvoiceViewModel;
import com.prologics.Bellose.databaseModule.EntityViewModel.ItemViewModel;
import com.prologics.Bellose.databaseModule.Shared;
import com.prologics.Bellose.fragments.dialogs.ItemAddAndUpdateDialog;
import com.prologics.Bellose.fragments.dialogs.ReturnItemAddDialog;
import com.prologics.Bellose.fragments.dummy.DummyContent;
import com.prologics.Bellose.recyclerViews.StockListRecyclerViewAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.prologics.Bellose.R;

import java.util.List;

import static com.prologics.Bellose.databaseModule.GlobalValues.DECIMAL_FORMAT;
import static com.prologics.Bellose.databaseModule.GlobalValues.ITEM_CODE;
import static com.prologics.Bellose.databaseModule.GlobalValues.ITEM_TYPE_KEY;
import static com.prologics.Bellose.databaseModule.GlobalValues.RETURN_ITEM_CODE;

public class StockActivity extends AppCompatActivity  {

    private int tabIndex = 0;
    private StockListRecyclerViewAdapter stockListRecyclerViewAdapter;
    private ItemViewModel itemViewModel;
    private boolean isPreSale = false;
    private int scrollPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabIndex = getIntent().getIntExtra(ITEM_TYPE_KEY, 1);
        isPreSale = getIntent().getBooleanExtra("isPreSale",false);
        System.out.println("=======>>>>>>>>>>>session id=====" + tabIndex);
        RecyclerView recyclerView = findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(this) {
            @Override protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };



        itemViewModel = new ViewModelProvider(this).get(ItemViewModel.class);
        InvoiceViewModel invoiceViewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);

        OnItemClickListner onItemClickListner = (item,position) -> {
            scrollPosition = position;


            if (tabIndex == 0) {
                if (item.qty == 0 && !isPreSale) {
                    return;
                }
                ItemAddAndUpdateDialog itemAddAndUpdateDialog = new ItemAddAndUpdateDialog(item,isPreSale);
                itemAddAndUpdateDialog.show(getSupportFragmentManager(), "ItemAddAndUpdateDialog");
                itemAddAndUpdateDialog.setCancelListner(v -> itemAddAndUpdateDialog.dismiss());
            } else {
                ReturnItemAddDialog returnItemAddDialog = new ReturnItemAddDialog(item);
                returnItemAddDialog.show(getSupportFragmentManager(), "returnItemAddDialog");
                returnItemAddDialog.setCancelListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        returnItemAddDialog.dismiss();
                    }
                });
            }


        };

        itemViewModel.getAllItems().observe(this, new Observer<List<Item>>() {
            @Override
            public void onChanged(List<Item> items) {

                System.out.println("============>>>>>>>>>>" + items.size());
                stockListRecyclerViewAdapter = new StockListRecyclerViewAdapter(getContext(), items, onItemClickListner);
                recyclerView.setAdapter(stockListRecyclerViewAdapter);
                recyclerView.scrollToPosition(scrollPosition);
            }
        });

        TextView productDiscount = findViewById(R.id.productDiscount);
        TextView productNetTotal = findViewById(R.id.productNetTotal);
        Shared shared = Shared.getInstance(this);
        invoiceViewModel.getInvoiceSummery(shared.getSelectedInv()).observe(this, invoiceSummeries -> {
            double netTotal = 0.0;
            double discount = 0.0;
            double returnTotal = 0.0;
            double invoiceTotal = 0.0;
            for (InvoiceSummery invoiceSummery : invoiceSummeries) {

                if (invoiceSummery.invoice_item_flag == ITEM_CODE) {
                    netTotal = netTotal + invoiceSummery.total;
                    discount = discount + invoiceSummery.discount_total;
                } else if (invoiceSummery.invoice_item_flag == RETURN_ITEM_CODE) {
                    returnTotal = returnTotal + invoiceSummery.total;
                }
            }

            invoiceTotal = netTotal - returnTotal;
            productDiscount.setText(DECIMAL_FORMAT.format(discount));
            productNetTotal.setText(DECIMAL_FORMAT.format(invoiceTotal));

        });


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_stock_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                System.out.println(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(stockListRecyclerViewAdapter != null){
                    stockListRecyclerViewAdapter.getFilter().filter(newText);
                    stockListRecyclerViewAdapter.notifyDataSetChanged();
                }

                return false;
            }
        });
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
//        return super.onCreateOptionsMenu(menu);

    }

    private Context getContext() {
        return this;
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyContent.DummyItem item);
    }

    public interface OnItemClickListner {
        void onClickItem(Item item,int position);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if( itemViewModel != null && itemViewModel.getAllItems().hasObservers()){
            itemViewModel.getAllItems().removeObservers(this);
        }

    }

    public void onBackPressed(View v) {
        onBackPressed();
    }


}
